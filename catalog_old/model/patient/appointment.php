<?php
class ModelPatientAppointment extends Model {
	
	public function addAppointment($data) {
		$pid="1";
		$status="1";
		$datetime = explode('/',$data['appointment_date']);
		$appo_date = $datetime[0]; 
		$appo_time = $datetime[1];

		$this->db->query("INSERT INTO " . DB_PREFIX . "appointment SET pid = '" . (int)$pid . "', doc_id = '" . (int)$data['doctor'] . "', app_date = '" . $appo_date . "', app_time = '" . $appo_time . "', status = '" . $status . "' , created_at = NOW()");

		return $a_id = $this->db->getLastId();
	}
	
	 public function doctorList(){
	   $customerlist =array();
	   
	   	$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
}