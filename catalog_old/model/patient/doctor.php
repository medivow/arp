<?php
class ModelPatientDoctor extends Model {
	
	public function getDoctors($data = array()) {
		$sql = "SELECT *  
		FROM " . DB_PREFIX . "customer c
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON c.customer_group_id = cgd.customer_group_id
		WHERE cgd.name='Doctors'";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "e.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(e.created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		/*if ($this->customer->getId()) {
			$sql .= " AND c.customer_id = ".$this->customer->getId()."";
		}*/

		

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);

		

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY c.customer_id DESC";
		} else {
			$sql .= " ORDER BY c.customer_id DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	
		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getTotalEvents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event e WHERE  1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if($this->user->user_group_id == JUDGE_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_judge WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if($this->user->user_group_id == STAFF_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_staff WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalEventsByEventGroupId($scoring_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event WHERE scoring_group_id = '" . (int)$scoring_group_id . "'");

		return $query->row['total'];
	}
	public function addPrescription($doctor_id, $patient_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_prescription SET patient_id = '" . (int)$patient_id . "', doctor_id = '" . (int)$doctor_id . "', title = '" . $this->db->escape($data['title']) . "', medicin = '" . $this->db->escape($data['medicin']) . "', precausion = '" . $this->db->escape($data['precausion']) . "', diet = '" . $this->db->escape($data['diet']) . "', description = '" . $this->db->escape($data['description']) . "'");
	}
	
	
	public function getPrescription($did, $pid) {
		//echo $pid; echo $did; die;
		$query= $this->db->query("SELECT *  
		FROM " . DB_PREFIX . "doctor_prescription where doctor_id=$did AND patient_id=$pid");

		return $query->rows;
	}
	public function getPatientDetail($pid) {
		$query= $this->db->query("SELECT *  
		FROM " . DB_PREFIX . "customer u
			WHERE customer_id=$pid");

		return $query->rows;
	}
		public function getDoctor($pid) {
		$query= $this->db->query("SELECT *  
		FROM " . DB_PREFIX . "customer u
			LEFT JOIN " . DB_PREFIX . "doctor_patients dp ON u.customer_id = dp.did where dp.pid IN($pid)");

		return $query->rows;
	}
}