<?php
class ControllerPatientPatient extends Controller {
	
public function index() 
	{
		
		//$this->load->language('patient/patient');
		$this->load->model('patient/patient');
		$this->document->setTitle($this->language->get('heading_title'));
		//$this->getList();
		
			
		$data['column_left'] = $this->load->controller('common/user_left');
		//$data['column_right'] = $this->load->controller('common/doctor_right');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		
		if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
				} else {
				$sort = 'id.title';
				}
				if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
				} else {
				$order = 'ASC';
				}
				if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
				} else {
				$page = 1;
				}
				$url = '';
				if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
				}
				$data['breadcrumbs'] = array();
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
				);
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('patient/patient', 'token=' . $this->session->data['token'] . $url, true)
				);

		 
			
			$filter_data = array(
			'customer_id'=>$this->customer->getId(),
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
			);
			
			$information_total = $this->model_patient_patient->getTotalInformations();
			$results = $this->model_patient_patient->getInformations($filter_data);
			
			
			foreach ($results as $result) {
			$data['informations'][] = array(
			'information_id' => $result['information_id'],
			'title'          => $result['title'],
			'sort_order'     => $result['sort_order'],
			'description'	 =>  $result['description'],
 			'edit'           => $this->url->link('patient/patient/edit', 'token=' . $this->session->data['token'] . '&information_id=' . $result['information_id'] . $url, true)
			);
			}
		
		
		///////////////////////////////////////////////
		
		$filter_data2 = array(
		'customer_id'=>$this->customer->getId() 
		);
		$resultsap = $this->model_patient_patient->getUserappointment($filter_data2);
		
		print_r($data['informations']);
		//die;
		foreach($resultsap as $res){
			
			$data['appoinments'][] = array(
			'pid'    => $res['pid'],
			'email'  => $res['email'],
			'fname'  => $res['firstname'],
			'lname'  => $res['lastname'],
			'app_time'  => $res['app_time'],
			'app_date'  => $res['app_date'],
			'speciality'=> $res['speciality'],
			'doc_address'=> $_res['doc_address']
			);
			}
		
		//$data['appoinments'] = $resultsap;

		/////////////////////////////////////////////			
			
			
		   $this->response->setOutput($this->load->view('patient/dashboard', $data));
	
	
		
		}
	
	
     
	  
	  
	  
	  

}