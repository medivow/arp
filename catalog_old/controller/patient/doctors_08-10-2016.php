<?php
class ControllerPatientDoctors extends Controller {
	
public function index() 
	{
		$data=array();
		$this->load->model('patient/doctor');
		$this->load->model('search/search');
		$this->load->language('patient/doctor');
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		$results = $this->model_patient_doctor->getDoctors($filter_data);
		if(!empty($results)){
            foreach($results as $doctor)
			{
			$cus_services= $this->model_search_search->getCustomerSpecilityById($doctor['customer_id']);
			$data['doctor_speciality']=implode(' , ',$cus_services);	
			$this->load->model('tool/image');
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['doctor_image'][$doctor['customer_id']]=$image;
			}
			}
			else
			{
				
				$data['doctor_speciality']='';
				$data['doctor_image']='';
			}
			
		$data['doctors'] = $results;
		$data['heading_title'] = $this->language->get('heading_title');
		$data['model_title'] = $this->language->get('model_title');
		$data['model_Doc_name'] = $this->language->get('model_Doc_name');
		$data['model_Doc_email'] = $this->language->get('model_Doc_Email');
		$data['model_Doc_fax'] = $this->language->get('model_Doc_Fax');
		$data['model_Doc_Contact'] = $this->language->get('model_Doc_Contact');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		$this->response->setOutput($this->load->view('patient/doctors', $data));

	}
	
	}


