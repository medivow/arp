<?php
class ControllerPatientPatient extends Controller {
	
public function index() 
	{
		
		//$this->load->language('patient/patient');
		$this->load->model('patient/patient');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->getList();	
			
		$data['column_left'] = $this->load->controller('common/user_left');
		//$data['column_right'] = $this->load->controller('common/doctor_right');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		
		if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
				} else {
				$sort = 'id.title';
				}
				if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
				} else {
				$order = 'ASC';
				}
				if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
				} else {
				$page = 1;
				}
				$url = '';
				if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
				}
				$data['breadcrumbs'] = array();
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
				);
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('patient/patient', 'token=' . $this->session->data['token'] . $url, true)
				);

		  
			
			$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
			);
			
			$information_total = $this->model_patient_patient->getTotalInformations();
			$results = $this->model_patient_patient->getInformations($filter_data);
			
			//print_r($results);
			//die();
			
			foreach ($results as $result) {
			$data['informations'][] = array(
			'information_id' => $result['information_id'],
			'title'          => $result['title'],
			'sort_order'     => $result['sort_order'],
			'description'	 =>  $result['description'],
 			'edit'           => $this->url->link('patient/patient/edit', 'token=' . $this->session->data['token'] . '&information_id=' . $result['information_id'] . $url, true)
			);
			}
		
		
		
		
		
		$this->response->setOutput($this->load->view('patient/dashboard', $data));
		

	}
	
	

      protected function getList() {
		  
				if (isset($this->request->get['sort'])) {
				$sort = $this->request->get['sort'];
				} else {
				$sort = 'id.title';
				}
				if (isset($this->request->get['order'])) {
				$order = $this->request->get['order'];
				} else {
				$order = 'ASC';
				}
				if (isset($this->request->get['page'])) {
				$page = $this->request->get['page'];
				} else {
				$page = 1;
				}
				$url = '';
				if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
				}
				if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
				}
				if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
				}
				$data['breadcrumbs'] = array();
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_home'),
				'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
				);
				$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('patient/patient', 'token=' . $this->session->data['token'] . $url, true)
				);

		  
			
			$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
			);
			
			$information_total = $this->model_patient_patient->getTotalInformations();
			$results = $this->model_patient_patient->getInformations($filter_data);
			
			//print_r($results);
			
			foreach ($results as $result) {
			$data['informations'][] = array(
			'information_id' => $result['information_id'],
			'title'          => $result['title'],
			'sort_order'     => $result['sort_order'],
			'edit'           => $this->url->link('patient/patient/edit', 'token=' . $this->session->data['token'] . '&information_id=' . $result['information_id'] . $url, true)
			);
			}
	   
	     
	  	
	  }

}