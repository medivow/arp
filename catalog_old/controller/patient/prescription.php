<?php
class ControllerPatientPrescription extends Controller {
	
public function index() 
	{
		$this->load->language('patient/prescription');
		$this->load->model('patient/prescription');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['model_heading_title'] = $this->language->get('model_heading_title');
		$data['model_pres_title'] = $this->language->get('model_pres_title');
		$data['model_pres_medicine'] = $this->language->get('model_pres_medicine');
		$data['model_pres_precautions'] = $this->language->get('model_pres_precautions');
		$data['model_pres_diet'] = $this->language->get('model_pres_diet');
		$data['model_pres_description'] = $this->language->get('model_pres_description');
		$data['model_pres_adddate']=$this->language->get('model_pres_adddate');
		$data['model_pres_docname']=$this->language->get('model_pres_docname');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
        $filter_data = array('customer_id'=>$this->customer->getId());
		$results = $this->model_patient_prescription->getPrescriptions($filter_data);
			if(!empty($results)){
				$data['prescription']=$results;
				}
			else{
			$data['informations'][]='NO RESULTS FOUND';
				}
		//////////////// Show In View page all data ///////////////////			
		 $this->response->setOutput($this->load->view('patient/prescription', $data));
	 
	 	}
	}
