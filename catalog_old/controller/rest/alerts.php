<?php 
/**
 * alerts.php
 *
 * Alert management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://newapi.opencart-api.com/demo/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestAlerts extends RestController {

	public function alerts()
	{
		
		$this->language->load('checkout/checkout');
		
		$this->load->model('account/alerts');
		
		
		$json = array('success' => true);
		
		 $data=$_POST;
		
		
		if ($json['success']) {	
					
			if ((utf8_strlen($data['user_id']) < 1) || (utf8_strlen($data['user_id']) > 32)) {
				$json['error']['user_id'] = " User id Not Found";
				$json['success'] = false;
			}

			if ((utf8_strlen($data['location']) < 1) || (utf8_strlen($data['location']) > 50)) {
				$json['error']['location'] = "Location Not Found";
				$json['success'] = false;
			}
			
			if ((utf8_strlen($data['user_comment']) < 1) || (utf8_strlen($data['user_comment']) > 1000)) {
				$json['error']['user_comment'] = "No User Comments";
				$json['success'] = false;
			}
			
		if ($json['success']) {
			
			$this->model_account_alerts->addAlerts($_POST);
		if ($this->debugIt) {
			//echo '<pre>';
			//print_r($json);
			//echo '</pre>';
		   }
		 else { $this->response->setOutput(json_encode($json)); }
		   }
	   	 }

	
	}
	
	public function registerCustomer($data) {
		
		$this->language->load('checkout/checkout');

		$this->load->model('account/alerts');

		$json = array('success' => true);


		if ($json['success']) {	
						
			if ((utf8_strlen($data['user_id']) < 1) || (utf8_strlen($data['user_id']) > 32)) {
				$json['error']['user_id'] = " User id Not Found";
				$json['success'] = false;
			}

			if ((utf8_strlen($data['location']) < 1) || (utf8_strlen($data['location']) > 50)) {
				$json['error']['location'] = "Location Not Found";
				$json['success'] = false;
			}
			
			if ((utf8_strlen($data['user_comment']) < 1) || (utf8_strlen($data['user_comment']) > 1000)) {
				$json['error']['user_comment'] = "No User Comments";
				$json['success'] = false;
			}
			
		}
 	
		if ($json['success']) {
			$this->model_account_alerts->addAlerts($data);
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		   }
		 else { $this->response->setOutput(json_encode($json)); }
		   }
	   	 }
	   }
