<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3>Prescription Details </h3>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  	<div class="col-sm-12">
                  	<div class="col-sm-offset-2">
                  	<!--<div class="col-sm-2">
                    	<img src="catalog/view/theme/default/image/user.png" />
                    </div>-->
                    
                
   <?php 
                           if(count($pres_results)>0){
                            //echo $patients['firstname']; die;
                           foreach($pres_results as $prescription) {
                           //echo $plist['firstname']; die;
                        ?>
                    <div class="col-sm-6">
                   
                    	<div class="row">
                        	    <div class="col-sm-4"><label>Title:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['title']; ?></div>
                           </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Medicin:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['medicin']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Precausion:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['precausion']; ?></div>
                             </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Diet:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['diet']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Added Date:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['added_date']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Description:</label></div>
                                <div class="col-sm-8"><?php echo $prescription['description']; ?></div>
                             </div>
                             
  
                        </div>
                        <?php } } ?>

                      
                    </div>
                  </div>
                </div>
                  	<div class="col-sm-12"><a href="<?php echo $action; ?>" class="btn btn-default pull-right">Add Prescription</a></div>  
                </div>
                <!--<div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>

<?php echo $footer; ?>