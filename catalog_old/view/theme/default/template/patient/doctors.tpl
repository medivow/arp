<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3><?php echo $heading_title; ?> </h3>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input id="searchval"  class="form-control" placeholder="Search for..." type="text">
            <span id="searchdoc"></span>
            <span class="input-group-btn">
            <button id="searchdoctor" class="btn btn-default" type="button">Go!</button>
            </span> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone </th>
                          <th>Fax </th>
                        </tr>
                      </thead>
                      <tbody class="odd">
                     
                        <?php 
                           if(count($doctors)>0){
                           $i=1;
                           foreach($doctors as $dlist) {
                        ?>
                       <tr class="odd1"><td class="sorting_1"><?php echo $dlist['firstname']; ?></td>
                          <td><?php echo $dlist['email']; ?></td>
                          <td><?php echo $dlist['telephone']; ?></td>
                          <td><?php echo $dlist['fax']; ?></td>
						   <td><a href="" data-toggle="modal" data-target="#myModal<?php echo $i; ?>">Detail</a></td>
                          </tr>
                        <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
                        <div class="modal-dialog"> <div class="modal-content">
                        <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo $model_title; ?></h4>
                        </div>
                        <div class="modal-body">
                        <div class="tabbable">
        <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#account">Personal Detail</a></li>
    <li><a data-toggle="tab" href="#speciality">Speciality</a></li>
 </ul>

  <div class="tab-content">
    <div id="account" class="tab-pane fade in active">
       
                        <img src="<?php echo $doctor_image[$dlist['customer_id']] ?>">
                        <p><?php echo $model_Doc_name; ?> : <?php echo $dlist['firstname']. ' '.$dlist['lastname']; ?></p>
                        <p><?php echo $model_Doc_Contact; ?> : <?php echo $dlist['telephone']?></p>
                         <p><?php echo $model_Doc_email; ?> : <?php echo $dlist['email']; ?></p>
                        <p><?php echo $model_Doc_fax; ?> : <?php echo $dlist['fax']; ?></p>
      
    </div>
    <div id="speciality" class="tab-pane fade">
      <h3></h3>
      <p><?php echo $doctor_speciality; ?></p>
    </div>
    
  </div>
      </div>
                        
                        
                        
                       
                       
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div></div>
                        </div></div>
                        <?php $i++;}} else { 
              ?>
              <td class="sorting_1" colspan="4">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                  
                  
                <div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>
<script>
$(document).ready(function(){
var inp = $("#searchval");
if (inp.val().length > 0) {
   $("odd1").html() 
}
});
</script>
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/doctors/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>