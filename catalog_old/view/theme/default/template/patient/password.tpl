<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
       <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
          <h2 class="appointments"><?php echo $heading_title; ?></h2>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
                <div class="form-group marRl0">
                <label for="comment"><?php echo $entry_password; ?></label>
                  <input type="password" name="password" placeholder="Password" class="form-control" required/>
                   <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
    </div>
                <div class="form-group marRl0">
                  <label for="comment"><?php echo $entry_confirm; ?></label>
                  <input type="password" name="confirm" class="form-control" placeholder="Confirm Password" required/>
                  <?php if ($error_confirm) { ?>
              <div class="text-danger"><?php echo $error_confirm; ?></div>
              <?php } ?>
                </div>
          <button class="btn btn-default" type="submit">Submit</button>
               </form>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
  </div>
  <!-- /#page-wrapper -->
<?php echo $footer; ?>