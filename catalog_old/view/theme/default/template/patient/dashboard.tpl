<?php echo $header; ?>
<?php echo $column_left;?>
  </nav>
<div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-5">
          <h2 class="appointments">Upcoming Appointments</h2>
          <div class="media">
            <div class="media-left">
              <p>Friday, 4/5 <br>
                11:45 am</p>
            </div>
            <div class="media-body"> <img src="catalog/view/theme/default/image/cont1.png">
              <div class="media-heading1">
                <h4 class="media-heading">Dr. Amit Gupta</h4>
                <p>Nternal Medicine</p>
                <p>129 west 9th St. Parparganj Delhi</p>
                <p>(255) 201-7348</p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="media-left">
              <p>Friday, 4/5 <br>
                11:45 am</p>
            </div>
            <div class="media-body"> <img src="catalog/view/theme/default/image/cont1.png">
              <div class="media-heading1">
                <h4 class="media-heading">Dr. Amit Gupta</h4>
                <p>Nternal Medicine</p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="media-left">
              <p>Friday, 4/5 <br>
                11:45 am</p>
            </div>
            <div class="media-body"> <img src="catalog/view/theme/default/image/cont1.png">
              <div class="media-heading1">
                <h4 class="media-heading">Dr. Amit Gupta</h4>
                <p>Nternal Medicine</p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="media-left">
              <p>Friday, 4/5 <br>
                11:45 am</p>
            </div>
            <div class="media-body"> <img src="catalog/view/theme/default/image/cont1.png">
              <div class="media-heading1">
                <h4 class="media-heading">Dr. Amit Gupta</h4>
                <p>Nternal Medicine</p>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="media-left">
              <p>Friday, 4/5 <br>
                11:45 am</p>
            </div>
            <div class="media-body"> <img src="catalog/view/theme/default/image/cont1.png">
              <div class="media-heading1">
                <h4 class="media-heading">Dr. Amit Gupta</h4>
                <p>Nternal Medicine</p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-7">
          <h2 class="appointments">Diet Chart</h2>
          <div class="table-responsive">
    
          <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Doctor</th>
                  
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
               <?php if ($informations) {?>
                <?php
                	$i=1;
                 foreach ($informations as $information) { ?>
                <?php if ($information['information_id']) { ?>
                <tr>
                  <td><?php echo $information['title']; ?></td>
                  
                  <td><a href="#" class="btn btn-primary" role="button" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Detail</a></td>
                </tr>
        <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
        <div class="modal-dialog"> <div class="modal-content">
        <div class="modal-header"> 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?php echo $information['title']; ?></h4>
        </div>
        <div class="modal-body">
        <p><?php echo html_entity_decode($information['description']); ?></p>
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div></div>
        </div></div>
        
           <?php }$i++;}} ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-5">
          <h2 class="appointments">FAMILY LIST <i class="fa fa-plus-square-o" aria-hidden="true"></i> </h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Relation</th>
                  <th>View</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Divya</td>
                  <td>Sister</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Aditya</td>
                  <td>Son </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Priya </td>
                  <td>Daughter </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Alok</td>
                  <td>Brother</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Nikita</td>
                  <td>Sister</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Riya</td>
                  <td>Sister</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Shweta</td>
                  <td>Sister</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Aakash</td>
                  <td>Brother</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-7">
          <h2 class="appointments">Test report</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Doctor</th>
                  <th>Visits</th>
                  <th>date</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Dengue </td>
                  <td>Dr. Vivek Jain</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Malaria </td>
                  <td>Dr. Nilesh Jain</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td> Thyroid </td>
                  <td>Dr. Raj  Jain</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Jaundice</td>
                  <td>Dr. Amit Gupta</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td> Dengue</td>
                  <td>Dr. Ansuman Gupta </td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Dengue</td>
                  <td>Dr. Salini Srivastav </td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Dengue </td>
                  <td>Dr. Vivek Jain </td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>stone in kidney</td>
                  <td> Dr. Sakshi Thakur</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper -->


<?php echo $footer; ?>