<?php echo $header; ?>
<?php echo $slider; ?>
<!-----------listing Start---------------->
<section class="container">
	<div class="row">
    	<aside class="col-sm-3">
          <div class="ddd"><img src="catalog/view/theme/default/image/cont1.png" /></div>
        </aside >
        
        <article class="col-sm-9">
        	<h3>Featured Listing</h3>
            <?php if(!empty($doctors)) { 
            foreach($doctors as $doctor){
           
            ?>
        	<div class="col-sm-4 dr_profile">
            	<p> <i class="fa fa-map-marker" aria-hidden="true"></i> Him River Resort</p>
                <div class="profileBox text-center"><img src="<?php echo $doctor_image[$doctor['customer_id']] ?>"></div>
                <p class="BDS"><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
                <p class="BDS"><?php echo $doctor['qualification'] ?></p>
                <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p>
                <p class="BDS"><?php echo $doctor_speciality[$doctor['customer_id']] ?></p>
               
                <div class="line"></div>
         		<ul class="star">
                	<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
               <a class="view_profile" href="<?php echo $detail_href; ?>&doctor_id=<?=$doctor['customer_id']?>">View profile</a>
            </div>
        	<?php }} ?>
        </article>
    </div>
</section>
<!-----------listing end---------------->
<?php echo $footer; ?>