<?php echo $header; ?>

<!--<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h2><?php echo $text_new_customer; ?></h2>
            <p><strong><?php echo $text_register; ?></strong></p>
            <p><?php echo $text_register_account; ?></p>
            <a href="<?php echo $register; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
        </div>
        <div class="col-sm-6">
          <div class="well">
            <h2><?php echo $text_returning_customer; ?></h2>
            <p><strong><?php echo $text_i_am_returning_customer; ?></strong></p>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label class="control-label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-password"><?php echo $entry_password; ?></label>
                <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
              <input type="submit" value="<?php echo $button_login; ?>" class="btn btn-primary" />
              <?php if ($redirect) { ?>
              <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
              <?php } ?>
            </form>
          </div>
        </div>
      </div>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>-->

<section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
		<h3 class="text-center _pad_tb20"> Login </h3>
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 _mar_btm35" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        	<h4 class="text-center _mar_btm35">Great, you're back!</h4>
            
            
            <div class="input_styl">
            	<input type="text" class="form-control" name="username" placeholder="User Name"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
            <div class="input_styl">
            	<input type="password" name="password" class="form-control" placeholder="Password"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
            
            <div class="text-center _mar_btm30"><a href="#">Forgot password?</a></div>
            <div class="clearfix _mar_btm30"><button class="btn btn_blue col-sm-6  col-sm-offset-3 col-xs-12" type="submit">Login</button></div>
            
        </form>
    </div>
</section>

<?php echo $footer; ?>
<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(6)').addClass("active");
    })
</script>