<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
        <li> <a>
          <div class="user text-center"> <img src="catalog/view/theme/default/image/user.png">
            <p>Welcome Mr. Sandip</p>
          </div>
          </a> </li>
        <li class="active"> <a href="#"><i class="fa fa-home" aria-hidden="true"></i></i> Home</a> </li>
        <li> <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> View Hospitals</a> </li>
        <li> <a href="#"><i class="fa fa-user-md" aria-hidden="true"></i> View Doctors</a> </li>
        <li> <a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> Appointments</a> </li>
        <li> <a href="#"><i class="fa fa-lock" aria-hidden="true"></i> View Prescriptions</a> </li>
        <li> <a href="#"> <i class="fa fa-credit-card" aria-hidden="true"></i> View Past Payments</a> </li>
        <li> <a href="#"> <i class="fa fa-female" aria-hidden="true"></i> View Family Dashboard</a> </li>
        <li> <a>
          <div class="Consult text-center"> <img src="catalog/view/theme/default/image/doctor.png">
            <p>Consult a Doctor</p>
          </div>
          </a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </nav>