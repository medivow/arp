<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <ul class="nav navbar-nav side-nav">
        <li> <a>
          <div class="user text-center"> <img src="catalog/view/theme/default/image/user.png">
            <p>Welcome Mr. Sandip</p>
          </div>
          </a> </li>
        <li class="active"> <a href="#"><i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard</a> </li>
        <li > <a href="#"><i class="fa fa-list-alt" aria-hidden="true"></i> Patients list</a> </li>
        <li> <a href="#"><i class="fa fa-home" aria-hidden="true"></i></i> Case Study</a> </li>
        <li> <a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Daily Essay</a> </li>
        <li> <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> Calendar</a> </li>
        <li> <a href="#"> <i class="fa fa-hand-o-down" aria-hidden="true"></i> Billings</a> </li>
        <li> <a>
          <div class="Consult text-center"> <img src="catalog/view/theme/default/image/doctor.png">
            <p>Consult a Doctor</p>
          </div>
          </a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </nav>