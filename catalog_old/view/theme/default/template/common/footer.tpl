<footer class="medivow_footer">
    <section class="container _pad_tp20">
        <div class="row">
            <div class="col-sm-3 _mar_btm30">
                <h4>for pateints</h4>
                <ul>
                    <li><a href="#">Ask free health questions</a></li>
                    <li><a href="#">Search for Clinics</a></li>
                    <li><a href="#">Search for Diagnostics</a></li>
                    <li><a href="#">Search for Doctors</a></li>
                    <li><a href="#">Search for Gyms</a></li>
                    <li><a href="#">Search for Spas/Salons</a></li>
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>for doctors</h4>
                <ul>
                    <li><a href="#">Medivow Consult</a></li>
                    <li><a href="#">Medivow Health Feed</a></li>
                    <li><a href="#">Medivow Profile</a></li>
                    <li><a href="#">Medivow Ray</a></li>
                    <li><a href="#">Medivow Reach</a></li>
                    <li><a href="#">Medivow Tab</a></li>
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>download</h4>
                <ul>
                    <li><a href="#"><img src="catalog/view/theme/default/image/android.png"/></a></li>
                    <li><a href="#"><img src="catalog/view/theme/default/image/appstore.png"/></a></li>
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>contact</h4>
                <ul>
                    <li><a href="#">Phone no. :98373837876</a></li>
                    <li><a href="#">Email: example@medivow.com</a></li>
                    <li><h4>CONECT WITH US</h4></li>
                    <li class="medivow_social _mar_tp20">
                    	<a href="#"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
                        <a href="#" class="_mar_lr10"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-skype fa-2x" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="medivow_copyright text-center">
    	<div class="container">
            <ul>
            	<li class="copyright_text">MediVOW © 2016</li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Sitemap</a></li>
            </ul>
        </div>
    </section>
</footer>
<!-----------footer end---------->
<!-----------scroll to top start---------->
<div><a href="#" class="scrollToTop"><i class="fa fa-angle-up fa-2x" aria-hidden="true"></i></a></div>
<!-----------scroll to top end---------->

<script src="catalog/view/javascript/jquery-3.0.0.min.js"></script>
<script src="catalog/view/javascript/bootstrap.min.js"></script>
<script src="catalog/view/javascript/owl.carousel.min.js"></script>
<script src="catalog/view/javascript/main.js"></script>
</body>

</body></html>