<!-----------banner start---------->
<section class="_style_bg banner input-group" style="background-image:url(catalog/view/theme/default/image/banner.png);">
    <div class="_bg_overlay white"></div>
    <div class="media-body media-middle _upr_lyr">
        <div class="container">
            <h1 class="text-center">Find and book a doctor</h1>
            <div class="search_bar col-md-10 col-md-offset-1 clearfix">
             <form class="" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            	<div class="col-sm-5 _pad_lft0">
                	<div class="txt_box loc">
                    	<input type="text" class="form-control _pad_lft35" placeholder="Location" />
                        <input type="text" class="form-control _pad_lft35 seprator" placeholder="Locality" />
                        <i class="fa fa-compass fa-2x icon_pos" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-sm-5 _pad_lft0">
                	<div class="txt_box spec">
                    	<input type="text" class="form-control _pad_lft35" placeholder="Specialities, Doctors, Clinics, Hospitals, Labs, Spas" />
                        <i class="fa fa-2x fa-crosshairs icon_pos" aria-hidden="true"></i>

                    </div>
                </div>
                <div class="col-sm-2 srch_btn">
                	<!--<a href="#" class="btn btn_blue">SEARCH</a>-->
                    <button type="submit" class="btn btn_blue regbtn">SEARCH</button>
                </div>
                
                     </form>
                
            </div>
        </div>
    </div>
</section>
<!-----------banner end----------> 