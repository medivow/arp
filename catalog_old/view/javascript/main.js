$(document).ready(function(){
	$(".logo_slider").owlCarousel({
	autoPlay: 3000,
	items : 4,
	itemsDesktopSmall : [992,3],
	navigation : true,
	pagination : false, 
	itemsTablet: [767,2],
	itemsMobile : [550,1],
	navigationText: [
  "<i class='fa fa-angle-left' aria-hidden='true'></i>",
  "<i class='fa fa-angle-right' aria-hidden='true'></i>"
  ]
  });
  
 //Check to see if the window is top if not then display button
 $(window).scroll(function(){
  if ($(this).scrollTop() > 100) {
   $('.scrollToTop').fadeIn();
  } else {
   $('.scrollToTop').fadeOut();
  }
 });
 
 //Click event to scroll to top
 $('.scrollToTop').click(function(){
  $('html, body').animate({scrollTop : 0},800);
  return false;
 });
 
	
	$(window).scroll(function() {
	if ( $(window).width() > 767) {
	if ($(this).scrollTop() > 200){
	$(".medivow_header").addClass("sticky");
	} else{
	$(".medivow_header").removeClass("sticky");
	}
	}
	});


});
