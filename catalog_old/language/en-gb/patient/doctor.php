<?php
// Heading
$_['entry_consult_doctor']        = 'Consult a Doctor';
// Entry
$_['heading_title']               = 'Doctors List';
$_['entry_doctors']               = 'Doctors';
$_['entry_appointment_date']      = 'Appointment Date';
$_['entry_about_patient']         = 'About Patient';
$_['entry_submit']                = 'Submit';
$_['model_title']                 = 'Doctor Detail';
$_['model_Doc_name']              = 'Name';
$_['model_Doc_Email']             = 'Email';
$_['model_Doc_Contact']           = 'Contact No';
$_['model_Doc_Fax']               = 'Fax';

//Success
$_['success_appoint'] = 'Your appointment booked successfully!';

// Error
$_['error_doctor']             = 'Please select a doctor!';
$_['error_appointment_date']   = 'Please select a appointment date!';
$_['error_about_patient']      = 'About Patient must be between 3 and 200 characters!';

