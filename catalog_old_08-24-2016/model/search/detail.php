<?php
class ModelSearchDetail extends Model {
	public function getInformation($information_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE i.information_id = '" . (int)$information_id . "' AND id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1'");

		return $query->row;
	}

	public function getInformations() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

		return $query->rows;
	}

	public function getInformationLayoutId($information_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_to_layout WHERE information_id = '" . (int)$information_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}
	
	public function getDoctor($doctor_id) {
		$query = $this->db->query("SELECT tblcus.*,docdetial.*,cusspec.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' AND tblcus.approved = '1' AND tblcus.customer_id = $doctor_id");

		return $query->rows;
	}
public function getCustomerSpecilityById($customer_id) {
			$doctor_service_data = array();
	       $query = $this->db->query("SELECT splct.title FROM " . DB_PREFIX . "customer_speciality cusspelty LEFT JOIN " . DB_PREFIX . "speciality splct ON(cusspelty.speciality_id=splct.id) WHERE customer_id = '" . (int)$customer_id . "'");
	
			foreach ($query->rows as $result) {
				$doctor_service_data[] = $result['title'];
			}

			return $doctor_service_data;
		}
}