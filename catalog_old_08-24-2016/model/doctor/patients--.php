<?php
class ModelDoctorPatients extends Model {
	
	public function getPatients($data = array()) {
		$sql = "SELECT *  
		FROM " . DB_PREFIX . "doctor_patients dp
			LEFT JOIN " . DB_PREFIX . "customer u ON u.customer_id = dp.pid
			
		WHERE 1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "e.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(e.created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		if ($this->customer->getId()) {
			$sql .= " AND dp.did = ".$this->customer->getId()."";
		}

		

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);

		

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY dp.dpid DESC";
		} else {
			$sql .= " ORDER BY dp.dpid DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalEvents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event e WHERE  1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if($this->user->user_group_id == JUDGE_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_judge WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if($this->user->user_group_id == STAFF_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_staff WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalEventsByEventGroupId($scoring_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event WHERE scoring_group_id = '" . (int)$scoring_group_id . "'");

		return $query->row['total'];
	}
	
}