<?php 
class ModelPatientPatient extends Model {

public function getTotalInformations() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "dietchart");

		return $query->row['total'];
	}
	
public function getInformations($data = array()) {
		if ($data) {
			
			$sql = "SELECT * FROM " . DB_PREFIX . "dietchart i LEFT JOIN " . DB_PREFIX . "dietchart_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "dietchart_docter dcd ON (id.information_id = dcd.information_id) WHERE dcd.user_id = '" . (int)$data['customer_id'] . "'";

			$sort_data = array(
				'id.title',
				'i.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY id.title";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$information_data = $this->cache->get('information.' . (int)$this->config->get('config_language_id'));

			if (!$information_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "dietchart i LEFT JOIN " . DB_PREFIX . "dietchart_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$information_data = $query->rows;

				$this->cache->set('information.' . (int)$this->config->get('config_language_id'), $information_data);
			}

			return $information_data;
		}
	}

	
/** **/	

public function getUserappointment($data = array()) {
	if ($data) {
		
      $sql ="SELECT * FROM " . DB_PREFIX . "appointment i LEFT JOIN " . DB_PREFIX . "customer id ON (i.pid = id.customer_id) WHERE id.customer_id = '" . (int)$data['customer_id'] . "'";

		$query = $this->db->query($sql);

			return $query->rows;
		}
		
		
	}	

/** **/	
	
}

?>