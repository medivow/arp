<?php
// Heading
$_['heading_title']      = 'My Account';

// Text
$_['text_account']       = 'Account';
$_['text_my_account']    = 'My Account';
$_['text_my_orders']     = 'My Orders';
$_['text_my_newsletter'] = 'Newsletter';
$_['text_edit']          = 'Edit your account information';
$_['text_password']      = 'Change your password';
$_['text_address']       = 'Modify your address book entries';
$_['text_credit_card']   = 'Manage Stored Credit Cards';
$_['text_wishlist']      = 'Modify your wish list';
$_['text_order']         = 'View your order history';
$_['text_download']      = 'Downloads';
$_['text_reward']        = 'Your Reward Points';
$_['text_return']        = 'View your return requests';
$_['text_transaction']   = 'Your Transactions';
$_['text_newsletter']    = 'Subscribe / unsubscribe to newsletter';
$_['text_recurring']     = 'Recurring payments';
$_['text_transactions']  = 'Transactions';
$_['text_firsName']      = 'First Name';
$_['text_LastName']      = 'Last Name';
$_['text_UserName']      = 'User Name';
$_['text_Email']         = 'Email';
$_['text_Telephone']         = 'Telephone';

$_['text_account_update_success']         = 'Account Information Updated ......';
$_['profile_pics_update_success']         = 'Profile Picture Updated ......';

$_['error_warning']                = 'Warning: Please check the form carefully for errors!';
$_['error_permission']             = 'Warning: You do not have permission to modify customers!';
$_['error_exists']                 = 'Warning: E-Mail Address is already registered!';
$_['error_username_exist']         = 'Warning: username is already registered!';
$_['error_firstname']              = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']               = 'Last Name must be between 1 and 32 characters!';
$_['error_email']                  = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']              = 'Telephone must be between 3 and 32 characters!';
$_['error_password']               = 'Password must be between 4 and 20 characters!';
$_['error_confirm']                = 'Password and password confirmation do not match!';

// tab title
$_['tab_account']  = 'Account';
$_['per_info']     = 'Personal Information';
$_['tab_img']      = 'Image';
$_['tab_specialization']      = 'Specialization';
$_['tab_services']      = 'Services';
$_['tab_general']      = 'General Information';