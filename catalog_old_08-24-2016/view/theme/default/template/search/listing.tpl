<?php echo $header; ?>
<?php echo $slider; ?>
<!-----------listing Start---------------->
<section class="container">
	<div class="row">
    	<aside class="col-sm-3 side_filter">
        	<div class="panel-group" role="tablist">
  				<div class="panel panel-default">
        			<div class="panel-heading accordion" role="tab">
          				<h4 class="panel-title">
            			<a role="button" data-toggle="collapse" data-parent=".accordion" href=".collapseOne" aria-expanded="true" aria-controls="collapseOne">
              			<i class="fa fa-caret-down" aria-hidden="true"></i> Appointment	</a></h4>
        			</div>
   					<div class="panel-collapse collapse in collapseOne" role="tabpanel">
      					<div class="panel-body">
                        	<input type="checkbox"> Check me out
  					    </div>
    				</div>
  				</div>
            </div>
            <h4 class="text-center">Availability</h4>
            <ul class="days_filter">
            	<li class="active">ANY</li>
                <li><a>M</a></li>
                <li><a>T</a></li>
                <li><a>W</a></li>
                <li><a>T</a></li>
                <li><a>F</a></li>
                <li><a>S</a></li>
                <li><a>S</a></li>
            </ul>
            <div class="clearfix"></div>
            <div class="time"  id="time-range">
            	<p><span class="slider-time">9:00 AM</span> - <span class="slider-time2">5:00 PM</span></p>
    			<div class="sliders_step1">
        			<div class="slider_range" id="slider-range"></div>
    			</div>
			</div>
            <div class="medvow_Consult">
            	<i class="fa fa-comments-o" aria-hidden="true"></i>
                <div class="clearfix"></div>
                <img src="catalog/view/theme/default/image/logo5.png" />
                <p>Unanswered Health Questions, Answered by Doctors</p>
                <a class="btn btn_blue tp_mar _mar_btm10" href="#">ASK FREE QUESTION</a>
			</div>
            <div class="clearfix"></div>
            <div class="latest_news">
            <h4 class="text-center">LATEST NEWS</h4>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            </div>
        </aside>
        
        <article class="col-sm-9 dr_list">
        	<h3>Featured Listing</h3>
        	
             <?php if(!empty($doctors)) { 
            foreach($doctors as $doctor){
           
            ?>
            <div class="col-sm-4 dr_profile">
            	<p> <i class="fa fa-map-marker" aria-hidden="true"></i> Him River Resort</p>
                <div class="profileBox text-center"><img src="<?php echo $doctor_image[$doctor['customer_id']] ?>" /></div>
                <p class="BDS"><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
                <p class="BDS"><?php echo $doctor['qualification'] ?></p>
                <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p>
                <p class="BDS"><?php echo $doctor_speciality[$doctor['customer_id']] ?></p>
               
                <div class="line"></div>
         		<ul class="star">
                	<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                </ul>
               <a class="view_profile" href="<?php echo $detail_href; ?>&doctor_id=<?=$doctor['customer_id']?>">View profile</a>
            </div>
            <?php }} ?>
        </article>
    </div>
</section>

<!-----------listing end---------------->

<?php echo $footer; ?>