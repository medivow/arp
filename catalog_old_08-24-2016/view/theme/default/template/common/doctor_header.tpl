<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php //echo $direction; ?>" lang="<?php //echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php //echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>


<link href="catalog/view/theme/default/stylesheet/bootstrap.min.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/sb-admin.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/style-admin.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/front-dashboard.css" rel="stylesheet">
<link href="catalog/view/theme/default/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
</head>
<body>
<div id="wrapper"> 
  <!-- Navigation -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="index.html">Dashboard</a> </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
     <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?php echo $image; ?>"> <?php echo $welcome_message; ?> </a></li>
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
      	<ul class="dropdown-menu">
          <li> <a href="<?php echo $action_account; ?>"><i class="fa fa-fw fa-user"></i> Profile</a> </li>
          <li> <a href="<?php echo $action_password; ?>"><i class="fa fa-fw fa-lock"></i>  Password</a> </li>
          <li> <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a> </li>
          <li class="divider"></li>
          <li> <a href="<?php echo $action_logout; ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>
        </ul>
      
      </li>
     
      <li class=""> <a href="<?php echo $action_logout; ?>" class="" ><i class="fa fa-power-off"></i> Log Out </a>
        
      </li>
    </ul>