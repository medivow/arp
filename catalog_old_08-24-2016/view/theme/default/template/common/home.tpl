<?php echo $header; ?>
<?php echo $slider; ?>
<!-----------parallax start---------->
<section class="container">
    <div class="_style_bg medivow_prlx input-group _fix_bg" style="background-image:url(catalog/view/theme/default/image/parallax1.png);">
        <div class="_bg_overlay black"></div>
        <div class="media-body media-middle _upr_lyr">
            <div class="container text-center">
                <h2>consult a doctor</h2>
                <p class="tp_mar col-sm-10 col-sm-offset-1 _pad0">Now Hassel free Online consult . Choose your doctor around the globe<br />
You can meet your doctor Remotely, just fix an appointment and your doctor is available to discuss the healthcare issues with you.
</p>
                <a href="index.php?route=common/appointment" class="btn btn_blue tp_mar _mar_btm10">Continue</a>
            </div>
        </div>
    </div>
</section>
<!-----------parallax end---------->
<!-----------service start---------->
<section class="container medivow_services _pad_tb20">
  <!--  <h2 class="text-center">At vero eos et accusamus et iusto odio dignissimos ?</h2>
    <h4 class="text-center _mar_tp20">Ducimus qui blanditiis praesentium voluptatum deleniti </h4>-->
    <div class="row _mar_tp30">
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-5x fa-search-plus" aria-hidden="true"></i>
            <h4>Doctor Search</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-flask fa-5x" aria-hidden="true"></i>
            <h4>labs</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-5x fa-hospital-o" aria-hidden="true"></i>
            <h4>medicine</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-5x fa-hospital-o" aria-hidden="true"></i>
            <h4>hospitals</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-5x fa-hospital-o" aria-hidden="true"></i>
            <h4>insurance</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
        <div class="col-sm-4 _pad_tb20">
        	<i class="fa fa-5x fa-medkit" aria-hidden="true"></i>
            <h4>health profile</h4>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo 
            inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.<a href="#">Continue</a></span></p>
        </div>
    </div>
</section>
<!-----------service end---------->

<!-----------lastest blog start---------->
<section class="medivow_blog _pad_btm30 _mar_tp50">
	<div class="container">
<!-----------news start---------->    
        <article class="col-md-7 _pad0">
        	<h3>Latest News</h3>
        	<?php foreach($articles2 as $srt2) {?>
            <div class="col-sm-6 _pad_btm20 _pad_lft0">
            	<img src="<?php echo $srt2['image']; ?>" class="img-responsive"/>
                <div class="_pad_tp15 clndr_box"><i class="fa fa-calendar" aria-hidden="true"></i>16-06-2016</div>
                <p class="blog_cnt"><?php echo $srt2['description'];?> <a href="<?php echo $srt2['href'];?>" class="more_cnt">Read more</a></p>
            </div>
            <?php } ?>
        </article>
<!-----------news end----------> 
<!-----------events start---------->     
        <aside class="col-md-5 medivow_events">
        	<h3>Events</h3>
            <div class="media">
            	<div class="media-left">
                	<img src="catalog/view/theme/default/image/side1.png"/>
                </div>
                <div class="media-body">
                	<div class="clndr_box"><i class="fa fa-calendar" aria-hidden="true"></i>16-06-2016</div>
                     <p class="blog_cnt">Nemo enim ipsam voluptatem quia voluptas sit aspernatur.....</p>
                     <a href="#" class="more_cnt">Read more</a>
                </div>
            </div>
            <div class="media">
            	<div class="media-left">
                	<img src="catalog/view/theme/default/image/side2.png"/>
                </div>
                <div class="media-body">
                	<div class="clndr_box"><i class="fa fa-calendar" aria-hidden="true"></i>16-06-2016</div>
                     <p class="blog_cnt">Nemo enim ipsam voluptatem quia voluptas sit aspernatur.....</p>
                     <a href="#" class="more_cnt">Read more</a>
                </div>
            </div>
            <div class="media">
            	<div class="media-left">
                	<img src="catalog/view/theme/default/image/side3.png"/>
                </div>
                <div class="media-body">
                	<div class="clndr_box"><i class="fa fa-calendar" aria-hidden="true"></i>16-06-2016</div>
                     <p class="blog_cnt">Nemo enim ipsam voluptatem quia voluptas sit aspernatur.....</p>
                     <a href="#" class="more_cnt">Read more</a>
                </div>
            </div>
        </aside>
<!-----------events end---------->   
    </div>
</section>
<!-----------lastest blog end---------->

<?php echo $footer; ?>
<!--<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(1)').addClass("active");
    })
</script>-->