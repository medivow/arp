<?php echo $header; ?>
<!------------Dr. Ramin Rak start------------>
<section class="_style_bg banner1" style="background-image:url(img/details_banner_bg.png);">
	<div class="RaminBox text-center">
    	<img src="img/RaminRak.png">
    	<h2>Dr. Ramin Rak</h2>
        <p>Pediatrics, Children’s health</p>
    </div>
</section>
<section>
	<div class="container">
    	<div class="infoBox">
        <div class="col-sm-2"><p>Dr. Ramin Rak </p></div>
        <div class="col-sm-3"><i class="fa fa-map-marker" aria-hidden="true"></i><span>Nehru Place Delhi </span></div>
        <div class="col-sm-2"><p class="ask">Ask</p></div>
        <div class="col-sm-2">
        	<ul class="icon">
            	<li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
            </ul>
        </div>
        <div class="col-sm-3"><p>Pediatrics, Children’s health</p></div>
        </div>
    </div>
</section>
<!------------Dr. Ramin Rak end------------>
<!-----------Dr. detail start---------->
<section class="martp_sticky dr_details _pad_tp20">
	<div class="container _pad0">
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>A LITTLE ABOUT ME</h4>
                </div>
                <div class="panel-body txt_hgt _pad_tp0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’M LOCATED AT</h4>
                    <img src="img/map.png" class="img-responsive"/>
                    <p class="location"> <i class="fa fa-map-marker" aria-hidden="true"></i> <span> Nehru Place Delhi  Lorem ipsum is a simply dummy text </span> </p>
                    <p class="location"> <i class="fa fa-phone" aria-hidden="true"></i><span>Call </span> </p>
                    <p class="location"> <i class="fa fa-globe" aria-hidden="true"></i><span>Visit Website </span> </p>
                    <p class="location"> <i class="fa fa-info-circle" aria-hidden="true"></i><span>More Information </span> </p>
                </div>
            </div>
           	<div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’ M AFFILIATED WITH</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>Hospitals</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                         labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Organisations</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’ VE PUBLISHED</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>How to cure cancer</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                         dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel">
                <div class="panel-heading _pad_btm0">
                    <h4>Claim The Listing</h4>
                    <form>
  						<div class="form-group">
      						<input type="listing" class="form-control"  placeholder="Claim the listing">
                            <textarea class="form-control" placeholder="Enter message"></textarea>
                            <button type="button" class="btn btn-primary btn-lg active">Submit claim</button>
  						</div>
                    </form>
                    <div class="clearfix"></div>
                </div>
             </div>
        </div>
        <div class="col-sm-5">
        	<div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>MY ANSWERS AND INSIGHTS HAVE:</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">Saved 256 lives</li>
                    <li class="list-group-item">Helped 955.714 People</li>
                    <li class="list-group-item">Received 714 doctor agress</li>
                    <li class="list-group-item">Received 5614 thanks</li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I SPECIALIZE IN</h4>
                </div>
                <ul class="list-group _pad_btm20">
                    <li class="list-group-item">Pediatrics, Board Certified</li>
                    <li class="list-group-item">Human Neurology, Board Certified</li>
                    <li class="list-group-item">Cardiology & Oncology</li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>WON THE FOLLOWING AWARD</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><strong>Top National Doctor</strong></li>
                    <li class="list-group-item"><strong>Best Pediatrician</strong></li>
                    <li class="list-group-item"><strong>Best Cardiology</strong></li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>BEEN IN PRACTICE FOR</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">23 years</li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>SOME KIND WORD FROM OTHERS</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>Dr. Vivek Jain</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                         labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Dr. Nilesh Jain</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Dr. Aman Gupta</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>ADDITIONAL LINKS</h4>
                </div>
                <ul class="list-group link_hover">
                    <li class="list-group-item"><a href="#">My website</a></li>
                    <li class="list-group-item"><a href="#">My hospital website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-----------Dr. detail end---------->


<?php echo $footer; ?>
