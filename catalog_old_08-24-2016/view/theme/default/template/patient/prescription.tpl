<?php echo $header; ?><?php echo $column_left; ?>

<div id="page-wrapper">
<div  class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <h3><?php echo $heading_title; ?> </h3>
      <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
        <div class="input-group">
          <input class="form-control" placeholder="Search for..." type="text">
          <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
          </span> </div>
      </div>
    </div>
    <div class="col-xs-12">
      <div class="x_panel">
        <div class="x_content">
          <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
            <div class="col-sm-12 cont201">
              <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                <thead>
                  <tr role="row">
                    <th>Title</th>
                    <th>Doctor Name </th>
                    <th>Medicine</th>
                    <th>Precautions </th>
                    <th>Diet </th>
                  </tr>
                </thead>
                <tbody>
                  <?php 

                           if(!empty($prescription)){

                           $i=1;

                           foreach($prescription as $prlist) {

                        ?>
                  <tr class="odd" role="row">
                    <td class="sorting_1"><?php echo $prlist['title']; ?></td>
                    <td><?php echo $prlist['firstname']. ' '.$prlist['lastname']; ?></td>
                    <td><?php echo $prlist['medicin']; ?></td>
                    <td><?php echo $prlist['precausion']; ?></td>
                    <td><?php echo $prlist['diet']; ?></td>
                    <td><a href="<?php echo $action; ?><?php echo "&patient_id=". $prlist['id']; ?>" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Detail</a></td>
                  </tr>
                <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><?php echo $model_heading_title; ?></h4>
                      </div>
                      <div class="modal-body">
                        <div class="tab-content">
                          <div id="account" class="tab-pane fade in active">
                            <p><?php echo $model_pres_title; ?> : <?php echo $prlist['title']?></p>
                            <p><?php echo $model_pres_docname; ?> : <?php echo $prlist['firstname']. ' '.$prlist['lastname']; ?></p>
                            <p><?php echo $model_pres_medicine; ?> : <?php echo $prlist['medicin']; ?></p>
                            <p><?php echo $model_pres_precautions; ?> : <?php echo $prlist['precausion']; ?></p>
                            <p><?php echo $model_pres_diet; ?> : <?php echo $prlist['diet']; ?></p>
                            <p><?php echo $model_pres_description; ?> : <?php echo $prlist['description']; ?></p>
                            <p><?php echo $model_pres_adddate; ?> : <?php echo $prlist['added_date']; ?></p>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
                <?php $i++;}} else { 

              ?>
                
                  <td class="sorting_1" colspan="4">Result Not found</td>
                  <?php

                         } ?>
                    </tbody>
              </table>
            </div>
            <div class="row">
              <div class="col-sm-7">
                <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                  <ul class="pagination">
                    <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                    <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                    <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                    <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(6)').addClass("active");
    })
</script>
<?php echo $footer; ?>