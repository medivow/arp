<?php echo $header; ?>

<?php echo $column_left; ?>

</nav>

<div id="page-wrapper">

    <div class="container-fluid">

      <div class="row">

       		<div class="col-xs-12"> 

          <h2 class="appointments"><?php echo $entry_consult_doctor; ?></h2>

          <div style="text-align:center;">

          <span class="success-msg"> <?php echo $success; ?> </span>  </div>

          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">

                <div class="form-group marRl0 required">

                <label for="sel1"><?php echo $entry_doctors; ?></label>

                <section id="intro">

                <select id="doctor" name="doctor">

                   <?php if ($customerlist) { ?>

                    <?php foreach ($customerlist as $customer) { ?>

                    <option value="<?php echo $customer['customer_id'];?>" ><?php echo $customer['name'];?></option>  

                    <?php } } ?>

                </select>

                </section>

                 <?php if ($error_doctor) { ?>

              <div class="text-danger"><?php echo $error_doctor; ?></div>

              <?php } ?>

                </div>

                

                <div class="form-group marRl0 required">

                    <label for="dtp_input1" class=""><?php echo $entry_appointment_date; ?></label>

                    <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd / HH:ii p" data-link-field="dtp_input1">

                        <input class="form-control" size="16" type="text" name="appointment_date" value="" readonly>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>

                    </div>

                    <input type="hidden" id="dtp_input1" value="" /><br/>

               <?php if ($error_appointment_date) { ?>

              <div class="text-danger"><?php echo $error_appointment_date; ?></div>

              <?php } ?>

                </div>

                

                <div class="form-group marRl0 required">

                  <label for="comment"><?php echo $entry_about_patient; ?></label>

                  <textarea class="form-control" rows="5" id="about_patient" name="about_patient"></textarea>

                <?php if ($error_about_patient) { ?>

              <div class="text-danger"><?php echo $error_about_patient; ?></div>

              <?php } ?>

                </div>



       			<button class="btn btn-default" type="submit"><?php echo $entry_submit; ?></button>

   

                  

               </form>

        </div>

      </div>

      

      <!-- /.row --> 

    </div>

    <!-- /.container-fluid --> 

    

  </div>

<?php echo $footer; ?>