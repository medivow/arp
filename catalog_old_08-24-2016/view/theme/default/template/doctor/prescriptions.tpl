<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3>Patients list </h3>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input class="form-control" placeholder="Search for..." type="text">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Doctor Name</th>
                          <th>Location</th>
                          <th>Telephone</th>
                          <th>Age</th>
                          <th>Added date</th>
                          
                        
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                           if(count($doctors)>0){
                           foreach($doctors as $plist) {
                        ?>
                        <tr class="odd" role="row">
                          <td class="sorting_1"><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></td>
                          <td><?php echo $plist['locality']; ?></td>
                          <td><?php echo $plist['telephone']; ?></td>
                          <td><?php echo $plist['age']; ?></td>
                         
                          <td><?php echo $plist['date_added']; ?></td>
                          <td><a href="<?php echo $action; ?><?php echo "&doctor_id=". $plist['customer_id']; ?><?php echo "&patient_id=". $plist['pid']; ?>">View Prescription</a></td>
                        </tr>
                        <?php }} else { 
              ?>
              <td class="sorting_1" colspan="4">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                  
                  
                <div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>

<?php echo $footer; ?>