<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
        
        <div class="col-sm-8 col-sm-offset-2">
       <?php if (isset($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
   <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
          <h2 class="appointments"><?php echo $heading_title; ?></h2>
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
                <div class="form-group marRl0">
                <label for="comment"><?php echo $entry_title; ?></label>
                 <input type="text" name="title" value="<?php echo isset($title) ? $title : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title" class="form-control" />
                   <?php if ($error_title) { ?>
              <div class="text-danger"><?php echo $error_title; ?></div>
              <?php } ?>
    </div>

                <div class="form-group marRl0">
                  <label for="comment"><?php echo $entry_description; ?></label>
                  <textarea name="description" placeholder="<?php echo $entry_description; ?>" id="input-description" class="form-control summernote"><?php echo isset($description) ? $description : ''; ?></textarea>
                  <?php if ($error_description) { ?>
              <div class="text-danger"><?php echo $error_description; ?></div>
              <?php } ?>
                </div>

                
          <button class="btn btn-default" type="submit">Submit</button>
       
                    
                   
                  
                  
               </form>
        </div>
      </div>
      
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper -->

<?php echo $footer; ?>