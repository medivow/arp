<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-7">
          <h2 class="appointments">calendar Appointments</h2>
          <div class="cont101"><img src="catalog/view/theme/default/image/cont2.png"></div>
        </div>
        <div class="col-lg-5">
          <h2 class="appointments">CASE STUDY</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>date</th>
                  <th>View </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Cancer </td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td> Thyroid </td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Liver Infection</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">View</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-7">
          <h2 class="appointments">pateint list</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Contact</th>
                  <th>Send</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Ravi</td>
                  <td>9988254689</td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-5">
          <h2 class="appointments">Daily essay</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>date</th>
                  <th>View </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Cancer </td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td> Thyroid </td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Liver Infection</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016 </td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016</td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <tr>
                  <td>Cancer</td>
                  <td>26/05/2016</td>
                  <td>
                  <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Detail</button>
                    <div class="modal fade" id="exampleModal">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                          </div>
            </div>
                    </div>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper --> 


<?php echo $footer; ?>