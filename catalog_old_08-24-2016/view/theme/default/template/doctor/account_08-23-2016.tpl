<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
         <h2 class="appointments"><?php echo $heading_title; ?></h2>
        <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#account"><?php echo $tab_account; ?></a></li>
    <!--<li><a data-toggle="tab" href="#per_info"><?php echo $tab_per_info; ?></a></li>-->
    <li><a data-toggle="tab" href="#image"><?php echo $tab_image ;?></a></li>
  </ul>

  <div class="tab-content">
    <div id="account" class="tab-pane fade in active">
      <h3><?php echo $tab_account; ?></h3>
      <?php if ($account_update) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $account_update; ?></div>
  <?php } ?>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
                <div class="form-group marRl0">
                <label for="comment"><?php echo $text_firstName; ?></label>
                  <input type="text" name="firstname" placeholder="<?php echo $text_firstName; ?>" value="<?php echo $customer_info['firstname']; ?>" class="form-control" required/>
                   <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php  echo  $error_firstname; ?></div>
              <?php } ?>
    </div>

                <div class="form-group marRl0">
                  <label for="comment"><?php echo $text_LastName; ?></label>
                  <input type="text" name="lastname" class="form-control" value="<?php echo $customer_info['lastname']; ?>" placeholder="<?php echo $text_LastName; ?>" required/>
                  <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php  echo $error_lastname; ?></div>
              <?php } ?>
                </div>

              <div class="form-group marRl0">
                  <label for="comment"><?php echo $text_UserName; ?></label>
                  <input type="text" readonly="readonly" name="username" class="form-control" value="<?php echo $customer_info['username']; ?>" placeholder="<?php echo $text_UserName; ?>" required/>
               <?php if ($error_username) { ?>
              <div class="text-danger"><?php  echo $error_username; ?></div>
              <?php } ?>
                </div>
                <div class="form-group marRl0">
                  <label for="comment"><?php echo $text_Email; ?></label>
                  <input type="text" name="email" class="form-control" value="<?php echo $customer_info['email']; ?>" placeholder="<?php echo $text_Email; ?>" required/>
                  <?php if ($error_email) { ?>
              <div class="text-danger"><?php  echo $error_email; ?></div>
              <?php } ?>
                </div>
                <div class="form-group marRl0">
                  <label for="comment"><?php echo $text_Telephone; ?></label>
                  <input type="text" name="telephone" class="form-control" value="<?php echo $customer_info['telephone']; ?>" placeholder="<?php echo $text_Telephone; ?>" />
                 <?php if ($error_telephone) { ?>
              <div class="text-danger"><?php  echo $error_telephone; ?></div>
              <?php } ?>
                </div>
          <button class="btn btn-default" type="submit">Submit</button>
               </form>
    </div>
    <!--<div id="per_info" class="tab-pane fade">
      <h3>Menu 1</h3>
      <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
    </div>-->
    <div id="image" class="tab-pane fade">
      <h3>Change Image</h3>
      <div class="form-group">
      <form action="<?php echo $action_updateImage; ?>" method="post" enctype="multipart/form-data" id="form-customer" class="form-horizontal">
                        <label class="col-sm-2 control-label" for="input-firstname">Profile Picture</label>
                        <div class="col-sm-10">
                        	<img src="<?=$image?>" /><input type="file" name="image" required="required" placeholder="Profile Picture" id="input-image" />
						</div>
                        <button class="btn btn-default" type="submit">Submit</button>
                        </form>
                      </div>
    </div>
  </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
  </div>
  <!-- /#page-wrapper -->
<?php echo $footer; ?>