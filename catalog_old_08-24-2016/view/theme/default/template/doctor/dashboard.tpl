<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-7">
          <h2 class="appointments">calendar Appointments</h2>
          <div class="cont101">
          <link href='fullcalender/fullcalendar.css' rel='stylesheet' />
            <link href='fullcalender/fullcalendar.print.css' rel='stylesheet' media='print' />
            <script src='fullcalender/lib/moment.min.js'></script>
            <script src='fullcalender/fullcalendar.min.js'></script>
            <script>
              $(document).ready(function() {
                $('#calendar').fullCalendar({
                  height: 350,
                  header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay',
                  },
                  editable: false,
                  eventLimit: true, // allow "more" link when too many events
                  events: {
                    url: '<?=$eventjson?>',
                    error: function() {
                      $('#script-warning').show();
                    }
                  },
                  loading: function(bool) {
                    $('#loading').toggle(bool);
                  }
                });
                
              });

            </script>
            <div id='script-warning'>
            </div>
            <div id='loading'>loading...</div>
            <div id='calendar'></div> 
          </div>
        </div>
        <div class="col-lg-5">
          <h2 class="appointments">CASE STUDY</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>date</th>
                  <th>View </th>
                </tr>
              </thead>
              <tbody>
              <?php 
              if(!empty($csae_studay)){ 
              foreach($csae_studay as $case){
              ?>
                <tr>
                  <td><?php echo $case['title']; ?> </td>
                  <td><?php echo $case['added_dt']; ?></td>
                  <td><a href="<?php echo $case['detail']; ?>" class="btn btn-primary" role="button">View</a></td>
                </tr>
                <?php }} else {?>
                
                <td class="sorting_1" colspan="3">Result Not found</td>
                <?php  } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-7">
          <h2 class="appointments">pateint list</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Contact</th>
                  <th>Send</th>
                  <th>Detail</th>
                </tr>
              </thead>
              <tbody>
              <?php if(!empty($patients)){ 
              foreach($patients as $patient){
              ?>
                <tr>
                  <td><?php echo $patient['firstname'] . ' '.$patient['lastname'] ?></td>
                  <td><?php echo $patient['telephone']?></td>
                  <td><i class="fa fa-envelope" aria-hidden="true"></i></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <?php } } else {?>
                 <td class="sorting_1" colspan="4">Result Not found</td>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-5">
          <h2 class="appointments">Daily essay</h2>
          <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>date</th>
                  <th>View </th>
                </tr>
              </thead>
              <tbody>
               <?php if(!empty($dailyessays)){ 
              foreach($dailyessays as $essay){
              ?>
                <tr>
                  <td><?php echo $essay['title']; ?> </td>
                  <td><?php echo $essay['added_dt']; ?></td>
                  <td><a href="#" class="btn btn-primary" role="button">Detail</a></td>
                </tr>
                <?php }} else {?>
                <td class="sorting_1" colspan="3">Result Not found</td>
                <?php  } ?>
                <!--<tr>
                  <td>
                    <div class="modal fade" id="exampleModal">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
                          </div>
                    </div>
                    </div>
                    </div>
                  </td>
                </tr>-->
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
  </div>
  <!-- /#page-wrapper --> 
</script> 
  <script type="text/javascript"><!--
$('.date').datetimepicker({
  pickTime: false
});
//--></script>
<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(2)').addClass("active");
    })
</script>
<?php echo $footer; ?>