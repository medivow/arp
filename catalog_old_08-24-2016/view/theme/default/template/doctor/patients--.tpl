<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3>Patients list </h3>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input class="form-control" placeholder="Search for..." type="text">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Name</th>
                          <th>Location</th>
                          <th>Office</th>
                          <th>Age</th>
                          <th>Start date</th>
                          <th>Fee</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                           if(count($patients)>0){
                           foreach($patients as $plist) {
                        ?>
                        <tr class="odd" role="row">
                          <td class="sorting_1"><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></td>
                          <td>Accountant</td>
                          <td>Tokyo</td>
                          <td>33</td>
                          <td>2008/11/28</td>
                          <td><?php echo $plist['fee']; ?></td>
                        </tr>
                        <?php }} else { 
              ?>
              <td class="sorting_1" colspan="4">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>

<?php echo $footer; ?>