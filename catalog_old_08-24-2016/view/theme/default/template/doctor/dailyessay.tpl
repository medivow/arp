<?php echo $header; ?>

<?php echo $column_left; ?>

</nav>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        <h2 class="appointments"><?php echo $text_list; ?> </h2>

        <div class="pull-right">

      <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>

      </div>

        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

          <div class="input-group">

            <input id="searchval" class="form-control" placeholder="Search for..." type="text">

            <span class="input-group-btn">

            <button class="btn btn-default" type="button">Go!</button>

            </span> 

            </div>

        </div>

      </div>



        <div class="col-xs-12">

          <div class="x_panel">

            <div class="x_content">

              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">

                <div class="row">

                  <div class="col-sm-12">

                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">

                      <thead>

                <tr>

                 

                  <td class="text-left"><?php if ($sort == 'id.title') { ?>

                    <strong><?php echo $column_title; ?></strong>

                    <?php } else { ?>

                    <strong><?php echo $column_title; ?></strong>

                    <?php } ?></td>

                  <td class="text-right"><?php if ($entry_description == 'entry_description') { ?>

                    <?php echo $column_sort_order; ?>

                    <?php } else { ?>

                   <strong> <?php echo $entry_description; ?></strong>

                    <?php } ?></td>

                  <td class="text-right"><?php echo $column_action; ?></td>

                </tr>

              </thead>

                      <tbody class="odd">

                        <?php 

                           if(count($dailyessays)>0 && !empty($dailyessays)){

                          foreach ($dailyessays as $dailyessay) {

                        ?>

                        <tr class="odd1" role="row">

                          <td class="sorting_1"><?php echo $dailyessay['title']; ?></td>

                          <td><?php echo $dailyessay['description']; ?></td>

                          <td><a href="<?php echo $dailyessay['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>

                          <a href="<?php echo $dailyessay['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>

                          </td>

                         

                        </tr>

                        <?php }} else { 

              ?>

              <td class="sorting_1" colspan="4">Result Not found</td>

            <?php

                         } ?>

                      </tbody>

                    </table>

                  </div>

                </div>

              

                  <div class="col-sm-12">

                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">

                      <ul class="pagination">

                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>

                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>

                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>

                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>

                      </ul>

                    </div>

                  </div>

         

              </div>

            </div>

          </div>

        </div>



    </div>

  </div>

  <!-- /.container-fluid --> 

  

</div>


<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=doctor/dailyessay/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>