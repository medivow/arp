<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3>Patients Details </h3>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  	<div class="col-sm-12">
                  	<div class="col-sm-offset-2">
                  	<div class="col-sm-2">
                    	<img src="catalog/view/theme/default/image/user.png" />
                    </div>
                    
                    
                      <?php 
                           if(count($patients)>0){
                            //echo $patients['firstname']; die;
                           foreach($patients as $plist) {
                           //echo $plist['firstname']; die;
                        ?>
                    <div class="col-sm-6">
                   
                    	<div class="row">
                        	    <div class="col-sm-4"><label>Name:</label></div>
                                <div class="col-sm-8"><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></div>
                           </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Age:</label></div>
                                <div class="col-sm-8"><?php echo $plist['age']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Email:</label></div>
                                <div class="col-sm-8"><?php echo $plist['email']; ?></div>
                             </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Telephone:</label></div>
                                <div class="col-sm-8"><?php echo $plist['telephone']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Fax:</label></div>
                                <div class="col-sm-8"><?php echo $plist['fax']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Locality:</label></div>
                                <div class="col-sm-8"><?php echo $plist['locality']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>City:</label></div>
                                <div class="col-sm-8"><?php echo $plist['city']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>State:</label></div>
                                <div class="col-sm-8"><?php echo $plist['state']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>Country:</label></div>
                                <div class="col-sm-8"><?php echo $plist['country']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>Date Added:</label></div>
                                <div class="col-sm-8"><?php echo $plist['date_added']; ?></div>
                             </div>
                             
  
                        </div>
                        
                        <?php } } ?>


                      
                    </div>
                  </div>
                </div>
                  	<div class="col-sm-12"><a href="<?php echo $action; ?>" class="btn btn-default pull-right">View Prescription</a></div>  
                </div>
                <!--<div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>

<?php echo $footer; ?>