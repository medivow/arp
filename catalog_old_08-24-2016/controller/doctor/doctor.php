<?php
class ControllerDoctorDoctor extends Controller {
public function index() 
	{
		$this->load->model('doctor/case');
		$this->load->model('doctor/dailyessay');
		$data=array();
        $url='';

		$doct_id = $this->customer->getId();// static id for testing only

		$results = $this->model_doctor_case->getInformations($doct_id);

		foreach ($results as $result) {
			
			$data['csae_studay'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/case/detail', 'case_id=' . $result['id'] . $url, true),
			);
		}

		$results_essay = $this->model_doctor_dailyessay->getDailyEssays($doct_id);
		 if(!empty($results_essay )){
		foreach ($results_essay as $essay) {
			
			$data['dailyessays'][] = array(
				'id' => $result['id'],
				'title'          => $essay['title'],
				'description'   =>$essay['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/dailyessay/detail', 'essay_id=' . $result['id'] . $url, true),
				
			);
		}
		}
		$this->document->setTitle($this->language->get('Dashboard'));
		$this->load->model('doctor/patients');
		$filter_data=array();
		$results_patent = $this->model_doctor_patients->getPatients($filter_data);
		$data['patients'] = $results_patent;
		$data['eventjson'] = html_entity_decode($this->url->link('doctor/doctor/eventjson', $url , true));
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
		$this->response->setOutput($this->load->view('doctor/dashboard', $data));
	}

	public function eventjson() {
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		
		$this->load->model('doctor/doctor');
		$results = $this->model_doctor_doctor->getEvents($filter_data);
		
		
		$event = array();
		foreach ($results as $result) {
			$event[] = array(
				"id"=> $result['aid'],
				"title"	=> $result['firstname'].' '.$result['lastname'],
				"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
				"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($event));		
	}

	public function  calendar() 

	{

		$this->document->setTitle($this->language->get('Calendar'));
		
		$data=array();
		$url='';
		$data['eventjson'] = html_entity_decode($this->url->link('doctor/doctor/eventjson', $url , true));

	
		

		$data['column_left'] = $this->load->controller('common/doctor_left');


		//$data['column_right'] = $this->load->controller('common/doctor_right');
		
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');

		


		$this->response->setOutput($this->load->view('doctor/calendar', $data));

	}
}