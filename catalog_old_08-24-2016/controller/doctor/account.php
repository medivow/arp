<?php
class ControllerDoctorAccount extends Controller {
	private $error = array();
	public function index() {
		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('doctor/doctor', '', true);
			$this->response->redirect($this->url->link('account/login', '', true));
		}

		$this->load->language('doctor/account');
		$this->load->model('doctor/account');
		$this->document->setTitle($this->language->get('heading_title'));
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_doctor_account->editCustomer($this->customer->getId(), $this->request->post);
			$this->session->data['success'] = $this->language->get('text_account_update_success');
			$this->response->redirect($this->url->link('doctor/account', '', ''));
		}
		$customer_info=$this->model_doctor_account->getCustomer($this->customer->getId());
		$this->load->model('tool/image');
		if (!empty($customer_info) && (is_file(DIR_IMAGE . $customer_info['image']))) {
			$image = $this->model_tool_image->resize($customer_info['image'], 80, 80);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 80, 80);
		}
		$data['image'] = $image;
		if (isset($this->session->data['success'])) {
			$data['account_update'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['account_update'] = '';
		}
		
		if (isset($this->error['lastname'])) {
			$data['error_lastname'] = $this->error['lastname'];
		} else {
			$data['error_lastname'] = '';
		}

		if (isset($this->error['email'])) {
			$data['error_email'] = $this->error['email'];
		} else {
			$data['error_email'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}
		if (isset($this->error['firstname'])) {
			$data['error_firstname'] = $this->error['firstname'];
		} else {
			$data['error_firstname'] = '';
		}
		if (isset($this->error['username'])) {
			$data['error_username'] = $this->error['username'];
		} else {
			$data['error_username'] = '';
		}
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_account'),
			'href' => $this->url->link('doctor/doctor', '', true)
		);
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		} 
		$data['action']              = $this->url->link('doctor/account', '', true);
		$data['action_updateImage']  = $this->url->link('doctor/account/profilepics', '', true);
		$data['customer_info']       = $customer_info;
		$data['heading_title']       = $this->language->get('heading_title');
		$data['tab_account']         = $this->language->get('tab_account');
		$data['tab_per_info']        = $this->language->get('per_info');
		$data['tab_image']           = $this->language->get('tab_img');
		$data['tab_specialization']           = $this->language->get('tab_specialization');
		$data['tab_services']           = $this->language->get('tab_services');
		$data['tab_general']           = $this->language->get('tab_general');
		
		$data['text_firstName']      = $this->language->get('text_firsName');
		$data['text_LastName']       = $this->language->get('text_LastName');
		$data['text_UserName']       = $this->language->get('text_UserName');
		$data['text_Email']          = $this->language->get('text_Email');
		$data['text_Telephone']      = $this->language->get('text_Telephone');
		$data['text_my_account']     = $this->language->get('text_my_account');
		$data['text_edit']           = $this->language->get('text_edit');
		$data['text_password']       = $this->language->get('text_password');
		$data['text_address']        = $this->language->get('text_address');
		$data['edit']                = $this->url->link('account/edit', '', true);
		$data['password']            = $this->url->link('account/password', '', true);
		$data['address']             = $this->url->link('account/address', '', true);
		
		if ($this->config->get('reward_status')) {
			$data['reward'] = $this->url->link('account/reward', '', true);
		} else {
			$data['reward'] = '';
		}		
		
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		echo $data['header'] = $this->load->controller('common/dheader');

		$this->response->setOutput($this->load->view('doctor/account', $data));
	}
	public function profilepics() {
		    $this->load->model('doctor/account');
			$this->load->language('doctor/account');
		    $customer_info=$this->model_doctor_account->getCustomer($this->customer->getId());
            if($this->request->files['image']['name'] and $this->request->files['image']['error']==0){
				$this->model_doctor_account->UpdateProfileImage($this->customer->getId(),$customer_info['image']);
				$this->session->data['success'] = $this->language->get('profile_pics_update_success');
				$this->response->redirect($this->url->link('doctor/account', '', ''));
			}
			else
			{
				$this->response->redirect($this->url->link('doctor/account', '', ''));
			}
			
		}
	
	protected function validate() {
		
		if ((utf8_strlen($this->request->post['firstname']) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}

		if ((utf8_strlen($this->request->post['lastname']) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}

		if ((utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}
		//$customer_info = $this->model_customer_customer->getCustomerByEmail($this->request->post['email']);
		/*if($this->customer->getUserName()!=$this->request->post['username']){
         $customer_info = $this->model_doctor_doctor->getCustomerByUsername($this->request->post['username']);
		if (!isset($this->request->get['customer_id'])) {
			if ($customer_info) {
				$this->error['warning'] = $this->language->get('error_username_exist');
			}
		} else {
			if ($customer_info && ($this->request->get['customer_id'] != $customer_info['customer_id'])) {
				$this->error['warning'] = $this->language->get('error_username_exist');
			}
		}
		}*/
		if ((utf8_strlen($this->request->post['telephone']) < 3) || (utf8_strlen($this->request->post['telephone']) > 32)) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}
}
