<?php
class ControllerDoctorCase extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('doctor/case');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/case');
		$data['success'] = '';
	  
	   
	 $this->getList();
		
	}

	public function add() {
		
		$this->load->language('doctor/case');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/case');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$doct_id = $this->customer->getId();
			$this->model_doctor_case->addCase($this->request->post, $doct_id);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('doctor/case');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('doctor/case');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_doctor_case->editCase($this->request->get['case_id'], $this->request->post);
			
			

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('doctor/case');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('doctor/case');

		/*if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $information_id) {
				$this->model_doctor_case->deleteInformation($information_id);
			}*/
			
 if (isset($this->request->get['case_id']) ) {
				$this->model_doctor_case->deleteCase($this->request->get['case_id']);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		$data['entry_description'] = $this->language->get('entry_description');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true)
		);*/

		$data['add'] = $this->url->link('doctor/case/add',  $url, true);
		$data['delete'] = $this->url->link('doctor/case/delete', $url, true);

		$data['informations'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$information_total = $this->model_doctor_case->getTotalInformations();
		$doct_id = $this->customer->getId();;/// static id for testing only
		$results = $this->model_doctor_case->getInformations($doct_id);

		//print_r($results);die;

		foreach ($results as $result) {
			
			$data['informations'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'edit'           => $this->url->link('doctor/case/edit', 'case_id=' . $result['id'] . $url, true),
				'delete'           => $this->url->link('doctor/case/delete', 'case_id=' . $result['id'] . $url, true),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('doctor/case','sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('doctor/case', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $information_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('doctor/case',  $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($information_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($information_total - $this->config->get('config_limit_admin'))) ? $information_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $information_total, ceil($information_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/dheader');
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');

		$this->response->setOutput($this->load->view('doctor/case', $data));
	}



protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['information_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_bottom'] = $this->language->get('entry_bottom');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_bottom'] = $this->language->get('help_bottom');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

       if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}
		

		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true)
		);
*/
		if (!isset($this->request->get['case_id'])) {
			$data['action'] = $this->url->link('doctor/case/add', $url, true);
		} else {
			$data['action'] = $this->url->link('doctor/case/edit',  '&case_id=' . $this->request->get['case_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('doctor/case',  $url, true);
		if (isset($this->request->get['case_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$case_info = $this->model_doctor_case->getCase($this->request->get['case_id']);
		}
		//$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
       if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (isset($case_info['title'])) {
			$data['title'] = $case_info['title'];
		} else {
			$data['title'] = '';
		}
		 if (isset($this->request->post['doct_id'])) {
			$data['doct_id'] = $this->request->post['doct_id'];
		} elseif (isset($case_info['doct_id'])) {
			$data['doct_id'] = $case_info['doct_id'];
		} else {
			$data['doct_id'] = '';
		}
		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (isset($case_info['description'])) {
			$data['description'] = $case_info['description'];
		} else {
			$data['description'] = '';
		}
		//print_r($results); die;
        $data['header'] = $this->load->controller('common/dheader');
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$this->response->setOutput($this->load->view('doctor/case_form', $data));
	}
	
	private function validate() {
         
		 if ($this->request->post['title'] == '') {
			$this->error['title'] = $this->language->get('error_title');
		 }
		 
		  if ($this->request->post['description'] == '') {
			$this->error['description'] = $this->language->get('error_appointment_date');
		}
		 
		if ((utf8_strlen(trim($this->request->post['about_patient'])) < 3) || (utf8_strlen(trim($this->request->post['about_patient'])) > 200)) {
			$this->error['about_patient'] = $this->language->get('error_about_patient');
		}
				
		return !$this->error;
	}

	protected function validateForm() {
			if ((utf8_strlen($this->request->post['title']) < 3) || (utf8_strlen($this->request->post['title']) > 64)) {
				$this->error['title'] = $this->language->get('error_title');
			}

			if (utf8_strlen($this->request->post['description']) < 3) {
				$this->error['description'] = $this->language->get('error_description');
			}

		

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/information')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}
		}

		return !$this->error;
	}
}