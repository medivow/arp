<?php
class ControllerDoctorPatients extends Controller {
	
public function index() 

	{

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		$data=array();
		$this->document->setTitle($this->language->get('Patients list'));

		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		
		$this->load->model('doctor/patients');
		
		$results = $this->model_doctor_patients->getPatients($filter_data);
		
		$data['patients'] = $results;
		//echo $patients['pid']; die;
		foreach ($results as $result) {
				$pid=$result['pid'];
				
			}

		
		$data['action'] = $this->url->link('doctor/patients/detail');
		$data['column_left'] = $this->load->controller('common/doctor_left');
		//$data['column_right'] = $this->load->controller('common/doctor_right');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');

		$this->response->setOutput($this->load->view('doctor/patients', $data));

	}


			public function add()
			{
			
			if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
			
			$this->document->setTitle($this->language->get('Add Patient'));
			if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		//echo "testtt"; die;
			$data['doctor_id']=$this->request->get['doctor_id']; 
			$this->load->model('doctor/patients');
	//echo $this->request->get['doctor_id']; echo $this->session->data['patient_id']; die;
			$results = $this->model_doctor_patients->addPrescription($this->request->get['doctor_id'],$this->session->data['patient_id'], $this->request->post);
			$this->response->redirect($this->url->link('doctor/patients'));
			
			}
			$data['action'] = $this->url->link('doctor/patients/add','&doctor_id=' . $this->request->get['doctor_id'], '', true);
			$data['column_left'] = $this->load->controller('common/doctor_left');
	
	
			//$data['column_right'] = $this->load->controller('common/doctor_right');
			
			$data['footer'] = $this->load->controller('common/doctor_footer');
			$data['header'] = $this->load->controller('common/dheader');
	
	
				$this->response->setOutput($this->load->view('doctor/patient_pres', $data));
			
			}
			
			public function detail()
			{
						
				$this->session->data['patient_id'] = $this->request->get['patient_id'];
				
				//echo $this->session->data['patient_id']; die;
				$this->load->model('doctor/patients');
				
				$results = $this->model_doctor_patients->getPatientDetail($this->request->get['patient_id']);
				
				$data['patients'] = $results;
				
					foreach ($results as $result) {
						$pid=$result['customer_id'];
					}
						
						//$data['patient_id']=$this->request->get['patient_id']; 
					//$this->load->model('doctor/patients');
					
					//$presults = $this->model_doctor_patients->getPatientdetail($pid);
					//$data['prescriptions'] = $presults;
					//print_r($presults); die;
					//$data['patients'] = $results;
					
					
		
				
				$data['action'] = $this->url->link('doctor/patients/view_doctor', '&patient_id=' . $this->request->get['patient_id']);
				
					$data['column_left'] = $this->load->controller('common/doctor_left');
			
					//$data['column_right'] = $this->load->controller('common/doctor_right');
					
					$data['footer'] = $this->load->controller('common/doctor_footer');
					$data['header'] = $this->load->controller('common/dheader');
			
			
						$this->response->setOutput($this->load->view('doctor/patient_detail', $data));
					
					}
					
					
					public function view_doctor()
					{
				
						$data['patient_id']=$this->request->get['patient_id']; 
						$this->load->model('doctor/patients');
						$dresults = $this->model_doctor_patients->getDoctor($this->request->get['patient_id']);
						//print_r($dresults); die;
						$data['doctors'] = $dresults;
						
							foreach ($dresults as $result) {
								$pid=$result['pid'];
							}
					
					
			
					//$results = $this->model_doctor_patients->addPrescription($this->request->get['patient_id'], $this->request->post);
					
					//die("tst");
					
					$data['action'] = $this->url->link('doctor/patients/prescription_detail');
					$data['column_left'] = $this->load->controller('common/doctor_left');
			
			
					//$data['column_right'] = $this->load->controller('common/doctor_right');
					
					$data['footer'] = $this->load->controller('common/doctor_footer');
					$data['header'] = $this->load->controller('common/dheader');
			
			
						$this->response->setOutput($this->load->view('doctor/prescriptions', $data));
			
			}
					
	
	
	public function prescription_detail()
			{
						
				if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
				
				$this->document->setTitle($this->language->get('Prescription Detail'));
				$this->load->model('doctor/patients');
				
				$pres_results = $this->model_doctor_patients->getPrescription($this->request->get['doctor_id'], $this->request->get['patient_id']);
				//print_r($pres_results); die;
				$data['pres_results'] = $pres_results;
				
					
						
						//$data['patient_id']=$this->request->get['patient_id']; 
					//$this->load->model('doctor/patients');
					
					//$presults = $this->model_doctor_patients->getPatientdetail($pid);
					//$data['prescriptions'] = $presults;
					//print_r($presults); die;
					//$data['patients'] = $results;
					
					
		
				//echo $this->customer->getId(); die;
				$data['action'] = $this->url->link('doctor/patients/add', '&doctor_id=' . $this->customer->getId());
				
					$data['column_left'] = $this->load->controller('common/doctor_left');
			
					//$data['column_right'] = $this->load->controller('common/doctor_right');
					
					$data['footer'] = $this->load->controller('common/doctor_footer');
					$data['header'] = $this->load->controller('common/dheader');
			
			
						$this->response->setOutput($this->load->view('doctor/prescription_detail', $data));
					
					}
					
					public function search() 
	{
		$this->load->model('doctor/patients');
		
		
		
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		//$cid= $this->customer->getId();
		$action = $this->url->link('doctor/patients/detail');
		$results = $this->model_doctor_patients->getSearchPatient($this->request->get['term']);
		//$results = $this->model_patient_prescription->getSearchPrescription($this->request->get['term']);
		$return='';
	
	foreach($results as $result) {
		$i=1;
		$return.= '<tr><td class="sorting_1">'.$result['firstname'].' '.$result['lastname']. '</td>
							<td>'  .$result['email'].'</td>
                          <td>' .$result['telephone'].'</td>
                          <td>' .$result['age']. '</td>
                          <td>' .$result['date_added']. '</td>
						  <td>' .$result['fee']. '</td>
						   <td><a href="'. $action.'&patient_id='. $result['pid'].'">Detail</a></td>
                          </tr>';
						
	$i++;}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
}