<?php
class ControllerAccountRegister1 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
			$this->load->model('account/customer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
			$customer_id = $this->model_account_customer->getCustomerId();
			$cust_id= $this->session->data['cust_id']; 
			$customer_id = $this->model_account_customer->addCustomerPackage($cust_id, $this->request->post);
			
			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'customer_group_id' => $this->customer->getGroupId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);
			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				$this->load->model('account/customer_group');
				$Customer_group_Type = $this->model_account_customer_group->getCustomerGroupsNameByID($activity_data['customer_group_id']);
			  if(strtolower($Customer_group_Type[0]['name'])=='doctors'){
				$this->response->redirect($this->url->link('doctor/doctor', '', true));
				}
				else
				{
				$this->response->redirect($this->url->link('patient/patient', '', true));	
				}
			}
			//$this->response->redirect($this->url->link('account/register2'));
		}
			$data['action'] = $this->url->link('account/register1', '', true);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/register1', $data));
	}
	
	protected function validate() {
		//echo $this->session->data['username']; echo $this->session->data['password']; die;
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->session->data['username']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByUserName($this->session->data['username']);
//print_r($customer_info); die;
		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			
		
			if (!$this->customer->login($this->session->data['username'], $this->session->data['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($this->session->data['username']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->session->data['username']);
			}
		}

		return !$this->error;
	}
}