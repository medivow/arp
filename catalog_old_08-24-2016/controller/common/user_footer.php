<?php
class ControllerCommonUserFooter extends Controller {
	public function index() {
		$data=array();	
		$route=$this->request->get['route'];
		$data['route']=$route;
		return $this->load->view('common/user_footer', $data);
	}
}
