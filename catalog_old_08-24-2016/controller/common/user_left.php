<?php
class ControllerCommonUserLeft extends Controller {
	public function index() {
	$data=array();
	$this->load->model('doctor/account');
	$this->load->language('doctor/account');
    $customer_info=$this->model_doctor_account->getCustomer($this->customer->getId());
	$data['welcome_message']='Welcome ' . $this->customer->getFirstName().' '.$this->customer->getLastName() ;
	$this->load->model('tool/image');
		if (!empty($customer_info) && (is_file(DIR_IMAGE . $customer_info['image']))) {
			$image = $this->model_tool_image->resize($customer_info['image'], 80, 80);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 80, 80);
		}
		$data['image'] = $image;

	$data['action_doctors'] = $this->url->link('patient/doctors', '', true);
	$data['action_appointments'] = $this->url->link('patient/patient/calendar', '', true);
	$data['action_prescription'] = $this->url->link('patient/prescription', '', true);
	$data['action_home'] = $this->url->link('patient/patient', '', true);
	return $this->load->view('common/user_left', $data);
	}
}
