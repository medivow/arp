<?php
class ControllerSearchDetail extends Controller {
	public function index() {
		$this->load->language('search/search');

		$this->load->model('search/detail');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		/*$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);*/
			$doctor_id=$_REQUEST['doctor_id'];
			$data['doctors']= $this->model_search_detail->getDoctor($doctor_id);
			
			
			if(!empty($data['doctors'])){
            foreach($data['doctors'] as $doctor)
			{
				//print_r($doctor);die;
			$cus_services= $this->model_search_detail->getCustomerSpecilityById($doctor['customer_id']);
			$data['doctor_speciality'][$doctor['customer_id']]=implode(' , ',$cus_services);	
			$this->load->model('tool/image');
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['doctor_image'][$doctor['customer_id']]=$image;
			}
			}
			else
			{
				
				$data['doctor_speciality']='';
				$data['doctor_image']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('search/detail', $data));
	}

	
}