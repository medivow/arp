<?php
// Heading
$_['heading_title']          = 'Daily Essay';

// Text
$_['text_success']           = 'Success: You have modified Daily Essay!';
$_['text_list']              = 'Daily Essay List';
$_['text_add']               = 'Add Daily Essay';
$_['text_edit']              = 'Edit Daily Essay';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Daily Essay Title';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_title']            = 'Daily Essay Title';
$_['entry_description']      = 'Description';
$_['entry_store']            = 'Stores';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_bottom']           = 'Bottom';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_layout']           = 'Layout Override';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_bottom']            = 'Display in the bottom footer.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Daily Essay!';
$_['error_title']            = 'Daily Essay Title must be between 3 and 64 characters!';
$_['error_description']      = 'Description must be more than 3 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_account']          = 'Warning: This Daily Essay page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']         = 'Warning: This Daily Essay page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']        = 'Warning: This Daily Essay page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_return']           = 'Warning: This Daily Essay page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']            = 'Warning: This Daily Essay page cannot be deleted as it is currently used by %s stores!';

// menu title

$_['title_caseStudy']           = 'Daily Essay';
$_['title_dailyEssay']           = 'Daily Essay';