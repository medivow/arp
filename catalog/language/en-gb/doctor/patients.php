<?php
// Heading
$_['heading_title']      = 'My Account';


$_['error_title']                  = 'Warning: Please enter the title!';
$_['error_medial_test']             = 'Warning: Please Enter the Medical Test!';
$_['error_precausion']                 = 'Warning: Please Enter Precausion!';
$_['error_diet']                   = 'Warning: Please Enter The Diet!';
$_['error_description']              = 'Warning: Please Enter The Description!';


// tab title
$_['tab_account']  = 'Account';
$_['per_info']     = 'Personal Information';
$_['tab_img']      = 'Image';
$_['tab_clininc']  = 'Clininc';


// column title

$_['column_location']     = 'Location';
$_['column_timing']       = 'Timing';
$_['column_action']       = 'Action';