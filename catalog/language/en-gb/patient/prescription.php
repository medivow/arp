<?php
// Heading
$_['entry_consult_doctor']        = 'Consult a Doctor';
// Entry
$_['heading_title']               = 'Prescription List';
$_['entry_doctors']               = 'Doctors';
$_['entry_appointment_date']      = 'Appointment Date';
$_['entry_about_patient']         = 'About Patient';
$_['entry_submit']                = 'Submit';
$_['model_heading_title']          = 'Prescription Detail';
$_['model_pres_title']            = 'Title';
$_['model_pres_medicine']         = 'Medicine';
$_['model_pres_precautions']      = 'Precautions';
$_['model_pres_diet']             = 'Diet';
$_['model_pres_description']      = 'Description';
$_['model_pres_adddate']          = 'Added Date';
$_['model_pres_docname']          = 'Doctor Name';

//Success
$_['success_appoint'] = 'Your appointment booked successfully!';

// Error
$_['error_doctor']             = 'Please select a doctor!';
$_['error_appointment_date']   = 'Please select a appointment date!';
$_['error_about_patient']      = 'About Patient must be between 3 and 200 characters!';

