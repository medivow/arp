<?php
// Heading
$_['entry_consult_doctor']        = 'Consult a Doctor';
$_['title_doctor_timing']        = 'Select Timing';
// Entry

$_['entry_doctors']               = 'Doctors';
$_['entry_appointment_date']      = 'Appointment Date';
$_['entry_about_patient']         = 'About Patient';
$_['entry_submit']                = 'Submit';

//Success
$_['success_appoint'] = 'Your appointment booked successfully!';
$_['success_free_appoint'] = 'Your Free consultation query has bee submited..';
$_['error_query']              = 'Pleaase Write Youre Query between 3 and 200 characters';
$_['error_mobile']              = 'Pleaase Enter Your Contact Number';

