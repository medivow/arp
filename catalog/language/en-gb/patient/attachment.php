<?php
// Heading
$_['heading_title']          = 'Attachment';

// Text
$_['text_success']           = 'Success: You have modified Attachment!';
$_['text_delete']            = 'Success: You have deleted Attachment!';
$_['text_upload']            = 'Success: You have uploaded Attachment!';
$_['text_list']              = 'Attachment List';
$_['text_add']               = 'Add Attachment';
$_['text_edit']              = 'Edit Attachment';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Title';
$_['column_date']            = 'Date';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_title']            = 'Title';
$_['entry_attachment']       = 'Attachment';
$_['entry_share']            = 'Share With Family';
$_['entry_yes']              = 'Yes';
$_['entry_no']               = 'No';
$_['entry_store']            = 'Stores';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_bottom']           = 'Bottom';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_layout']           = 'Layout Override';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_bottom']            = 'Display in the bottom footer.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Attachment!';
$_['error_title']            = 'Attachment Title must be between 3 and 64 characters!';
$_['error_attachment']      = 'Attachment is required!';
$_['error_share']           = 'Share With Family must be more than 3 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_account']          = 'Warning: This Attachment page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']         = 'Warning: This Attachment page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']        = 'Warning: This Attachment page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_return']           = 'Warning: This Attachment page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']            = 'Warning: This Attachment page cannot be deleted as it is currently used by %s stores!';

// menu title

$_['title_caseStudy']           = 'Attachment';
$_['title_dailyEssay']           = 'Daily Essay';