<?php
// Heading
$_['heading_title']          = 'Add Family';

// Text
$_['text_success']           = 'Success: You have modified Family!';
$_['text_list']              = 'Family List';
$_['text_add']               = 'Add Family';
$_['text_edit']              = 'Edit Family';
$_['text_default']           = 'Default';

// Column
$_['column_title']           = 'Name';
$_['column_sort_order']	     = 'Sort Order';
$_['column_action']          = 'Action';

// Entry
$_['entry_title']            = 'Name';
$_['entry_description']      = 'Relation';
$_['entry_store']            = 'Stores';
$_['entry_meta_title'] 	     = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_bottom']           = 'Bottom';
$_['entry_status']           = 'Status';
$_['entry_sort_order']       = 'Sort Order';
$_['entry_layout']           = 'Layout Override';
$_['entry_message']           = 'Entry Message';
$_['send_message']           = 'Send Message';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_bottom']            = 'Display in the bottom footer.';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify Family!';
$_['error_patient']          = 'Please select a patient!';
$_['error_relation']         = 'Please select a relation!!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_keyword']          = 'SEO URL already in use!';
$_['error_account']          = 'Warning: This Family page cannot be deleted as it is currently assigned as the store account terms!';
$_['error_checkout']         = 'Warning: This Family page cannot be deleted as it is currently assigned as the store checkout terms!';
$_['error_affiliate']        = 'Warning: This Family page cannot be deleted as it is currently assigned as the store affiliate terms!';
$_['error_return']           = 'Warning: This Family page cannot be deleted as it is currently assigned as the store return terms!';
$_['error_store']            = 'Warning: This Family page cannot be deleted as it is currently used by %s stores!';

// menu title

$_['title_caseStudy']           = 'Family';
$_['title_dailyEssay']           = 'Family';