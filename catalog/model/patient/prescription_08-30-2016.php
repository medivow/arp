<?php 
class ModelPatientPrescription extends Model {
public function getPrescriptions($data = array()) {
		$sql = "SELECT *  
		FROM " . DB_PREFIX . "doctor_prescription dp
		LEFT JOIN " . DB_PREFIX . "customer c ON dp.doctor_id= c.customer_id
		WHERE 1";
		

		$implode = array();

		if (!empty($data['filter_medicin'])) {
			$implode[] = "medicin LIKE '%" . $this->db->escape($data['filter_medicin']) . "%'";
		}

		if (isset($data['filter_precausion']) && !is_null($data['filter_precausion'])) {
			$implode[] = "precausion = '" . (int)$data['precausion'] . "'";
		}

		
		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		if ($this->customer->getId()) {
			$sql .= " AND patient_id = ".$this->customer->getId()."";
		}

		//print_r($sql); die;

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY id DESC";
		} else {
			$sql .= " ORDER BY id DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	

		$query = $this->db->query($sql);

		return $query->rows;
	}
	
	
	public function getSearchPrescription($term) {
	$searchTerm = $term;
//get matched data from skills table

$query = $this->db->query("SELECT *  
		FROM " . DB_PREFIX . "doctor_prescription dp
		LEFT JOIN " . DB_PREFIX . "customer c ON dp.doctor_id= c.customer_id WHERE dp.title LIKE '%".$term."%' OR c.firstname LIKE '%".$term."%' ORDER BY dp.title ASC");
return $query->rows;
	}
/** **/
}

?>