<?php
class ModelPatientFamily extends Model {
	public function addFamily($data,$patient_id) { 
		 $this->db->query("INSERT INTO " . DB_PREFIX . "family SET pid = '".$data['patient']."', rid= '".$data['relation']."',p_id = '".$patient_id."' ");
		
	}

   public function patientList(){
	   $customerlist =array();
	   
	   	$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c.customer_group_id = 1";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
	  
	  public function getFamilies($patient_id) {
		if ($patient_id) {
			$sql = "SELECT * FROM " . DB_PREFIX . "family  where (p_id= $patient_id or pid= $patient_id) and approve='yes'";

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			return null;
			}
	}
	
	public function getNotification($patient_id) {
		if ($patient_id) {
			$sql = "SELECT f.*,CONCAT(c.firstname, ' ', c.lastname) AS pname,r.name FROM " . DB_PREFIX . "family f LEFT JOIN " . DB_PREFIX . "customer c ON (f.p_id = c.customer_id) 
			LEFT JOIN " . DB_PREFIX . "family_rel r ON (f.rid = r.rid)
			where f.pid= $patient_id and f.approve='no'";
			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			return null;
			}
	}
	
	
	public function sendMail($email,$message,$sendrname){
	    $this->load->language('patient/mail');
		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		//$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";
        $message .= 'Hello ,' . "\n\n";
		$message .= $sendrname.' Send you new message'."\n";;
		$message .= $message . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($email);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();	
	}
	 public function getPatient($patient_id) {
		if ($patient_id) {
			$sql = "SELECT email,CONCAT(c.firstname, ' ', c.lastname) AS name FROM " . DB_PREFIX . "customer c  where customer_id = $patient_id AND customer_group_id = 1";
			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			return null;
			}
	}
	
	
	 public function getRelation($rid) {
		if ($rid) {
			$sql = "SELECT name FROM " . DB_PREFIX . "family_rel  where rid = $rid ";

			$query = $this->db->query($sql);
			return $query->rows;
		} else {
			return null;
			}
	}
	
	  
	  public function familyRelList(){
	   
	   	$sql = "SELECT * FROM " . DB_PREFIX . "family_rel";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
	  
	public function deleteFamily($fid) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "family WHERE fid = '" . (int)$fid . "'");
		$this->cache->delete('case');
	}
	
	public function updateRequeststatus($fid) {
		$this->db->query("UPDATE " . DB_PREFIX . "family set approve='yes' WHERE fid = '" . (int)$fid . "'");
		$this->cache->delete('family');
	}

public function editDailyEssay ($essay_id, $data) {
			$this->db->query("UPDATE " . DB_PREFIX . "doctor_daily_essay SET title = '" . $data['title'] . "', description = '".$data['description']."' where id = $essay_id");
	}
	
   public function getSearchFamily($term) {
			//$sql = "SELECT * FROM " . DB_PREFIX . "family f JOIN customer as c f.pid = c.customer_id  where name LIKE '%".$term."%' AND doct_id= ".$this->customer->getId()."";
				$sql = "SELECT f.fid,f.rid,f.pid,f.p_id FROM " . DB_PREFIX . "family f LEFT JOIN " . DB_PREFIX . "customer c ON (f.pid = c.customer_id) WHERE c.customer_group_id = '1' AND CONCAT(c.firstname, ' ', c.lastname) LIKE '%".$term."%' and approve='yes'";
			$query = $this->db->query($sql);
			return $query->rows;
	}

	public function getDailyEssay($essay_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_daily_essay WHERE id = '" . (int)$essay_id . "'");
		return $query->row;
	}

		public function getTotalDailyEssay() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "doctor_daily_essay");
		return $query->row['total'];
	}
}