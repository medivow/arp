<?php
class ModelPatientAppointment extends Model {
	
	public function addAppointment($data, $photo) {
		//print_r($data); die;
		$pid="1";
		$status="1";
		$datetime = explode('/',$data['appointment_date']);
		$appo_date = $datetime[0]; 
		//$appo_time = $datetime[1];

		$this->db->query("INSERT INTO " . DB_PREFIX . "appointment SET pid = '" . $this->customer->getId() . "', doc_id = '" . (int)$data['doctor'] . "' , email = '" . $this->db->escape($data['email']) . "' , file_name = '" . $photo . "' , app_date = '" . $appo_date . "',  status = '" . $status . "' , created_at = NOW()");
		
		//$this->db->query("INSERT INTO " . DB_PREFIX . "appointment SET pid = '" . (int)$pid . "', doc_id = '" . (int)$data['doctor'] . "' , email = '" . (int)$data['email'] . "' , file_name = '" . (int)$data['photo'] . "', app_date = '" . $appo_date . "', app_time = '" . $appo_time . "', status = '" . $status . "' , created_at = NOW()");

		return $a_id = $this->db->getLastId();
	}
	
	 public function doctorList(){
	   $customerlist =array();
	   
	   	$sql = "SELECT *, CONCAT(c.firstname, ' ', c.lastname) AS name, cgd.name AS customer_group FROM " . DB_PREFIX . "customer c LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON (c.customer_group_id = cgd.customer_group_id) WHERE cgd.language_id = '" . (int)$this->config->get('config_language_id') . "' and cgd.name='Doctors'";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
	  
	   public function availabilityList($doctor, $customer_id){
		   //echo $doctor."<br>";
		   //echo $customer_id; die;
	   $availabilitylist =array();
	   
	   	$sql = "SELECT * FROM " . DB_PREFIX . "doctor_availability WHERE doctor_id = '" . (int)$customer_id . "'";
		
	    $query = $this->db->query($sql);
	 	return $query->rows;
	  }
	  
	    public function appointmentTime($doctor,$appDate){
		   //echo $doctor."<br>";
		   //echo $customer_id; die;
	   $availabilitylist =array();
	   
	   	$sql = "SELECT app_time FROM " . DB_PREFIX . "appointment WHERE doc_id = '" . (int)$doctor . "' AND app_date='".$appDate."' AND app_time !=''";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
	  
	   public function GetCustomerTitle($id){
		  
	   	$sql = "SELECT CONCAT(firstname,' ',lastname) as full_name FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$id . "'";
	    $query = $this->db->query($sql);
	 
	 	return $query->rows;
	  }
	  
	   public function paid_pay($data,$pid){
		
		  $this->db->query("INSERT INTO " . DB_PREFIX . "appointment SET pid = '" . (int)$pid . "', doc_id = '" . (int)$data['doc_id'] . "' ,location = '" . (int)$data['locationID'] . "', app_date = '" . $data['booking_date'] . "', app_time = '" . $data['timing'] . "', status = '1' , created_at = NOW()");
		


		return $a_id = $this->db->getLastId();
	  }
	  
	  public function offfline_appointment($data,$docId){
		
		  $this->db->query("INSERT INTO " . DB_PREFIX . "appointment SET pid = '" . (int)$data['cus_id'] . "', doc_id = '" . (int)$docId . "', location = '" . (int)$data['locationID'] . "' , app_date = '" . $data['booking_date'] . "', app_time = '" . $data['timing'] . "', status = '1' , created_at = NOW()");
		


		return $a_id = $this->db->getLastId();
	  }
	  
	  public function addFreeConsultation($data,$pid){
		  $filename = $pid.'_'.time().'_'.basename(html_entity_decode($this->request->files['image']['name'], ENT_QUOTES, 'UTF-8'));					
			$directory = DIR_IMAGE . 'free_consultatiion'. '/' . $filename;
		
		  $this->db->query("INSERT INTO " . DB_PREFIX . "free_consultation SET cus_id = '" . (int)$pid . "', mobile = '" .$data['mobile'] . "' , query = '" . $data['query'] . "', status = '1', added_date = NOW()");
		
$a_id = $this->db->getLastId();
if(move_uploaded_file($this->request->files['image']['tmp_name'], $directory )){				

				$image = 'free_consultatiion/' . $filename;

				$this->db->query("UPDATE " . DB_PREFIX . "free_consultation SET  file='".$image."' WHERE id = '" . (int)$a_id . "'");

				

			}					


		return $a_id ;
	  }
	  
}