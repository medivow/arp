<?php
class ModelPatientAttachment extends Model {
	public function addAttachment($data,$doct_id) {
		$filename = $doct_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['attachment']['name'], ENT_QUOTES, 'UTF-8'));
		$catid = '';
		$status = 1;
		
		 $directory = DIR_IMAGE . 'attachment'. '/' . $filename;
		if(move_uploaded_file($this->request->files['attachment']['tmp_name'], $directory )){				
		$image = 'attachment/' . $filename;
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "attachment SET c_id = '".$doct_id."', cat_id = '".$catid."', title= '".$data['title']."', attachment = '".$filename."' ,share = '".$data['share']."', status = '".$status."', created_at = now()");
			}
		 
		
	}


  public function editAttach($attach_id, $data) {
	  
		$catid = '';
		$status = 1;
		$directory = DIR_IMAGE . 'attachment'. '/' . $filename;
		$attach_info=$this->model_patient_attachment->getAttach($attach_id);
		$filename = $attach_info['attachment'];
		if($this->request->files['attachment']['tmp_name']) {
			 $filename = $attach_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['attachment']['name'], ENT_QUOTES, 'UTF-8'));
			 $directory = DIR_IMAGE . 'attachment'. '/' . $filename;
		   if(move_uploaded_file($this->request->files['attachment']['tmp_name'], $directory )){	
		        $old_attach = 'attachment/'. $attach_info['attachment']; 
				$image = 'attachment/' . $filename;
				if(file_exists(DIR_IMAGE.$old_attach))
				{
					unlink(DIR_IMAGE.$old_attach);
				}
				
			}
		}
			
         $this->db->query("UPDATE " . DB_PREFIX . "attachment SET  c_id = '".$doct_id."', cat_id = '".$catid."', title= '".$data['title']."', attachment = '".$filename."' ,share = '".$data['share']."', status = '".$status."', updated_at = now() WHERE id = '" . (int)$attach_id . "'");	
	}

	public function deleteAttach($attach_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "attachment WHERE id = '" . (int)$attach_id . "'");
		$this->cache->delete('case');
	}

	public function getAttach($attach_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attachment WHERE id = '" . (int)$attach_id . "'");

		return $query->row;
	}

	public function getInformations($data = array()) {
		//echo '<pre>';print_r($data); die;
		$implode = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "attachment where c_id='".$this->customer->getId()."'";
		

			$sql .= " ORDER BY id";

		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {

			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}
		
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}
		//echo $sql; die;
				$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getInformationDescriptions($information_id) {
		$information_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $information_description_data;
	}

	public function getInformationStores($information_id) {
		$information_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_store WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_store_data[] = $result['store_id'];
		}

		return $information_store_data;
	}

   	public function getCustomer($cust_id) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$cust_id . "'");

       return $query->row;

	}

	public function getInformationLayouts($information_id) {
		$information_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_layout WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $information_layout_data;
	}

	public function getTotalInformations($cus_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "attachment where c_id='".$cus_id."'");

		return $query->row['total'];
	}

	public function getTotalInformationsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "helthtips_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
	public function getSearchAttach($term) { 
		
			$sql = "SELECT * FROM " . DB_PREFIX . "attachment  where title LIKE '%".$term."%'";

			$query = $this->db->query($sql);

			return $query->rows;
		
	}
}