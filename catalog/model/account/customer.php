<?php
class ModelAccountCustomer extends Model {
	public function addCustomer($data) {
		
		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
if(isset($data['doctor'])) {$customer_group_id = 2;} else{$customer_group_id = 1;}
		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id_reg'] . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', username = '" . $this->db->escape($data['username']) . "',  email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['mobile']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = 1, date_added = NOW()");

		$customer_id = $this->db->getLastId();

		$this->db->query("INSERT INTO " . DB_PREFIX . "address SET customer_id = '" . (int)$customer_id . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', company = '" . $this->db->escape($data['company']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "'");

		$address_id = $this->db->getLastId();
        
		if($this->request->files['image']['name'] and $this->request->files['image']['error']==0){
			$filename = $customer_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['image']['name'], ENT_QUOTES, 'UTF-8'));					
			$directory = DIR_IMAGE . 'user'. '/' . $filename;
			
			if(move_uploaded_file($this->request->files['image']['tmp_name'], $directory )){				
				$image = 'user/' . $filename;
				$this->db->query("UPDATE " . DB_PREFIX . "customer SET  image='".$image."' WHERE customer_id = '" . (int)$customer_id . "'");
				
			}					
		}
		
		
		
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "'");

		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();

		// Send to main admin email if new account email is enabled
		if ($this->config->get('config_account_mail')) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_mail_alert'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		return $customer_id;
	}
    
	public function addUser($data)
	{
			if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}
if(isset($data['doctor'])) {$customer_group_id = 2;} else{$customer_group_id = 1;}
		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET customer_group_id = '" . (int)$data['customer_group_id_reg'] . "', store_id = '" . (int)$this->config->get('config_store_id') . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', username = '" . $this->db->escape($data['username']) . "',  email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['mobile']) . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', approved = 1, date_added = NOW(),added_by = '".$this->customer->getId()."'");

		$customer_id = $this->db->getLastId();
		return $customer_id;
	}
	   
	public function editCustomer($data) {
		$customer_id = $this->customer->getId();

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function editPassword($email, $password) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editCode($email, $code) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	
	public function addCustomerPackage($cust_id, $package) {
		$this->db->query("UPDATE `" . DB_PREFIX . "customer` SET package = '" . $this->db->escape($package) . "' WHERE customer_id =  $cust_id ");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE " . DB_PREFIX . "customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}
	
	public function getCustomerId() {
		$customer_id = $this->db->getLastId();
	}
public function getCustomerUsername($username) {
	
		$query = $this->db->query("SELECT username FROM " . DB_PREFIX . "customer WHERE username = '" . $username . "'");

		return $query->row;
	}
	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}
	public function getCustomerByUsername($username) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE LOWER(username) = '" . $this->db->escape(utf8_strtolower($username)) . "'");

		return $query->row;
	}

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname FROM `" . DB_PREFIX . "customer` WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}
	
	public function getTotalCustomersByUsername($username) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "customer WHERE LOWER(username) = '" . $this->db->escape(utf8_strtolower($username)) . "'");
		return $query->row['total'];
	}

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM " . DB_PREFIX . "customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_ip` WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE " . DB_PREFIX . "customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "customer_login` WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}
	public function updateCustomer($cust_id, $data) 
	{
	$mrMeetingTiming=$data['mrtimefrom'].'-'.$data['mrtimefromto'];
	$counter=0;	
    
	$location=$data['locality'];
	$endtime=$data['etime'];
	$latitude  =  $this->db->escape($data['latitude']) ;
    $longitude =  $this->db->escape($data['longitude']);
	
	 $time1  = date("H:i", strtotime($data['stime']));
	 $time2  = date("H:i", strtotime($data['etime']));
	  
	 $drtiming = $time1.'-'.$time2;
	$availability = implode(",", $data['availability']);
	$sql = "INSERT INTO `" . DB_PREFIX . "doctor_availability` (doctor_id, locality, timing, status, latitude, longitude, availability) values ('".$cust_id."', '".$location."', '".$drtiming."', '1', '".$latitude."','".$longitude."','".$availability."')"; 
  
	$query = $this->db->query($sql);
	
	/*foreach($data['stime'] as $result ){
    $location=$data['locality'][$counter];
	$endtime=$data['etime'][$counter];
	 $latitude  =  $this->db->escape($data['latitude']) ;
     $longitude =  $this->db->escape($data['longitude']);
    $sql = "INSERT INTO `" . DB_PREFIX . "doctor_availability` (doctor_id, locality, timing, status, latitude, longitude) values ('".$cust_id."', '".$location."', '".$result." - ".$endtime."', '1', '".$latitude."','".$longitude."')"; 
  	$query = $this->db->query($sql);
    $counter++;
    } 
*/

$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_detail SET customer_id = '" . $cust_id . "', about_doctor = '" . $this->db->escape($data['about_me']) . "', mr_timimg = '" . $this->db->escape($mrMeetingTiming) . "', ima_number = '" . $this->db->escape($data['ima']) . "', contact = '" . $this->db->escape($data['telephone']) . "', city = '" . $this->db->escape($data['city']) . "', country = '" . $this->db->escape($data['country']) . "', availability = '" . $availability . "'");

		$this->db->query("UPDATE " . DB_PREFIX . "customer SET about_me = '" . $this->db->escape($data['about_me']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', availability = '" . $availability . "', fax = '" . $this->db->escape($data['fax']) . "', city = '" . $this->db->escape($data['city']) . "', country = '" . $this->db->escape($data['country']) . "' WHERE customer_id = '" . (int)$cust_id . "'");
		
		if (isset($data['speciality'])) {
			
			
				$this->db->query("INSERT INTO " . DB_PREFIX . "customer_speciality SET customer_id = '" . (int)$cust_id . "', speciality_id = '" . $this->db->escape($data['speciality']) . "'");
					}
	}
	
	public function getCustomerGroup() {
		$query = $this->db->query("SELECT customer_group_id FROM `" . DB_PREFIX . "customer_group_description` WHERE name = 'Doctors'");

		return $query->row;
	}
	public function getCustomerParientGroup() {
		$query = $this->db->query("SELECT customer_group_id FROM `" . DB_PREFIX . "customer_group_description` WHERE name = 'Default'");

		return $query->row;
	}
	
	public function getSpeciality() {
		$query = $this->db->query("SELECT id,title FROM " . DB_PREFIX . "speciality WHERE status = '1'");

		return $query->rows;
	}
	public function replogin($login)
	{
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "doctor_availability` WHERE reception_username = '".strtolower($login['username'])."' AND reception_password = md5('".$login['password']."')");
		return $query->row;
	}
}
