<?php
class ModelSearchSearch extends Model {
	public function getInformation($information_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE i.information_id = '" . (int)$information_id . "' AND id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1'");

		return $query->row;
	}

	public function getInformations() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information i LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id) LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' AND i2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND i.status = '1' ORDER BY i.sort_order, LCASE(id.title) ASC");

		return $query->rows;
	}

	public function getInformationLayoutId($information_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "information_to_layout WHERE information_id = '" . (int)$information_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");

		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}
	
	/*
	public function getDoctors() {
	$query = $this->db->query("SELECT tblcus.*,docdetial.*,cusspec.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' AND tblcus.approved = '1'");
	return $query->rows;
	}
	*/
	
	///////////////

public function getSpacial_id($spname){
$query = $this->db->query("SELECT id FROM " . DB_PREFIX . "speciality WHERE title = '".$spname."'");
 return $query->rows;
	}
	

public function getDoctorsList() {

	
	$query = $this->db->query("SELECT tblcus.customer_id as doctor_id,tblcus.*,docdetial.*,cusspec.*,doc_av.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_availability doc_av ON (tblcus.customer_id = doc_av.doctor_id) 
WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' AND tblcus.approved = '1' ");
	
 return $query->rows;
 
  }
 public function getDoctors($data) {
	 
	  
	$sql="SELECT tblcus.customer_id as doc_id,tblcus.*,docdetial.*,cusspec.*,doc_av.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_availability doc_av ON (tblcus.customer_id = doc_av.doctor_id) WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' ";
	
	if(!empty($data['locality']))
	{
		
	//$sql .= "  AND doc_av.locality LIKE '%".$data['locality']."%' ";
	//$sql .= "  AND distance<=10";
	$sql .= " AND GetDistance(".$data['latitude'].", ".$data['longitude'].", doc_av.latitude,doc_av.longitude) <=10 ";
	
	}
	
	if(!empty($data['res']))
	{
		
	//$sql .= "  AND doc_av.locality LIKE '%".$data['locality']."%' ";
	//$sql .= "  AND distance<=10";
	$sql .= " AND tblcus.firstname LIKE '%".$data['res']."%'  ";
	$sql .= " OR tblcus.lastname LIKE '%".$data['res']."%'  ";
	
	}
	
	
	if(!empty($data['spaci']))
	{
		
	$sql .= "  AND cusspec.speciality_id='".$data['spaci']."' ";
	
	}
	if(!empty($data['doctorID']))
	{
		
	$sql .= "  AND tblcus.customer_id='".$data['doctorID']."' ";
	$sql .= " GROUP BY tblcus.customer_id";
	
	}
	//print_r($data); 
	//echo $sql; die;
	$query = $this->db->query($sql);
	
	//$query = $this->db->query("SELECT tblcus.*,docdetial.*,cusspec.*,doc_av.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_availability doc_av ON (tblcus.customer_id = doc_av.doctor_id) 
	
	
	//WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' AND tblcus.approved = '1' AND doc_av.locality LIKE '%".$locality."%' AND cusspec.speciality_id='".$spaci."'");
	
 return $query->rows;
 
  }
	
	///////////////


 public function getDoctorsPatient($doc_id) {

	$sql="SELECT tblcus.customer_id as cus_id,tblcus.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id)   LEFT JOIN " . DB_PREFIX . "appointment doc_ap ON (tblcus.customer_id = doc_ap.pid) WHERE cusgrpdes.name != '" . strtolower ('doctors') . "'  AND tblcus.status = '1' ";
	if(!empty($doc_id))
	{
	$sql .= "  AND (tblcus.added_by='".$doc_id."' or doc_ap.doc_id='".$doc_id."') ";
	$sql .= " GROUP BY tblcus.customer_id";
	}
	if(!empty($cus_Id))
	{
	$sql .= "  AND (tblcus.added_by='".$cus_Id."' or doc_ap.doc_id='".$cus_Id."') ";
	}
	$query = $this->db->query($sql);
	 return $query->rows;
  }	
  
  public function getPatientDetail($data) {

	$sql="SELECT tblcus.customer_id as cus_id,tblcus.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id)   LEFT JOIN " . DB_PREFIX . "appointment doc_ap ON (tblcus.customer_id = doc_ap.pid) WHERE cusgrpdes.name != '" . strtolower ('doctors') . "'  AND tblcus.status = '1' ";
	if(!empty($doc_id))
	{
	$sql .= "  AND (tblcus.added_by='".$doc_id."' or doc_ap.doc_id='".$doc_id."') ";
	$sql .= " GROUP BY tblcus.customer_id";
	}
	if(!empty($data['cus_Id']))
	{
	$sql .= "  AND (tblcus.customer_id='".$data['cus_Id']."') ";
	}
	$sql .= " GROUP BY tblcus.customer_id";
	$query = $this->db->query($sql);
	 return $query->rows;
  }	

public function getPatients() {
		$query = $this->db->query("SELECT tblcus.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) WHERE cusgrpdes.name != '" . strtolower ('doctors') . "'  AND tblcus.status = '1' AND tblcus.approved = '1'");

		return $query->rows;
	}
public function getCustomerSpecilityById($customer_id) {
			$doctor_service_data = array();
	       $query = $this->db->query("SELECT splct.title FROM " . DB_PREFIX . "customer_speciality cusspelty LEFT JOIN " . DB_PREFIX . "speciality splct ON(cusspelty.speciality_id=splct.id) WHERE customer_id = '" . (int)$customer_id . "'");
	
			foreach ($query->rows as $result) {
				$doctor_service_data[] = $result['title'];
			}

			return $doctor_service_data;
		}
		
public function getAvailabilityDays($days,$stime,$etime) {
	
	$sql="SELECT tblcus.customer_id as doc_id,tblcus.*,docdetial.*,cusspec.*,doc_av.* FROM " . DB_PREFIX . "customer tblcus LEFT JOIN " . DB_PREFIX . "customer_group_description cusgrpdes ON (tblcus.customer_group_id = cusgrpdes.customer_group_id) LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (tblcus.customer_id = cusspec.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (tblcus.customer_id = docdetial.customer_id) LEFT JOIN " . DB_PREFIX . "doctor_availability doc_av ON (tblcus.customer_id = doc_av.doctor_id) WHERE cusgrpdes.name = '" . strtolower ('doctors') . "'  AND tblcus.status = '1' ";
	if(!empty($days))
	{
		for($i=0;$i<=count($days)-1;$i++){
			if($i==0){$key="AND";} else {$key="OR";}
	$sql .= "  ".$key." FIND_IN_SET('".$days[$i]."', tblcus.availability)";
		}}
		
	$stime = str_replace(" AM", "", $stime);
	$stime = str_replace(" PM", "", $stime);
	$stime = str_replace(" am", "", $stime);
	$stime = str_replace(" pm", "", $stime);
	
	
	
	$etime = str_replace(" AM", "", $etime);
	$etime = str_replace(" PM", "", $etime);
	$etime = str_replace(" am", "", $etime);
	$etime = str_replace(" pm", "", $etime);
	
		$sql .= "  AND doc_av.timing BETWEEN '".$stime."' AND '".$etime."'";
	//echo $sql; die;
	$query = $this->db->query($sql);
 return $query->rows;
 
  }
}