<?php
class ModelDoctorDoctor extends Model {
	
	public function getEvents($data = array()) {
		$sql = "SELECT *  
		FROM " . DB_PREFIX . "appointment e
			LEFT JOIN " . DB_PREFIX . "customer u ON u.customer_id = e.pid
			
		WHERE 1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "e.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(e.created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}
if (isset($data['doctor_id']) && !is_null($data['doctor_id'])) {
			$sql .= " AND e.doc_id = ".$data['doctor_id']."";
		}if (isset($data['doctor_id']) && !is_null($data['doctor_id'])) {
			$sql .= " AND e.doc_id = ".$data['doctor_id']."";
		}
		if ($this->customer->getId()) {
			$sql .= " AND e.doc_id = ".$this->customer->getId()."";
		}

		

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);

		

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY e.aid DESC";
		} else {
			$sql .= " ORDER BY e.aid DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	

		$query = $this->db->query($sql);

		return $query->rows;
	}
public function getCustomerByUsername($username) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE LCASE(username) = '" . $this->db->escape(utf8_strtolower($username)) . "'");

		return $query->row;
	}
	public function getTotalEvents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event e WHERE  1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if($this->user->user_group_id == JUDGE_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_judge WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if($this->user->user_group_id == STAFF_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_staff WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalEventsByEventGroupId($scoring_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event WHERE scoring_group_id = '" . (int)$scoring_group_id . "'");

		return $query->row['total'];
	}
	
	
	public function add_clinic($cust_id, $data) {
	  $mrMeetingTiming = $data['mr_start_time'].'-'.$data['mr_end_time'];
      $location = $data['lcoality'];
	  $addres = $data['addres'];
	  
	  $time1  = date("H:i", strtotime($data['dr_start_tm']));
	   $time2  = date("H:i", strtotime($data['dr_end_tm']));
	
	  
	  $drtiming = $time1.'-'.$time2;
	  
	 // $drtiming = $data['dr_start_tm'].'-'.$data['dr_end_tm'];
	  
	  
	  
	  $latitude  =  $this->db->escape($data['latitude']) ;
      $longitude =  $this->db->escape($data['longitude']);
	  $rec_username=$data['rec_username'];
	  $rec_pwd=$data['rec_pwd'];
	 
    $sql = "INSERT INTO `" . DB_PREFIX . "doctor_availability` (doctor_id, locality, timing, mr_metting, address, status, latitude, longitude, reception_username, reception_password ) values ('".$cust_id."', '".$location."', '".$drtiming."', '".$mrMeetingTiming."', '".$addres."','1', '".$latitude."','".$longitude."','".strtolower($rec_username)."','" . md5($rec_pwd) . "')"; 
	$query = $this->db->query($sql);
    
	}
	
	public function deleteClinic($del_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_availability WHERE id = '".(int)$del_id."'");
		//$this->cache->delete('case');
	}
	
	
	public function getSpaciality($data){
	  $keys=$data['keyword'];
      if($keys!=''){
	  $dat= $this->db->query("SELECT * FROM ".DB_PREFIX."speciality WHERE title LIKE '%".$keys."%' limit 0,8");
	  return $dat->rows; } 
	}
	
	public function checkusrname($data){
	     
	  $dat= $this->db->query("SELECT count(*) as count FROM ".DB_PREFIX."doctor_availability WHERE reception_username='" . strtolower($data["username"]) . "'");
	  
	  return $dat->row['count']; 
	}
	
}