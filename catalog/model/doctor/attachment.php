<?php
class ModelDoctorAttachment extends Model {
	public function addAttachment($data,$doct_id) {
		$filename = $doct_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['attachment']['name'], ENT_QUOTES, 'UTF-8'));
		$catid = '';
		$status = 1;
         $share = 0;
		 $directory = DIR_IMAGE . 'attachment'. '/' . $filename;
	
		if(move_uploaded_file($this->request->files['attachment']['tmp_name'], $directory )){				
		$image = 'attachment/' . $filename;
		$this->db->query("INSERT INTO " . DB_PREFIX . "attachment SET c_id = '".$doct_id."', cat_id = '".$catid."', title= '".$data['title']."', attachment = '".$filename."' ,share = '".$share."', status = '".$status."', created_at = now()");
			}
		 
		
	}

   public function addAttachmentdocp($data,$doct_id,$userid) {
		
		
		$catid = '';
		$status = 1;
        $share = 0;
		//$directory = DIR_IMAGE . 'attachment'. '/' . $filename;
	
		$adoct_id=$this->customer->getId();
		
		
		
		foreach($_FILES['images']['name'] as $key=>$val)
		{
		
		
		$image_name = $_FILES['images']['name'][$key];
		$tmp_name 	= $_FILES['images']['tmp_name'][$key];
		$size 		= $_FILES['images']['size'][$key];
		$type 		= $_FILES['images']['type'][$key];
		$error 		= $_FILES['images']['error'][$key];
		//checking image type
		$allowed =  array('gif','png','jpg','jpeg','JPEG');
		$filename = $_FILES['images']['name'][$key];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(in_array($ext,$allowed)){
		//move uploaded file to uploads folder
		$target_dir = $directory = DIR_IMAGE . 'attachment'. '/';
		$filename = $adoct_id.'_'.time().'_'.basename(html_entity_decode($_FILES['images']['name'][$key], ENT_QUOTES, 'UTF-8'));
		
		$target_file = $target_dir.$filename;
		
		
		if(move_uploaded_file($_FILES['images']['tmp_name'][$key],$target_file))
		{
			
			$images_arr[] = $target_file;
		
			$this->db->query("INSERT INTO " . DB_PREFIX . "attachment_pat SET p_id = '".$userid."', d_id = '".$adoct_id."', title= '', attachment = '".$filename."' ,share = '".$share."', status = '".$status."', created_at = now()");
		
		}
		
		}
		
		
		$error="Image type not valid";
		
	  
	  
	  
	  }
		
		
		
		
		//$filename = $doct_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['attachment']['name'], ENT_QUOTES, 'UTF-8'));
		
		//if(move_uploaded_file($this->request->files['attachment']['tmp_name'], $directory ))
		//{				
		//$image = 'attachment/' . $filename;
		//$this->db->query("INSERT INTO " . DB_PREFIX . "attachment SET c_id = '".$doct_id."', cat_id = '".$catid."', title= '".$data['title']."', attachment = '".$filename."' ,share = '".$share."', status = '".$status."', created_at = now()");
		//}
		 
		
	}
	
	public function getpatattached($pid,$did)
	{
		$sql = "SELECT * FROM " . DB_PREFIX . "attachment_pat  where p_id='".$pid."' AND d_id='".$did."'";

		$query = $this->db->query($sql);
		return $query->rows;
	}
	
  public function editAttach($attach_id, $data,$doct_id) {
		$catid = '';
		$status = 1;
		$share = 0;
		$directory = DIR_IMAGE . 'attachment'. '/' . $filename;
		$attach_info=$this->model_doctor_attachment->getAttach($attach_id);
		$filename = $attach_info['attachment'];
		if($this->request->files['attachment']['tmp_name']) {
			 $filename = $attach_id.'_'.time().'_'.basename(html_entity_decode($this->request->files['attachment']['name'], ENT_QUOTES, 'UTF-8'));
			 $directory = DIR_IMAGE . 'attachment'. '/' . $filename;
		   if(move_uploaded_file($this->request->files['attachment']['tmp_name'], $directory )){	
		        $old_attach = 'attachment/'. $attach_info['attachment']; 
				$image = 'attachment/' . $filename;
				if(file_exists(DIR_IMAGE.$old_attach))
				{
					unlink(DIR_IMAGE.$old_attach);
				}
				
			}
		}
         $this->db->query("UPDATE " . DB_PREFIX . "attachment SET  c_id = '".$doct_id."', cat_id = '".$catid."', title= '".$data['title']."', attachment = '".$filename."' ,share = '".$share."', status = '".$status."', updated_at = now() WHERE id = '" . (int)$attach_id . "'");	
	}

	public function deleteAttach($attach_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "attachment WHERE id = '" . (int)$attach_id . "'");
		$this->cache->delete('case');
	}

	public function getAttach($attach_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "attachment WHERE id = '" . (int)$attach_id . "'");

		return $query->row;
	}

	public function getInformations($data = array(),$doct_id) {
		$implode = array();
		$sql = "SELECT * FROM " . DB_PREFIX . "attachment where c_id='".$doct_id."'";
		

			$sql .= " ORDER BY id";

		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {

			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}
		
		if (isset($data['start']) || isset($data['limit'])) {

			if ($data['start'] < 0) {

				$data['start'] = 0;

			}



			if ($data['limit'] < 1) {

				$data['limit'] = 20;

			}



			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];

		}
				$query = $this->db->query($sql);



		return $query->rows;
	}

	public function getInformationDescriptions($information_id) {
		$information_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $information_description_data;
	}

	public function getInformationStores($information_id) {
		$information_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_store WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_store_data[] = $result['store_id'];
		}

		return $information_store_data;
	}

   	public function getCustomer($cust_id) {

		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$cust_id . "'");

       return $query->row;

	}

	public function getInformationLayouts($information_id) {
		$information_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_layout WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $information_layout_data;
	}

	public function getTotalInformations($cus_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "attachment where c_id='".$cus_id."'");

		return $query->row['total'];
	}

	public function getTotalInformationsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "helthtips_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
	public function getSearchAttach($term,$doct_id) { 
		
			$sql = "SELECT * FROM " . DB_PREFIX . "attachment  where title LIKE '%".$term."%' and c_id='".$doct_id."'";

			$query = $this->db->query($sql);

			return $query->rows;
		
	}
}