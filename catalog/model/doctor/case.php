<?php
class ModelDoctorcase extends Model {
	public function addCase($data,$doct_id) {
		 $this->db->query("INSERT INTO " . DB_PREFIX . "doctor_case_study SET doct_id = '".$doct_id."', title= '".$data['title']."',description = '".$data['description']."' ");
		
	}


public function editCase($case_id, $data) {
		/*$this->db->query("UPDATE " . DB_PREFIX . "helthtips SET sort_order = '" . (int)$data['sort_order'] . "', bottom = '" . (isset($data['bottom']) ? (int)$data['bottom'] : 0) . "', status = '" . (int)$data['status'] . "' WHERE information_id = '" . (int)$information_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "helthtips_description WHERE information_id = '" . (int)$information_id . "'");*/

	
			$this->db->query("UPDATE " . DB_PREFIX . "doctor_case_study SET title = '" . $data['title'] . "', description = '".$data['description']."' where id = $case_id");
	

		
	}

	public function deleteCase($case_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_case_study WHERE id = '" . (int)$case_id . "'");
		$this->cache->delete('case');
	}

	public function getCase($case_id) {
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_case_study WHERE id = '" . (int)$case_id . "'");

		return $query->row;
	}

	public function getInformations($doct_id,$data) {
		
		if ($doct_id) {
			
			$sql = "SELECT * FROM " . DB_PREFIX . "doctor_case_study  where doct_id= $doct_id ORDER BY title";

			/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {

			$sql .= " ORDER BY title";

		}*/

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);
       
		return $query->rows;
		} else {
			$information_data = $this->cache->get('information.' . (int)$this->config->get('config_language_id'));

			if (!$information_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips i LEFT JOIN " . DB_PREFIX . "helthtips_description id ON (i.information_id = id.information_id) WHERE id.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY id.title");

				$information_data = $query->rows;

				$this->cache->set('information.' . (int)$this->config->get('config_language_id'), $information_data);
			}
			

			return $information_data;
		}
	}

	public function getInformationDescriptions($information_id) {
		$information_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_description WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_description_data[$result['language_id']] = array(
				'title'            => $result['title'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword']
			);
		}

		return $information_description_data;
	}

	public function getInformationStores($information_id) {
		$information_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_store WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_store_data[] = $result['store_id'];
		}

		return $information_store_data;
	}

	public function getInformationLayouts($information_id) {
		$information_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "helthtips_to_layout WHERE information_id = '" . (int)$information_id . "'");

		foreach ($query->rows as $result) {
			$information_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $information_layout_data;
	}

	public function getTotalInformations($doct_id,$data) {
				
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "doctor_case_study  where doct_id= $doct_id ORDER BY title";

			/*if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {

			$sql .= " ORDER BY title";

		}*/

		
		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}
			
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}

	public function getTotalInformationsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "helthtips_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
	public function getSearchCase($term) {
		
			$sql = "SELECT * FROM " . DB_PREFIX . "doctor_case_study  where title LIKE '%".$term."%' AND doct_id= ".$this->customer->getId()."";

			$query = $this->db->query($sql);

			return $query->rows;
		
	}
}