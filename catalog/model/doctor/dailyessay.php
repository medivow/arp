<?php
class ModelDoctorDailyessay extends Model {
	public function addDailyEssay($data,$doct_id) {
		 $this->db->query("INSERT INTO " . DB_PREFIX . "doctor_daily_essay SET doct_id = '".$doct_id."', title= '".$data['title']."',description = '".$data['description']."' ");
		
	}


public function editDailyEssay ($essay_id, $data) {
			$this->db->query("UPDATE " . DB_PREFIX . "doctor_daily_essay SET title = '" . $data['title'] . "', description = '".$data['description']."' where id = $essay_id");
	

		
	}

	public function deleteDailyEsay($essay_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_daily_essay WHERE id = '" . (int)$essay_id . "'");
		$this->cache->delete('case');
	}

	public function getDailyEssay($essay_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_daily_essay WHERE id = '" . (int)$essay_id . "'");

		return $query->row;
	}

	public function getDailyEssays($doct_id,$data) {
		if ($doct_id) {
			$sql = "SELECT * FROM " . DB_PREFIX . "doctor_daily_essay  where doct_id= $doct_id ORDER BY title";
            if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";

		} else {

			$sql .= " ASC";

		}
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}
			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			return null;
			}
	}


		public function getTotalDailyEssay($doc_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "doctor_daily_essay where doct_id='".$doc_id."'");

		return $query->row['total'];
	}
	
	public function getSearchDailyessay($term) {
		
			$sql = "SELECT * FROM " . DB_PREFIX . "doctor_daily_essay  where title LIKE '%".$term."%' AND doct_id= ".$this->customer->getId()."";

			$query = $this->db->query($sql);

			return $query->rows;
		
	}

	
}