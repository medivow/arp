<?php
class ModelDoctorPatients extends Model {
	
	public function getPatients($data = array()) {
		$sql = "SELECT c.customer_id as doc_id,c.*,docdetial.*,cusspec.*,csapp.fee 
		FROM " . DB_PREFIX . "customer c
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON c.customer_group_id = cgd.customer_group_id
			LEFT JOIN " . DB_PREFIX . "appointment csapp ON c.customer_id = csapp.pid
			LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (c.customer_id = cusspec.customer_id) 
			LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (c.customer_id = docdetial.customer_id)
		WHERE cgd.name!='Doctors' and csapp.doc_id=".$data['doctor_id']."";
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "e.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(e.created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		/*if ($this->customer->getId()) {
			$sql .= " AND c.customer_id = ".$this->customer->getId()."";
		}*/

		

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);

		 $sql .= " GROUP BY c.customer_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY c.customer_id DESC";
		} else {
			$sql .= " ORDER BY c.customer_id DESC";
		}
        
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	
		
		$query = $this->db->query($sql);
		return $query->rows;
	}

	public function getPatientstotal($data = array()) {
		$sql = "SELECT COUNT(*) AS total  
		FROM " . DB_PREFIX . "customer c
			LEFT JOIN " . DB_PREFIX . "customer_group_description cgd ON c.customer_group_id = cgd.customer_group_id
			LEFT JOIN " . DB_PREFIX . "appointment csapp ON c.customer_id = csapp.pid
			LEFT JOIN " . DB_PREFIX . "customer_speciality cusspec ON (c.customer_id = cusspec.customer_id) 
			LEFT JOIN " . DB_PREFIX . "doctor_detail docdetial ON (c.customer_id = docdetial.customer_id)
		WHERE cgd.name!='Doctors' and csapp.doc_id=".$data['doctor_id']."";
		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "e.status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(e.created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		/*if ($this->customer->getId()) {
			$sql .= " AND c.customer_id = ".$this->customer->getId()."";
		}*/

		

		$sort_data = array(
			'event_name',
			'e.status',
			'e.created_on'
		);

		 $sql .= " GROUP BY c.customer_id";

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " ORDER BY c.customer_id DESC";
		} else {
			$sql .= " ORDER BY c.customer_id DESC";
		}
        
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
	
		
		
		$query = $this->db->query($sql);
		
		//echo count($query->rows);
		
		return count($query->rows);
	}
	
	public function getTotalEvents($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event e WHERE  1 ";

		$implode = array();

		if (!empty($data['filter_name'])) {
			$implode[] = "event_name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$implode[] = "status = '" . (int)$data['filter_status'] . "'";
		}

		if (!empty($data['filter_created_on'])) {
			$implode[] = "DATE(created_on) = DATE('" . $this->db->escape($data['filter_created_on']) . "')";
		}

		if($this->user->user_group_id == JUDGE_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_judge WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if($this->user->user_group_id == STAFF_GROUP_ID){
			$sql .= " AND e.div_id IN( SELECT cdv_id FROM oc_competition_staff WHERE user_id = '".$this->user->getId()."' AND status = 1 )";
		}

		if ($implode) {
			$sql .= " AND " . implode(" AND ", $implode);
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalEventsByEventGroupId($scoring_group_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "competition_event WHERE scoring_group_id = '" . (int)$scoring_group_id . "'");

		return $query->row['total'];
	}
	public function addPrescription($doctor_id, $patient_id, $data) {
		//print_r($data['dose']); die;
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_prescription SET patient_id = '" . (int)$patient_id . "', doctor_id = '" . (int)$doctor_id . "', title = '" . $this->db->escape($data['title']) . "',  precausion = '" . $this->db->escape($data['precausion']) . "', diet = '" . $this->db->escape($data['diet']) . "', description = '" . $this->db->escape($data['description']) . "',added_date = NOW()");
		$prescription_id = $this->db->getLastId();
		$i=0;
		if (isset($data['medicin'])&& !empty($this->request->post['medicin'])) {

			foreach ($data['medicin'] as $mned) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "prescription_detail SET prescription_id = '" . (int)$prescription_id . "', medicine = '" . $mned . "', dose = '" . $data['dose'][$i] . "',medicine_timing='".$data['timing'][$i]."'");
				$i=$i+1;
			}
		}
		$j=0;
		if (isset($data['medial_test'])&& !empty($this->request->post['medial_test'])) {

			foreach ($data['medial_test'] as $test) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "medical_test SET prescription_id = '" . (int)$prescription_id . "', title = '" . $test . "'");
				$i=$i+1;
			}
		}
	}
	
	
	public function getPrescription($did, $pid) {
		$query= $this->db->query("SELECT dp.*,CONCAT(c.firstname,' ',c.lastname) as pat_name 
		FROM " . DB_PREFIX . "doctor_prescription  as dp
		LEFT JOIN " . DB_PREFIX . "customer c ON dp.patient_id = c.customer_id
		
		where doctor_id=$did AND patient_id=$pid GROUP BY dp.id ORDER BY dp.id DESC");

		return $query->rows;
	}
	/*public function getPrescriptionDetail($presID) {
		$query= $this->db->query("SELECT * 
		FROM " . DB_PREFIX . "prescription_detail  
		where prescription_id=$presID");
		return $query->rows;
	}*/
	public function getPatientDetail($pid) {
		$query= $this->db->query("SELECT u.customer_id as cus_id , u.*  
		FROM " . DB_PREFIX . "customer u
			WHERE customer_id=$pid");

		return $query->rows;
	}
		public function getDoctor($pid) {
		$query= $this->db->query("SELECT *  
		FROM " . DB_PREFIX . "customer u
			LEFT JOIN " . DB_PREFIX . "doctor_patients dp ON u.customer_id = dp.did where dp.pid IN($pid)");

		return $query->rows;
	}
	
	public function getSearchPatient($term) {
	$searchTerm = $term;
//get matched data from skills table

		$sql = "SELECT *  
		FROM " . DB_PREFIX . "doctor_patients dp
			LEFT JOIN " . DB_PREFIX . "customer c ON c.customer_id = dp.pid
			WHERE ";
			
			
			$sql .= " (c.firstname LIKE '%".$term."%' OR c.telephone LIKE '%".$term."%')";
		
		if ($this->customer->getId()) {
			$sql .= " AND dp.did = ".$this->customer->getId()."";
		}	
		//echo $sql; die;
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	public function getMedicine($cid,$data){
	  $keys=$data['keyword'];
      if($keys!=''){
	  $dat= $this->db->query("SELECT p.product_id,pd.name  FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.name LIKE '%".$keys."%' AND p2c.category_id = '" . (int)$cid . "' ");
	  return $dat->rows; } 
	}
	
	public function getPrescriptionDetail($data) {
		$sql = "SELECT *  
		FROM " . DB_PREFIX . "prescription_detail pd  WHERE prescription_id='".$data['id']."'";
        
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	public function getPrescriptionInfo($data) {
		$sql = "SELECT dp.*,pd.medicine,pd.dose,pd.medicine_timing  
		FROM " . DB_PREFIX . "doctor_prescription dp 
		LEFT JOIN " . DB_PREFIX . "prescription_detail pd ON dp.id = pd.prescription_id
		 WHERE dp.id='".$data['id']."'";
        
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	public function getMedicalTest($data) {
		$sql = "SELECT mt.id,mt.title  
		FROM " . DB_PREFIX . "medical_test mt 
		LEFT JOIN " . DB_PREFIX . "doctor_prescription dp ON mt.prescription_id = dp.id
		 WHERE mt.prescription_id='".$data['id']."'";
        
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
	
	public function getPatientInfoByPrescriptionId($id) {
		$sql = "SELECT c.email  
		FROM " . DB_PREFIX . "customer c 
		LEFT JOIN " . DB_PREFIX . "doctor_prescription dp ON c.customer_id = dp.patient_id
		 WHERE dp.id='".$id."'";
        
		$query = $this->db->query($sql);

		return $query->rows;
		
	}
}