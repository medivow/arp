<?php
class ControllerPatientPrescription extends Controller {
	
public function index() 
	{
		$this->load->language('patient/prescription');
		$this->load->model('patient/prescription');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['model_heading_title'] = $this->language->get('model_heading_title');
		$data['model_pres_title'] = $this->language->get('model_pres_title');
		$data['model_pres_medicine'] = $this->language->get('model_pres_medicine');
		$data['model_pres_precautions'] = $this->language->get('model_pres_precautions');
		$data['model_pres_diet'] = $this->language->get('model_pres_diet');
		$data['model_pres_description'] = $this->language->get('model_pres_description');
		$data['model_pres_adddate']=$this->language->get('model_pres_adddate');
		$data['model_pres_docname']=$this->language->get('model_pres_docname');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
        $filter_data = array('customer_id'=>$this->customer->getId());
		$results = $this->model_patient_prescription->getPrescriptions($filter_data);
			if(!empty($results)){
				$data['prescription']=$results;
				}
			else{
			$data['informations'][]='NO RESULTS FOUND';
				}
		//////////////// Show In View page all data ///////////////////			
		 $this->response->setOutput($this->load->view('patient/prescription', $data));
	 
	 	}
		
		public function search() 
	{
		$this->load->model('patient/doctor');
		$this->load->model('search/search');
		$this->load->language('patient/doctor');
		$this->load->language('patient/prescription');
		$this->load->model('patient/prescription');
		
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		//echo $this->request->get['term']; 
		$results = $this->model_patient_prescription->getSearchPrescription($this->request->get['term']);
		$return='';
	
	foreach($results as $result) {
		$i=1;
		$return.= '<tr><td class="sorting_1">'. $result['title']. '</td>
							<td>' .$result['firstname'].' '.$result['lastname'].'</td>
                          <td>' .$result['medicin'].'</td>
                          <td>' .$result['precausion']. '</td>
                          <td>' .$result['diet']. '</td>
						   <td><a href="" data-toggle="modal" data-target="#myModal' .$i. '">Detail</a></td>
                          </tr>';
						
	$i++;}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
	}
