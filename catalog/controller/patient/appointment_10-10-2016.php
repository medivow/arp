<?php
class ControllerPatientAppointment extends Controller {
private $error = array();	
public function index() 

	{
       $this->load->language('patient/appointment');
	   $this->load->model('patient/appointment');
	   $data['success'] = '';
	   if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$appointment_id = $this->model_patient_appointment->addAppointment($this->request->post);
			$data['success'] = $this->language->get('success_appoint');
		}
	   
	   $data['customerlist'] = $this->model_patient_appointment->doctorList();
	   $data['column_left'] = $this->load->controller('common/user_left');
		
		/*----------------lables--------------*/
		$data['entry_consult_doctor'] = $this->language->get('entry_consult_doctor');	
        $data['entry_doctors'] = $this->language->get('entry_doctors');	
		$data['entry_appointment_date'] = $this->language->get('entry_appointment_date');
		$data['entry_about_patient'] = $this->language->get('entry_about_patient');
		$data['entry_submit'] = $this->language->get('entry_submit');
		/*---------------lables---------------*/
		
		
		if (isset($this->error['doctor'])) {
			$data['error_doctor'] = $this->error['doctor'];
		} else {
			$data['error_doctor'] = '';
		}
		
		if (isset($this->error['appointment_date'])) {
			$data['error_appointment_date'] = $this->error['appointment_date'];
		} else {
			$data['error_appointment_date'] = '';
		}
		
		if (isset($this->error['about_patient'])) {
			$data['error_about_patient'] = $this->error['about_patient'];
		} else {
			$data['error_about_patient'] = '';
		}
		
		$data['action'] = $this->url->link('patient/appointment', '', true);
		
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/consult-doctor', $data));

	}
	

  private function validate() {
         
		 if ($this->request->post['doctor'] == '') {
			$this->error['doctor'] = $this->language->get('error_doctor');
		 }
		 
		  if ($this->request->post['appointment_date'] == '') {
			$this->error['appointment_date'] = $this->language->get('error_appointment_date');
		}
		 
		if ((utf8_strlen(trim($this->request->post['about_patient'])) < 3) || (utf8_strlen(trim($this->request->post['about_patient'])) > 200)) {
			$this->error['about_patient'] = $this->language->get('error_about_patient');
		}
				
		return !$this->error;
	}
	
	public function package() 

	{
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/package', $data));
	}
	
	public function free() 

	{
		
		 $this->load->model('patient/appointment');
		$data['customerlist'] = $this->model_patient_appointment->doctorList();
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/free', $data));
	}
	
	public function paid() 

	{
		 $this->load->model('patient/appointment');
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
        $data['customerlist'] = $this->model_patient_appointment->doctorList();
		$this->response->setOutput($this->load->view('patient/paid', $data));
	}

}