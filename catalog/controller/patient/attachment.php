<?php
class ControllerPatientAttachment extends Controller {
	private $error = array();

	public function index() { 
		$this->load->language('patient/attachment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('patient/attachment');
		$data['success'] = '';
	  
	  
	 $this->getList();
		
	}

	public function add() { 
		$this->load->language('patient/attachment');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('patient/attachment');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$doct_id = $this->customer->getId();
			$this->model_patient_attachment->addAttachment($this->request->post, $doct_id);
			
			$this->session->data['success'] = $this->language->get('text_upload');
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/attachment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() { 
		$this->load->language('patient/attachment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('patient/attachment');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_patient_attachment->editAttach($this->request->get['attach_id'], $this->request->post);
			
			

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/attachment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('patient/attachment');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('patient/attachment');

		/*if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $information_id) {
				$this->model_patient_attachment->deleteInformation($information_id);
		}*/
			
           if (isset($this->request->get['attach_id']) ) {
			   $attach_detail=$this->model_patient_attachment->getAttach($this->request->get['attach_id']);
			$file_name=$attach_detail['attachment'];
			$urls = DIR_IMAGE.'attachment'. '/' . $file_name;
			$this->model_patient_attachment->deleteAttach($this->request->get['attach_id']);
			if(file_exists($urls))
			{
				unlink($urls);
			}
			$this->session->data['success'] = $this->language->get('text_delete');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/attachment', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}
	

	protected function getList() { 
		$data['entry_attachment'] = $this->language->get('entry_attachment');
		$data['entry_share'] = $this->language->get('entry_share');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		
		 if (isset($this->session->data['success'])) {
			$data['attachment_update'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['attachment_update'] = '';
		}

		$data['add'] = $this->url->link('patient/attachment/add',  $url, true);
		$data['delete'] = $this->url->link('patient/attachment/delete', $url, true);

		$data['informations'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
         $doct_id = $this->customer->getId();;/// static id for testing only
		$information_total = $this->model_patient_attachment->getTotalInformations($doct_id);
		
		$results = $this->model_patient_attachment->getInformations($filter_data);


		foreach ($results as $result) {
			$urls = $this->config->get('config_url').'image/attachment'. '/' . $result['attachment'];
			$data['informations'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'attachment'     => $result['attachment'],
				'urls'			 => $urls,	
				'created_at'     => $result['created_at'],
				'edit'           => $this->url->link('patient/attachment/edit', 'attach_id=' . $result['id'] . $url, true),
				'delete'           => $this->url->link('patient/attachment/delete', 'attach_id=' . $result['id'] . $url, true),
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_date'] = $this->language->get('column_date');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=DESC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('patient/attachment','sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('patient/attachment', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $information_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('patient/attachment',  $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($information_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($information_total - $this->config->get('config_limit_admin'))) ? $information_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $information_total, ceil($information_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/uheader');
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');

		$this->response->setOutput($this->load->view('patient/attachment', $data));
	}



protected function getForm() {
		
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['information_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_attachment'] = $this->language->get('entry_attachment');
		
		$data['entry_share'] = $this->language->get('entry_share');
		$data['entry_yes'] = $this->language->get('entry_yes');
		$data['entry_no'] = $this->language->get('entry_no');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_store'] = $this->language->get('entry_store');
		$data['entry_bottom'] = $this->language->get('entry_bottom');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_layout'] = $this->language->get('entry_layout');

		$data['help_keyword'] = $this->language->get('help_keyword');
		$data['help_bottom'] = $this->language->get('help_bottom');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_design'] = $this->language->get('tab_design');
         
		 if (isset($this->session->data['success'])) {
			$data['attachment_update'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$data['attachment_update'] = '';
		}
		 
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

       if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}
		

		if (isset($this->error['attachment'])) {
			$data['error_attachment'] = $this->error['attachment'];
		} else {
			$data['error_attachment'] = '';
		}
		
		if (isset($this->error['share'])) {
			$data['error_share'] = $this->error['share'];
		} else {
			$data['error_share'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('patient/attachment', 'token=' . $this->session->data['token'] . $url, true)
		);
*/
		if (!isset($this->request->get['attach_id'])) {
			$data['action'] = $this->url->link('patient/attachment/add', $url, true);
		} else {
			$data['action'] = $this->url->link('patient/attachment/edit',  '&attach_id=' . $this->request->get['attach_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('patient/attachment',  $url, true);
		if (isset($this->request->get['attach_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$case_info = $this->model_patient_attachment->getAttach($this->request->get['attach_id']);
		}
		//$data['token'] = $this->session->data['token'];
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
       if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (isset($case_info['title'])) {
			$data['title'] = $case_info['title'];
		} else {
			$data['title'] = '';
		}
		
		if (isset($this->request->post['share'])) {
			$data['share'] = $this->request->post['share'];
		} elseif (isset($case_info['share'])) {
			$data['share'] = $case_info['share'];
		} else {
			$data['share'] = '';
		}
		
		 if (isset($this->request->post['doct_id'])) {
			$data['doct_id'] = $this->request->post['doct_id'];
		} elseif (isset($case_info['doct_id'])) {
			$data['doct_id'] = $case_info['doct_id'];
		} else {
			$data['doct_id'] = '';
		}
		if (isset($this->request->post['attachment'])) { 
			$data['attachment'] = $this->request->post['attachment'];
		} elseif (isset($case_info['attachment'])) {
			$urls = $this->config->get('config_url').'image/attachment'. '/' . $case_info['attachment']; 
			$data['attachment'] = '<a href="'.$urls.'">'.$case_info['attachment'].'</a>';
		} else {
			$data['attachment'] = '';
		}

        $data['header'] = $this->load->controller('common/uheader');
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$this->response->setOutput($this->load->view('patient/attachment_form', $data));
	}
	
	private function validate() {
         
		 if ($this->request->post['title'] == '') {
			$this->error['title'] = $this->language->get('error_title');
		 }
		 
		  if ($this->request->post['attachment'] == '') {
			$this->error['attachment'] = $this->language->get('error_appointment_date');
		}
		 
		if ((utf8_strlen(trim($this->request->post['about_patient'])) < 3) || (utf8_strlen(trim($this->request->post['about_patient'])) > 200)) {
			$this->error['about_patient'] = $this->language->get('error_about_patient');
		}
				
		return !$this->error;
	}

	protected function validateForm() {
			if ((utf8_strlen($this->request->post['title']) < 3) || (utf8_strlen($this->request->post['title']) > 64)) {
				$this->error['title'] = $this->language->get('error_title');
			}
			
	       if($this->request->get['route'] == 'patient/attachment/edit' ) {
			
		   } else {
			   if (utf8_strlen($this->request->files['attachment']['name']) == '') { 
				$this->error['attachment'] = $this->language->get('error_attachment');
			}
			   
			   }
			
			/*if (utf8_strlen($this->request->post['share']) < 3) {
				$this->error['share'] = $this->language->get('error_share');
			}*/

		

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/information')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$this->load->model('setting/store');

		foreach ($this->request->post['selected'] as $information_id) {
			if ($this->config->get('config_account_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_account');
			}

			if ($this->config->get('config_checkout_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_checkout');
			}

			if ($this->config->get('config_affiliate_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_affiliate');
			}

			if ($this->config->get('config_return_id') == $information_id) {
				$this->error['warning'] = $this->language->get('error_return');
			}

			$store_total = $this->model_setting_store->getTotalStoresByInformationId($information_id);

			if ($store_total) {
				$this->error['warning'] = sprintf($this->language->get('error_store'), $store_total);
			}
		}

		return !$this->error;
	}
	
	public function search() 
	{ 
		$this->load->language('patient/attachment');
		$this->load->model('patient/attachment');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		$results = $this->model_patient_attachment->getSearchAttach($this->request->get['term']);
		$return='';
	
	foreach($results as $result) {
		$urls = $this->config->get('config_url').'image/attachment'. '/' . $result['attachment'];
		$i=1;

		$return.= '<tr><td class="sorting_1">'.$result['title'].'</td>
							<td><a href="'.$urls.'" target="_blank"> <i class="fa fa-file"> </a></td>
							<td>'  .$result['created_at'].'</td>
                          <td><a href="'.$this->url->link('patient/attachment/edit', 'attach_id=' . $result['id'] . $url, true).'" data-toggle="tooltip" title="'.$this->language->get('button_edit').'" class="btn btn-primary"><i class="fa fa-pencil"></i></a><a href="'. $this->url->link('patient/attachment/delete', 'attach_id=' . $result['id'] . $url, true).'" data-toggle="tooltip" title="'.$this->language->get('button_delete').'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
						  
                          </tr>';
						
	$i++;}
		echo $return;
	}
}