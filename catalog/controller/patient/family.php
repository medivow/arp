<?php
class ControllerPatientFamily extends Controller {
	private $error = array();

	public function index() { 
		$this->load->language('patient/family');
		$this->document->setTitle($this->language->get('Family Dashboard'));
		$this->load->model('patient/family');
		$data['success'] = '';
	 $this->getList();
		
	}

	public function add() { 
		
		$this->load->language('patient/family');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('patient/family');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$patient_id = $this->customer->getId();
			$this->model_patient_family->addFamily($this->request->post, $patient_id);
			$this->session->data['success'] = $this->language->get('text_success');
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/family', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('patient/family');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('patient/family');


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			
			$this->model_patient_family->editDailyEssay($this->request->get['essay_id'], $this->request->post);
			
			

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/family', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('patient/family');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('patient/family');

		/*if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $information_id) {
				$this->model_doctor_case->deleteInformation($information_id);
			}*/
			
          if (isset($this->request->get['fid']) ) {
				$this->model_patient_family->deleteFamily($this->request->get['fid']);
			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('patient/family', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		$data['entry_description'] = $this->language->get('entry_description');
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true)
		);*/

		$data['add'] = $this->url->link('patient/family/add',  $url, true);
		$data['delete'] = $this->url->link('patient/family/delete', $url, true);

		$data['dailyessay'] = array();

		$filter_data = array(
			'sort'  => $sort,
			'order' => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);

		$Dailyessay_total = $this->model_patient_family->getTotalDailyEssay();
		$patient_id = $this->customer->getId();;/// static id for testing only
		$results = $this->model_patient_family->getFamilies($patient_id);
		$notifications = $this->model_patient_family->getNotification($patient_id);
        //$values = $this->model_patient_family->getPatient($result['pid']);
		//print_r($results);die;
		
		 if(!empty($notifications )){
		foreach ($notifications as $notification) {
			$data['notification'][] = array(
				'id' => $notification['fid'],
				'patient'    => $notification['pname'],
				'relation'   => $notification['name'],
				/*'accept'           => $this->url->link('patient/family/chnagestatus', 'essay_id=' . $result['fid'] . $url, true),
				'delete'           => $this->url->link('patient/family/delete', 'fid=' . $result['fid'] . $url, true),*/
			);
		}
		}
		else
		{
			$data['notification'] = array();
		}
		
        if(!empty($results )){
		foreach ($results as $result) {
			if($result['pid']!=$this->customer->getId())
			{
			$pid=$result['pid'];
			}
			else
			{
				$pid=$result['p_id'];
			}
			$patient = $this->model_patient_family->getPatient($pid);
			$patientName = $patient[0]['name'];
			$patientEmail = $patient[0]['email']; 
			$relation = $this->model_patient_family->getRelation($result['rid']);
			$relationName = $relation[0]['name']; 
			$data['families'][] = array(
				'id' => $result['fid'],
				'patient'    => $patientName,
				'email'    => $patientEmail,
				'relation'   => $relationName,
				'edit'           => $this->url->link('patient/family/edit', 'essay_id=' . $result['fid'] . $url, true),
				'delete'           => $this->url->link('patient/family/delete', 'fid=' . $result['fid'] . $url, true),
				'message_action'           => $this->url->link('patient/family/sendmessage', 'fid=' . $result['fid'] . $url, true),
			);
		}
		}
		else
		{
			$data['families'] = array();
		}
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_title'] = $this->language->get('column_title');
		$data['column_sort_order'] = $this->language->get('column_sort_order');
		$data['column_action'] = $this->language->get('column_action');

		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['send_message'] = $this->language->get('send_message');
		$data['entry_message'] = $this->language->get('entry_message');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('patient/family','sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('patient/family', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $Dailyessay_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('patient/family',  $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($Dailyessay_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($Dailyessay_total - $this->config->get('config_limit_admin'))) ? $Dailyessay_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $Dailyessay_total, ceil($Dailyessay_total / $this->config->get('config_limit_admin')));

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/family', $data));
	}



protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_form'] = !isset($this->request->get['essay_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_title'] = $this->language->get('entry_title');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_bottom'] = $this->language->get('entry_bottom');
		
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

       if (isset($this->error['patient'])) {
			$data['error_patient'] = $this->error['patient'];
		} else {
			$data['error_patient'] = '';
		}
		

		if (isset($this->error['relation'])) {
			$data['error_relation'] = $this->error['relation'];
		} else {
			$data['error_relation'] = '';
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		/*$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('doctor/case', 'token=' . $this->session->data['token'] . $url, true)
		);
*/
		if (!isset($this->request->get['essay_id'])) {
			$data['action'] = $this->url->link('patient/family/add', $url, true);
		} else {
			$data['action'] = $this->url->link('patient/family/edit',  '&essay_id=' . $this->request->get['essay_id'] . $url, true);
		}
		$data['cancel'] = $this->url->link('patient/family',  $url, true);
		if (isset($this->request->get['essay_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$DailyEssay_info = $this->model_patient_family->getDailyEssay($this->request->get['essay_id']);
		}
		//$data['token'] = $this->session->data['token'];
		if (isset($this->request->post['title'])) {
			$data['title'] = $this->request->post['title'];
		} elseif (isset($DailyEssay_info['title'])) {
			$data['title'] = $DailyEssay_info['title'];
		} else {
			$data['title'] = '';
		}
		 if (isset($this->request->post['doct_id'])) {
			$data['doct_id'] = $this->request->post['doct_id'];
		} elseif (isset($DailyEssay_info['doct_id'])) {
			$data['doct_id'] = $DailyEssay_info['doct_id'];
		} else {
			$data['doct_id'] = '';
		}
		if (isset($this->request->post['description'])) {
			$data['description'] = $this->request->post['description'];
		} elseif (isset($DailyEssay_info['description'])) {
			$data['description'] = $DailyEssay_info['description'];
		} else {
			$data['description'] = '';
		}
		//print_r($results); die; 
		$data['familyRelList'] = $this->model_patient_family->familyRelList();
		$data['patientlist'] = $this->model_patient_family->patientList();
        $data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		$this->response->setOutput($this->load->view('patient/family_form', $data));
	}
	protected function validateForm() {
	
		if ($this->request->post['patient'] == '') {
			$this->error['patient'] = $this->language->get('error_patient');
		 }
		 if ($this->request->post['relation'] == '') {
			$this->error['relation'] = $this->language->get('error_relation');
		 }


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

public function search() 
	{
		$this->load->language('patient/family');
		$this->load->model('patient/family');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		
		$results = $this->model_patient_family->getSearchFamily($this->request->get['term']);
		//$results = $this->model_patient_prescription->getSearchPrescription($this->request->get['term']);
		$return='';
	foreach($results as $result) {
		$i=1;
		if($result['pid']!=$this->customer->getId())
			{
			$pid=$result['pid'];
			}
			else
			{
				$pid=$result['p_id'];
			}
			$patient = $this->model_patient_family->getPatient($pid);
			$patientName = $patient[0]['name']; 
		$relation = $this->model_patient_family->getRelation($result['rid']);
		$relationName = $relation[0]['name']; 
		//echo $this->url->link('doctor/case/edit', 'case_id=' . $result['id'] . $url, true); die;
		$return.= '<tr><td class="sorting_1">'.$patientName.'</td>
							<td>'  .$relationName.'</td>
                          <td><a href="'. $this->url->link('patient/family/delete', 'fid=' . $result['fid'] . $url, true).'" data-toggle="tooltip" title="'.$this->language->get('button_delete').'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
						  
                          </tr>';
						
	$i++;}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}

public function chnagerelstatus() 
	{
		$this->load->language('patient/family');
		$this->load->model('patient/family');
		$fid=$this->request->get['id'];
        $results = $this->model_patient_family->updateRequeststatus($fid);
		$patient_id = $this->customer->getId();
		$results = $this->model_patient_family->getFamilies($patient_id);
		$notifications = $this->model_patient_family->getNotification($patient_id);
		$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		$return='';
	foreach($results as $result) {
		$i=1;
		if($result['pid']!=$this->customer->getId())
			{
			$pid=$result['pid'];
			}
			else
			{
				$pid=$result['p_id'];
			}
		$relation = $this->model_patient_family->getRelation($result['rid']);
		$relationName = $relation[0]['name']; 
		$patient = $this->model_patient_family->getPatient($pid);
		$patientName = $patient[0]['name']; 
		//echo $this->url->link('doctor/case/edit', 'case_id=' . $result['id'] . $url, true); die;
		$return.= '<td class="sorting_1">'.$patientName.'</td>
							<td>'  .$relationName.'</td>
                          <td><a href="'. $this->url->link('patient/family/delete', 'fid=' . $result['fid'] . $url, true).'" data-toggle="tooltip" title="'.$this->language->get('button_delete').'" class="btn btn-danger"><i class="fa fa-trash-o"></i></a></td>
                          ';
							$i++;}
							$return.='+';
							if(!empty($notifications)){
							foreach($notifications as $not) {
								$return.= '<td class="sorting_1">'.$patientName.'</td>
							<td>'  .$relationName.'</td>
                          <td><a href="#" data-toggle="tooltip" title="Accept" class="btn "><img src="catalog/view/theme/default/image/ok.png" /></a></td>
						  <td><a href="#" data-toggle="tooltip" title="Reject" class="btn "><img src="catalog/view/theme/default/image/reject.png" /></a></td>
                          ';
						
	$i++;}}
	else
	{
		$return.='<td colspan="3">No record found..</td>';
	}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
	
	
	public function sendmessage() 
	{
		$this->load->language('patient/family');
		$this->load->model('patient/family');
		$email=$this->request->post['email'];
		$message=$this->request->post['message'];
		$sender_id = $this->customer->getId();
		$sender_fname = $this->customer->getFirstName();
		$sender_lname = $this->customer->getLastName();
		$sendrname    = $sender_fname .' ' .$sender_lname; 
        $results = $this->model_patient_family->sendMail($email,$message,$sendrname);
		
	}

}