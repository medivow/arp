<?php
class ControllerPatientPatient extends Controller {
	
public function index() 
	{
		//$this->load->language('patient/patient');
		$this->load->model('patient/patient');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
        $filter_data = array('customer_id'=>$this->customer->getId());
		$information_total = $this->model_patient_patient->getTotalInformations();
		$results = $this->model_patient_patient->getInformations($filter_data);
			if(!empty($results)){
				foreach ($results as $result) {
			$data['informations'][] = array(
			'information_id' => $result['information_id'],
			'title'          => $result['title'],
			'sort_order'     => $result['sort_order'],
			'description'	 =>  $result['description']
			);
			 }
			}
			else{
			$data['informations'][]='NO RESULTS FOUND';
				}
			
		///////////// Get the current User Id ///////////
		$filter_data2 = array(
		 'customer_id'=>$this->customer->getId() 
		);
		   // Get list Of appointment 
		$resultsap = $this->model_patient_patient->getUserappointment($filter_data2);
 		  // Get Docter Name Fucntion
		  // foreach($resultsap as $docnm ){
		  // $docn[]=$docnm['doc_id']; }
		// $doctername=$this->model_patient_patient->getDoctername();
		// $pusharr = array("doc_name" => 'Dr. ANAND PANDEY');
		// $resultpushed = array_merge($resultsap,$pusharr);
 		
		$data['appoinments'] = $resultsap;
		//////////////// Show In View page all data ///////////////////			
		 $this->response->setOutput($this->load->view('patient/dashboard', $data));
	 
	 	}
		
		public function eventjson() {
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		
		$this->load->model('patient/patient');
		$results = $this->model_patient_patient->getEvents($filter_data);
		
		
		$event = array();
		foreach ($results as $result) {
			$event[] = array(
				"id"=> $result['aid'],
				"title"	=> $result['firstname'].' '.$result['lastname'],
				"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
				"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($event));		
	}

	public function  calendar() 

	{

		$data=array();
  $url='';
		$data['eventjson'] = html_entity_decode($this->url->link('patient/patient/eventjson', $url , true));

	
		

		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		


		$this->response->setOutput($this->load->view('patient/calendar', $data));

	}
	
	}


