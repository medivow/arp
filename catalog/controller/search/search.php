<?php
class ControllerSearchSearch extends Controller {
	public function index() {
		$this->load->language('search/search');

		$this->load->model('search/search');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		    /*$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);*/
			if(!empty($this->request->post['autocomplete'])){
				
			if(!empty($this->request->post['autocomplete'])){$localad =$this->request->post['autocomplete'];}
			if(!empty($this->request->post['latitude'])){ $lat=$this->request->post['latitude'];}
			if(!empty($this->request->post['longitude'])){ $lang=$this->request->post['longitude']; }
			
			}else{
				 $localad='';
				 $lat='';
			     $lang='';
			}
	
			if(!empty($this->request->post['keyword'])){
			$res =$this->request->post['keyword'];
			
			$spac=$res[0][0];
			}else{
				 $spac='';
			}
			//get spaciality id by there name
			$spcid= $this->model_search_search->getSpacial_id($spac);
			 // get results of serching at home page search
			$filters=array();
			if(!empty($localad)){$filters['locality'] = $localad;}
			if(!empty($lat)){$filters['latitude'] = $lat;}
			if(!empty($lang)){$filters['longitude'] = $lang;}
			if(!empty($res)){$filters['res'] = $res;}
				
			$data['doctors']= $this->model_search_search->getDoctors($filters);
			
			if(!empty($data['doctors'])){
           
		   foreach($data['doctors'] as $doctor)
			{
			  $cus_services= $this->model_search_search->getCustomerSpecilityById($doctor['doc_id']);
			   
			   $data['doctor_speciality'][$doctor['doc_id']]=implode(' , ',$cus_services);	
			   
			   $this->load->model('tool/image');
			
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['doctor_image'][$doctor['doc_id']]=$image;
			}
			}
			else
			{
				
				$data['doctor_speciality']='';
				$data['doctor_image']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
            
			$data['detail_href'] = $this->url->link('search/detail');
			
			$data['book_now_href'] = $this->url->link('patient/appointment/detail');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $this->document->setTitle($this->language->get('heading_title'));
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('search/listing', $data));
	}

	public function detail(){
	return $this->load->view('common/detail', $data);	
	}
	public function searchtest()
	{
		//echo $this->request->post['day']; die;
		//print_r($this->request->post['availability']); die;
		$this->load->language('search/search');
		$this->load->model('search/search');
		if(!empty($this->request->post['availability']))
		//$days=implode(",",$this->request->post['availability']);
		//if(!empty($days))
		$data['availability_days']= $this->model_search_search->getAvailabilityDays($this->request->post['availability'], $this->request->post['stime'], $this->request->post['etime']);
		//echo "test"; die;
		//print_r($data['availability_days']); die;
		
		if(!empty($data['availability_days'])){
           
		   foreach($data['availability_days'] as $doctor)
			{
			  $cus_services= $this->model_search_search->getCustomerSpecilityById($doctor['doc_id']);
			   
			   $data['doctor_speciality'][$doctor['doc_id']]=implode(' , ',$cus_services);	
			   
			   $this->load->model('tool/image');
			
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['doctor_image'][$doctor['doc_id']]=$image;
			}
			
		
		
		$data['detail_href'] = $this->url->link('search/detail');
		$data['book_now_href'] = $this->url->link('patient/appointment/detail');
		
		
		 if(!empty($data['availability_days'])) {
              $i=1;
            foreach($data['availability_days'] as $doctor){
           ?>
           <?php
            if($i==1){
        echo '<div class="col-sm-12">';
             } 
            
            echo '<div class="col-md-6 col-lg-6 wbreaks">';
            
           echo '<div class="media patient_list">
            	<div class="media-left media_sec text-center"><img class="patient_img" src="'.$data['doctor_image'][$doctor['doc_id']].'"></div>';
       				echo '<div class="media-body">';
                    	echo '<div class="cont_h">';
            				echo '<p>Name:'.$doctor['firstname'].' '.$doctor['lastname'].'</p>';
              				if($doctor['qualification']!=''){
								echo '<p>'.$doctor['qualification'].'</p>';
							 } 
							 if($doctor['experience']!=''){
                             
                            echo '<p class="BDS">'.$doctor['experience'].' years experience</p>';
							 }
                        	echo '<p>'.$data['doctor_speciality'][$doctor['doc_id']].'</p>';
                        	echo '<p> <i class="fa fa-map-marker" aria-hidden="true"></i>'.$doctor['locality'].'</p>';
                        echo '<ul class="star">
                			<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                		</ul>
                        </div> 
       				
           </div>
		    <div class="clearfix"></div>
            <a class="btn btn-book_now" href="'.$data['book_now_href'].'/&doc_id='.$doctor['doc_id'].'">Book Now</a>
            <a class="btn btn-primary2" href="'.$data['detail_href'].'&doctor_id='.$doctor['doc_id'].'">View profile</a>
           </div>
           </div>
           ';
            
           
             if($i==2){
           $i=0;
           
        echo '</div>';
        } 
        $i=$i+1;
            
            }} 
		
		}
		//die;
		
		else
	{	
		//$this->response->setOutput($this->load->view('search/listing', $data));
		$this->response->redirect($this->url->link('search/search', '', true));
	}
		
		}
}
