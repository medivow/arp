<?php
class ControllerDoctorDoctor extends Controller {
public function index() 
	{
		$this->load->model('doctor/case');
		$this->load->model('doctor/dailyessay');
		$data=array();
        $url='';

		$doct_id = $this->customer->getId();// static id for testing only
$filter_data = array(
'start' => '0',
'limit' => '7'
			
		);
		$results = $this->model_doctor_case->getInformations($doct_id,$filter_data);
		//print_r($results); die;
		foreach ($results as $result) {
			
			$data['csae_studay'][] = array(
			
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/case/detail', 'case_id=' . $result['id'] . $url, true),
			);
		}

		$results_essay = $this->model_doctor_dailyessay->getDailyEssays($doct_id,$filter_data);
		
		 if(!empty($results_essay )){
		foreach ($results_essay as $essay) {
			
			$data['dailyessays'][] = array(
				'id' => $essay['id'],
				'title'          => $essay['title'],
				'description'   =>$essay['description'],
				'added_dt'      =>$essay['added_date'],
				'detail'           => $this->url->link('doctor/dailyessay/detail', 'essay_id=' . $essay['id'] . $url, true),
				
			);
		}
		}
		$this->document->setTitle($this->language->get('Dashboard'));
		$this->load->model('doctor/patients');
		$filter_data=array('doctor_id'                => $doct_id,);
		$results_patent = $this->model_doctor_patients->getPatients($filter_data);
		$data['patients'] = $results_patent;
		$data['eventjson'] = html_entity_decode($this->url->link('doctor/doctor/eventjson', $url , true));
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
		$this->response->setOutput($this->load->view('doctor/dashboard', $data));
	}

	public function eventjson() {
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		
		$this->load->model('doctor/doctor');
		$results = $this->model_doctor_doctor->getEvents($filter_data);
		
		
		$event = array();
		foreach ($results as $result) {
			$event[] = array(
				"id"=> $result['aid'],
				"title"	=> $result['firstname'].' '.$result['lastname'],
				"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
				"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($event));		
	}

	public function  calendar() 

	{

		$this->document->setTitle($this->language->get('Calendar'));
		$data=array();
		$url='';
		$data['eventjson'] = html_entity_decode($this->url->link('doctor/doctor/eventjson', $url , true));
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
		$this->response->setOutput($this->load->view('doctor/calendar', $data));
	}
	
	public function  readSpacilities() 
		{
			$this->load->model('doctor/doctor');
			$results = $this->model_doctor_doctor->getSpaciality($this->request->post);
			if(!empty($results)){
    	echo '<ul class="search_ul">';
		foreach($results as $info)
			{ ?>
<li style="text-transform:uppercase; list-style:none;"><a href="javascript:void(0);" onclick="setindiv('<?php echo $info['title'];?>')">&raquo;&nbsp;<?php echo $info['title'];?></a></li>
	<?php
	 }
    echo '</ul>';
    }}
	
    }