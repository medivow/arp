<?php
class ControllerDoctorClinicajax extends Controller {
	
	private $error = array();
    
	public function index() {
	
		$this->load->model('account/customer');
		$this->load->model('doctor/doctor');
	
		if (($this->request->server['REQUEST_METHOD'] == 'POST'))
		  {
			$cust_id= $this->customer->getId(); 
			$customer_id = $this->model_doctor_doctor->add_clinic($cust_id, $this->request->post);
		  }
	}
	
	public function checkrepusername()
	{
		$this->load->model('account/customer');
		$this->load->model('doctor/doctor');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST'))
		{
			if(!empty($_POST["username"])) 
			{
			$checkcl = $this->model_doctor_doctor->checkusrname($this->request->post);
						
			if($checkcl>0) echo "<span class='status-not-available'><input type='hidden' name='usernamevali' id='usernamevali' value='1'><font color='red'> Username Not Available.</font></span>";
			else echo "<span class='status-available'><input type='hidden' name='usernamevali' id='usernamevali' value='0'> <font color='green'>Username Available.</font></span>";
			}
		}
		
	}

	private function validate() {
				
		if ((utf8_strlen(trim($this->request->post['username'])) < 1) || (utf8_strlen(trim($this->request->post['username'])) > 32 )) {
			$this->error['username'] = $this->language->get('error_username');
		}
		
		if ($this->model_account_customer->getTotalCustomersByUsername($this->request->post['username'])) {
			$this->error['warning'] = $this->language->get('error_user_exists');
		}
		
		if ((utf8_strlen(trim($this->request->post['firstname'])) < 1) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
			$this->error['firstname'] = $this->language->get('error_firstname');
		}
		
		if ((utf8_strlen(trim($this->request->post['lastname'])) < 1) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
			$this->error['lastname'] = $this->language->get('error_lastname');
		}
	}
	
	
	
	
	public function delete() {
		//$this->load->language('doctor/attachment');
		//$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/doctor');

           if (isset($this->request->get['del_id']) ) {
			$this->model_doctor_doctor->deleteClinic($this->request->get['del_id']);
			$this->session->data['success'] = $this->language->get('text_delete');
			$url = '';
			$this->response->redirect($this->url->link('doctor/account', 'token=' . $this->session->data['token'] . $url, true));
		}

		//$this->getList();
	}
	
	
	
}