<?php
class ControllerDoctorBilling extends Controller {
	
public function index() 

	{
		$this->load->language('doctor/billing');
		$data=array();
		

		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		
		if (!$this->customer->isLogged()) {
   $this->session->data['redirect'] = $this->url->link('account/order', '', true);

   $this->response->redirect($this->url->link('account/login', '', true));
  }
  $this->document->setTitle($this->language->get('Billing Details'));
		
		$this->load->model('doctor/billing');
		$total_biling = $this->model_doctor_billing->getTotalBillingInfo($filter_data);
		$results = $this->model_doctor_billing->getBillingInfo($filter_data);
		
		$data['billing'] = $results;
	
		if (isset($this->error['patient'])) {
			$data['error_patient'] = $this->error['patient'];
		} else {
			$data['error_patient'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}
		
		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '';
		}
		
		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		if (isset($this->error['discount'])) {
			$data['error_discount'] = $this->error['discount'];
		} else {
			$data['error_discount'] = '';
		}
        $url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_title'] = $this->url->link('doctor/case','sort=id.title' . $url, true);
		$data['sort_sort_order'] = $this->url->link('doctor/case', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		$pagination = new Pagination();
		$pagination->total = $total_biling;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('doctor/billing',  $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total_biling) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total_biling - $this->config->get('config_limit_admin'))) ? $total_biling : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total_biling, ceil($total_biling / $this->config->get('config_limit_admin')));
		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['add']       = $this->url->link('doctor/billing/add', '', true);
		$data['action'] = $this->url->link('doctor/billing/add');
		$data['column_left'] = $this->load->controller('common/doctor_left');
		//$data['column_right'] = $this->load->controller('common/doctor_right');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');

		$this->response->setOutput($this->load->view('doctor/billing', $data));

	}


			public function add()
			{
			if (!$this->customer->isLogged()) {
			   $this->session->data['redirect'] = $this->url->link('account/order', '', true);
			
			   $this->response->redirect($this->url->link('account/login', '', true));
			  }
			  $this->document->setTitle($this->language->get('Prescription Details'));
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
		
			$this->load->model('doctor/billing');
	//echo $this->request->get['doctor_id']; echo $this->session->data['patient_id']; die;
			$results = $this->model_doctor_billing->addBilling($this->request->post);
			$this->response->redirect($this->url->link('doctor/billing'));
			}
			$this->load->model('doctor/billing');
			$presults = $this->model_doctor_billing->getPatients($this->customer->getId());
			$data['presults'] = $presults;
			
			
			
			if (isset($this->error['patient'])) {
			$data['error_patient'] = $this->error['patient'];
		} else {
			$data['error_patient'] = '';
		}

		if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
		} else {
			$data['error_title'] = '';
		}
		
		if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
		} else {
			$data['error_description'] = '';
		}
		
		if (isset($this->error['amount'])) {
			$data['error_amount'] = $this->error['amount'];
		} else {
			$data['error_amount'] = '';
		}

		if (isset($this->error['discount'])) {
			$data['error_discount'] = $this->error['discount'];
		} else {
			$data['error_discount'] = '';
		}
			//print_r($presults); die;
			$data['action'] = $this->url->link('doctor/billing/add', '', true);
			$data['column_left'] = $this->load->controller('common/doctor_left');
	
	
			//$data['column_right'] = $this->load->controller('common/doctor_right');
			
			$data['footer'] = $this->load->controller('common/doctor_footer');
			$data['header'] = $this->load->controller('common/dheader');
	
	
				$this->response->setOutput($this->load->view('doctor/addbilling', $data));
			
			}
			
			public function detail()
			{
				if (!$this->customer->isLogged()) {
				   $this->session->data['redirect'] = $this->url->link('account/order', '', true);
				
				   $this->response->redirect($this->url->link('account/login', '', true));
				  }
				  $this->document->setTitle($this->language->get('Prescription Details'));		
				//echo (int) $this->request->get['billing_id']; die;
				
				//echo $this->session->data['patient_id']; die;
				$this->load->model('doctor/billing');
				
				$billresults = $this->model_doctor_billing->getBillingInfoByID((int) $this->request->get['billing_id']);
				//echo $billresults['amount']; die;
					echo '<div class="row">
                        	    <div class="col-sm-4"><label>Title:</label></div>
                                <div class="col-sm-8">' .$billresults['title']. '</div>
                           </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Description:</label></div>
                                <div class="col-sm-8">' .$billresults['description']. '</div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Amount:</label></div>
                                <div class="col-sm-8">' .$billresults['amount']. '</div>
                             </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Discount:</label></div>
                                <div class="col-sm-8">' .$billresults['discount']. '</div>
                            </div>
							<div class="row">
                            	<div class="col-sm-4"><label>Added Date:</label></div>
                                <div class="col-sm-8">' .$billresults['added_date']. '</div>
                            </div>'; 
						
						//$data['patient_id']=$this->request->get['patient_id']; 
					//$this->load->model('doctor/patients');
					
					//$presults = $this->model_doctor_patients->getPatientdetail($pid);
					//$data['prescriptions'] = $presults;
					//print_r($presults); die;
					//$data['patients'] = $results;
					
					
		
				
				//$data['action'] = $this->url->link('doctor/patients/view_doctor', '&patient_id=' . $this->request->get['patient_id']);
				
					$data['column_left'] = $this->load->controller('common/doctor_left');
			
					//$data['column_right'] = $this->load->controller('common/doctor_right');
					
					$data['footer'] = $this->load->controller('common/doctor_footer');
					$data['header'] = $this->load->controller('common/dheader');
			
			
						//$this->response->setOutput($this->load->view('doctor/billing', $data));
					
					}
					
					
		private function validate() {
		
				
			if ((utf8_strlen(trim($this->request->post['title'])) < 1) || (utf8_strlen(trim($this->request->post['title'])) > 32 )) {
			$this->error['title'] = $this->language->get('error_title');
			
		}
		
		
		if ((utf8_strlen(trim($this->request->post['description'])) < 1)) {
			$this->error['description'] = $this->language->get('error_description');
		}
		
		if ((utf8_strlen(trim($this->request->post['amount'])) < 1)) {
			$this->error['amount'] = $this->language->get('error_amount');
		}

		if ((utf8_strlen($this->request->post['discount']) < 1)) {
			$this->error['discount'] = $this->language->get('error_discount');
		}
		
		if ((utf8_strlen($this->request->post['patient']) < 1)) {
			$this->error['patient'] = $this->language->get('error_patient');
		}

		return !$this->error;
	}
	
	public function search() 
	{
		$this->load->language('doctor/billing');
		$this->load->model('doctor/billing');
		//$data['button_edit'] = $this->language->get('button_edit');
		//$data['button_delete'] = $this->language->get('button_delete');
		$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
		//$cid= $this->customer->getId();
		$results = $this->model_doctor_billing->getSearchBilling($this->request->get['term']);
		//$results = $this->model_patient_prescription->getSearchPrescription($this->request->get['term']);
		$return='';
	
	foreach($results as $result) {
		$i=1;
		//echo $this->url->link('doctor/case/edit', 'case_id=' . $result['id'] . $url, true); die;
		$return.= '<tr><td class="sorting_1">'.$result['title'].'</td>
							<td>'  .$result['amount'].'</td>
							<td>'  .$result['discount'].'</td>
							<td>'  .$result['added_date'].'</td>
                          <td> <button type="button" data-value="'.$result['bid'].'" class="btn btn-info btn-lg" id="detail" value="'.$result['bid'].'" data-toggle="modal" data-target="#myModal" >Detail</button></td>
						  
                          </tr>';
						
	$i++;}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
					
}