<?php
class ControllerDoctorAppointment extends Controller {
private $error = array();	
public function index() 

	{
		$this->load->language('search/search');

		$this->load->model('search/search');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
			$doct_id = $this->customer->getId();	
			$data['patients']= $this->model_search_search->getDoctorsPatient($doct_id);
			//print_r($data['patients']); die;
			if(!empty($data['patients'])){
           
		   foreach($data['patients'] as $patient)
			{
			   $this->load->model('tool/image');
			
			if (!empty($patient) && (is_file(DIR_IMAGE . $patient['image']))) {
				$image = $this->model_tool_image->resize($patient['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['patient_image'][$patient['cus_id']]=$image;
			$data['patient_appointment'][$patient['cus_id']]=$this->url->link('doctor/appointment/paid_2/&cus_id='.$patient['cus_id'].'', '', true);
			$data['patient_detail'][$patient['cus_id']]=$this->url->link('doctor/appointment/detail/&cus_id='.$patient['cus_id'].'', '', true);
			}
			}
			else
			{
				$data['doctor_image']='';
				$data['doctor_detail']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
            
			$data['detail_href'] = $this->url->link('search/detail');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $this->document->setTitle($this->language->get('heading_title'));
		    $data['column_left'] = $this->load->controller('common/doctor_left');
		   $data['footer'] = $this->load->controller('common/doctor_footer');
		    $data['header'] = $this->load->controller('common/dheader');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			/*$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');*/

			$this->response->setOutput($this->load->view('doctor/patients_listing', $data));
	}
	

	public function detail() {
		$this->load->language('search/search');

		$this->load->model('search/search');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		    
			$filters=array();
			$doct_id = $this->customer->getId();
			if(isset($this->request->get['cus_id'])){
				$cusID=$this->request->get['cus_id'];
				$data['cus_Id']=$cusID;
				$filters['cus_Id'] = $cusID;
			}
				
			$data['patients']= $this->model_search_search->getPatientDetail($filters);
			//print_r($data['patients']); die;
			if(!empty($data['patients'])){
            foreach($data['patients'] as $patient)
			{
			   $this->load->model('tool/image');
			
			if (!empty($patient) && (is_file(DIR_IMAGE . $patient['image']))) {
				$image = $this->model_tool_image->resize($patient['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['patient_image'][$patient['cus_id']]=$image;
			
			$data['book_appointment'][$patient['cus_id']]=$this->url->link('doctor/appointment/booking/&cus_id='.$patient['cus_id'].'', '', true);
			}
			}
			else
			{
				
				
				$data['patient_image']='';
				$data['book_appointment']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));
            $data['action'] = $this->url->link('doctor/patients/prescription', '&patient_id=' . $this->request->get['cus_id']);
			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
            
			$data['detail_href'] = $this->url->link('search/detail');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $this->document->setTitle($this->language->get('heading_title'));
			
			
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			/*$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');*/

			$this->response->setOutput($this->load->view('doctor/patient_detail', $data));
	}
	public function package() 

	{
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/package', $data));
	}
	
	public function free() 

	{
	   $this->load->language('patient/appointment');
	   $this->load->model('patient/appointment');
	   $data['customerlist'] = $this->model_patient_appointment->doctorList();
	  // $data['success'] = '';
	   if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateFreeConsulation()) {
			$appointment_id = $this->model_patient_appointment->addFreeConsultation($this->request->post,$this->customer->getId());
		
			$data['success'] = $this->language->get('success_free_appoint');
			$data['query'] ='';
		}
		if (isset($this->request->post['mobile'])) {

			$data['mobile'] = $this->request->post['mobile'];

		}
		else{$data['mobile']='';}
		if (isset($this->request->post['query'])) {

			$data['query'] = $this->request->post['query'];

		}
		else{$data['query']='';}
		if (isset($this->error['mobile'])) {
			$data['error_mobile'] = $this->error['mobile'];
		} else {
			$data['error_mobile'] = '';
		}
		
		if (isset($this->error['query'])) {
			$data['error_query'] = $this->error['query'];
		} else {
			$data['error_query'] = '';
		}
		$data['action'] = $this->url->link('patient/appointment/free', '', true);
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');

		$this->response->setOutput($this->load->view('patient/free', $data));
	}
	
	public function paid() 
	{
		$this->load->language('patient/appointment');	
		$this->load->model('patient/appointment');
		$data['success'] = '';
	   if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		   $this->session->data['doctor'] = $this->request->post['doctor'];
			$data['photo'] = '';
			$uploads_dir = DIR_UPLOAD; // set you upload path here
			if (is_uploaded_file($this->request->files['photo']['tmp_name'])) {
				move_uploaded_file($this->request->files['photo']['tmp_name'],$uploads_dir.$this->request->files['photo']['name']);
			    $data['photo'] = $this->request->files['photo']['name'];
			}
			$appointment_id = $this->model_patient_appointment->addAppointment($this->request->post, $data['photo']);
			$this->session->data['appointment_id'] = $appointment_id;
			$this->response->redirect($this->url->link('patient/appointment/paid_2'));
			$data['success'] = $this->language->get('success_appoint');
		}
		
		
			/*----------------lables--------------*/
		$data['entry_consult_doctor'] = $this->language->get('entry_consult_doctor');	
        $data['entry_doctors'] = $this->language->get('entry_doctors');	
		$data['entry_appointment_date'] = $this->language->get('entry_appointment_date');
		$data['entry_about_patient'] = $this->language->get('entry_about_patient');
		$data['entry_submit'] = $this->language->get('entry_submit');
		/*---------------lables---------------*/
		
		
		if (isset($this->error['doctor'])) {
			$data['error_doctor'] = $this->error['doctor'];
		} else {
			$data['error_doctor'] = '';
		}
		
		if (isset($this->error['appointment_date'])) {
			$data['error_appointment_date'] = $this->error['appointment_date'];
		} else {
			$data['error_appointment_date'] = '';
		}
		
		if (isset($this->error['about_patient'])) {
			$data['error_about_patient'] = $this->error['about_patient'];
		} else {
			$data['error_about_patient'] = ''; }
		
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		$data['action'] = $this->url->link('patient/appointment/doctor_list', '', true);
        $data['customerlist'] = $this->model_patient_appointment->doctorList();
		$this->response->setOutput($this->load->view('patient/paid', $data));
	}
	
	public function doctor_list() {
		$this->load->language('search/search');

		$this->load->model('search/search');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		    /*$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);*/
			if(isset($this->request->post['cur_lat'])){
			 $lat=$this->request->post['cur_lat'];
			 $lang=$this->request->post['cur_lag']; 
			
			}else{
				$lat='';
			     $lang='';
			}
	
			if(!empty($this->request->post['keyword'])){
			$spac =$this->request->post['keyword'];
			}else{
				 $spac='';
			}
			//get spaciality id by there name
			$spcid= $this->model_search_search->getSpacial_id($spac);
			 // get results of serching at home page search
			$filters=array();
			if(!empty($spcid)){$filters['speciality'] = $spcid;}
			if(!empty($lat)){$filters['latitude'] = $lat;
			$filters['locality'] = $lat;
			}
			if(!empty($lang)){$filters['longitude'] = $lang;}
			if(!empty($spcid)){$filters['spaci'] = $spcid[0]['id'];}
				
			$data['doctors']= $this->model_search_search->getDoctors($filters);
			
			if(!empty($data['doctors'])){
           
		   foreach($data['doctors'] as $doctor)
			{
			   $cus_services= $this->model_search_search->getCustomerSpecilityById($doctor['doc_id']);
			   
			   $data['doctor_speciality'][$doctor['doc_id']]=implode(' , ',$cus_services);	
			   
			   $this->load->model('tool/image');
			
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['doctor_image'][$doctor['doc_id']]=$image;
			$data['doctor_appointment'][$doctor['doc_id']]=$this->url->link('patient/appointment/paid_2/&doc_id='.$doctor['doc_id'].'', '', true);
			$data['doctor_detail'][$doctor['doc_id']]=$this->url->link('patient/appointment/detail/&doc_id='.$doctor['doc_id'].'', '', true);
			}
			}
			else
			{
				
				$data['doctor_speciality']='';
				$data['doctor_image']='';
				$data['doctor_detail']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');
            
			$data['detail_href'] = $this->url->link('search/detail');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $this->document->setTitle($this->language->get('heading_title'));
			
			
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			/*$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');*/

			$this->response->setOutput($this->load->view('patient/doctors_listing', $data));
	}
	
	
	public function booking() 
	{
		$this->load->language('patient/appointment');	
		$this->load->model('patient/appointment');
		$data['success'] = '';
		$this->document->setTitle('Select Timing');
			/*----------------lables--------------*/
		$data['entry_select_timing'] = $this->language->get('title_doctor_timing');	
        $data['entry_doctors'] = $this->language->get('entry_doctors');	
		$data['entry_appointment_date'] = $this->language->get('entry_appointment_date');
		$data['entry_about_patient'] = $this->language->get('entry_about_patient');
		$data['entry_submit'] = $this->language->get('entry_submit');
		/*---------------lables---------------*/
		$doct_id = $this->customer->getId();
		//print_r($this->request->post['booking_date']); die;
		if (isset($this->error['doctor'])) {
			$data['error_doctor'] = $this->error['doctor'];
		} else {
			$data['error_doctor'] = '';
		}
		
		if (isset($this->error['appointment_date'])) {
			$data['error_appointment_date'] = $this->error['appointment_date'];
		} else {
			$data['error_appointment_date'] = '';
		}
		
		if (isset($this->error['about_patient'])) {
			$data['error_about_patient'] = $this->error['about_patient'];
		} else {
			$data['error_about_patient'] = ''; }
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
		$data['action'] = $this->url->link('patient/appointment/paid_2', '', true);
		$data['appoint_cus']=$this->request->get['cus_id'];
		$data['booking_date']=$this->request->post['booking_date'];
		$this->session->data['customerID']=$this->request->get['cus_id'];
		//$appointment_id= $this->session->data['appointment_id'];
		$doctor='';
		$data['availabilitylist'] = $this->model_patient_appointment->availabilityList($doctor, $doct_id);
		//print_r($data['availabilitylist']); die;
		$appointment_time = $this->model_patient_appointment->appointmentTime($doct_id,$this->request->post['booking_date']);
		if(!empty($appointment_time )){
		foreach ($appointment_time as $time) {
			$data['exist_appointment'][] =  $time['app_time'];
			
			
		}
		}
		else
		{
			$data['exist_appointment'] = array();
		}
		
		
		
		$this->response->setOutput($this->load->view('doctor/avality_list', $data));
	}
	
	public function paid_2() 
	{
		$this->load->language('patient/appointment');	
		$this->load->model('patient/appointment');
		 
		$data['success'] = '';
	   if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		   $appointment_id= $this->session->data['appointment_id'];
			$appointment_id = $this->model_patient_appointment->addAppointment($this->request->post, $data['photo'], $appointment_id);
			$this->response->redirect($this->url->link('patient/appointment/paid_2'));
			$data['success'] = $this->language->get('success_appoint');
		}
		
			/*----------------lables--------------*/
		$data['entry_select_timing'] = $this->language->get('title_doctor_timing');	
        $data['entry_doctors'] = $this->language->get('entry_doctors');	
		$data['entry_appointment_date'] = $this->language->get('entry_appointment_date');
		$data['entry_about_patient'] = $this->language->get('entry_about_patient');
		$data['entry_submit'] = $this->language->get('entry_submit');
		/*---------------lables---------------*/
		
		
		if (isset($this->error['doctor'])) {
			$data['error_doctor'] = $this->error['doctor'];
		} else {
			$data['error_doctor'] = '';
		}
		
		if (isset($this->error['appointment_date'])) {
			$data['error_appointment_date'] = $this->error['appointment_date'];
		} else {
			$data['error_appointment_date'] = '';
		}
		
		if (isset($this->error['about_patient'])) {
			$data['error_about_patient'] = $this->error['about_patient'];
		} else {
			$data['error_about_patient'] = ''; }
		$data['column_left'] = $this->load->controller('common/user_left');
		$data['footer'] = $this->load->controller('common/user_footer');
		$data['header'] = $this->load->controller('common/uheader');
		$data['action'] = $this->url->link('patient/appointment/paid_2', '', true);
		$data['appoint_doc']=$this->request->get['doc_id'];
		$this->session->data['doctorID']=$this->request->get['doc_id'];
		//$appointment_id= $this->session->data['appointment_id'];
		$doctor='';
		
		$data['availabilitylist'] = $this->model_patient_appointment->availabilityList($doctor, $this->request->post['DocID']);
		$appointment_time = $this->model_patient_appointment->appointmentTime($this->request->post['DocID']);
		 if(!empty($appointment_time )){
		foreach ($appointment_time as $time) {
			$data['exist_appointment'][] =  $time['app_time'];
				
			
		}
		}
		else
		{
			$data['exist_appointment'] = array();
		}
		
		
		
		$this->response->setOutput($this->load->view('patient/paid_2', $data));
	}
	
	
	
	public function payment() 
	{
		$this->load->language('patient/appointment');	
		$this->load->model('patient/appointment');
		$cus_detial=$this->model_patient_appointment->GetCustomerTitle($this->request->post['cus_id']);
		$cus_name=$cus_detial[0]['full_name'];
		$appointment_id= $this->model_patient_appointment->offfline_appointment($this->request->post,$this->customer->getId());
		
		$this->load->model('doctor/billing');
		$datapost=array();
		$datapost['patient']=$this->request->post['cus_id'];
		$datapost['title']='Cash from '.$cus_name;
		$datapost['description']='Cash from '.$cus_name.' from ';
		$datapost['amount']=$this->request->post['package'];
		$datapost['discount']=0;
		//$datapost['cash_loc']=$this->request->post['appint_loc'];
		
		
		$results = $this->model_doctor_billing->addBilling($datapost);
		
		
		if($appointment_id)
		{
			echo "<div>
			Your Appointment Has been Fixed with <br>
			User . ".$cus_name."<br>
			Date : ".$this->request->post['booking_date']."<br>
			Time : ".$this->request->post['timing']."
			</div>";
			}
		
	}
	
	
	private function validateFreeConsulation() {
         
		 if ($this->request->post['mobile'] == '') {
			$this->error['mobile'] = $this->language->get('error_mobile');
		 }
		 
		  if ($this->request->post['query'] == '') {
			$this->error['query'] = $this->language->get('error_query');
		}
		 
		if ((utf8_strlen(trim($this->request->post['query'])) < 3) || (utf8_strlen(trim($this->request->post['query'])) > 200)) {
			$this->error['query'] = $this->language->get('error_query');
		}
			
			
			
				
		return !$this->error;
	}
	  private function validate() {
         
		 if ($this->request->post['doctor'] == '') {
			$this->error['doctor'] = $this->language->get('error_doctor');
		 }
		 
		  if ($this->request->post['appointment_date'] == '') {
			$this->error['appointment_date'] = $this->language->get('error_appointment_date');
		}
		 
		if ((utf8_strlen(trim($this->request->post['about_patient'])) < 3) || (utf8_strlen(trim($this->request->post['about_patient'])) > 200)) {
			$this->error['about_patient'] = $this->language->get('error_about_patient');
		}
			
			
			
				
		return !$this->error;
	}

}