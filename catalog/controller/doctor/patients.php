<?php
class ControllerDoctorPatients extends Controller {
private $error = array();	
public function index() 

	{

		if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
		$data=array();
		$this->document->setTitle($this->language->get('Patients list'));

		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$doct_id=$this->customer->getId(); 
		$data['events'] = array();
		$filter_data = array(
		    'doctor_id'                => $doct_id,
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
			'start' => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit' => $this->config->get('config_limit_admin')
		);
		$this->load->model('doctor/patients');
		$totaldata = $this->model_doctor_patients->getPatientstotal($filter_data);
		//die();
		$results = $this->model_doctor_patients->getPatients($filter_data);
		
		$data['patients'] = $results;
		
		
		
		foreach ($results as $result) {
				$pid=$result['doc_id'];
			}
		
		
		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$pagination = new Pagination();
		$pagination->total = $totaldata;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('doctor/patients',  $url . '&page={page}', true);
		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($totaldata) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($totaldata - $this->config->get('config_limit_admin'))) ? $totaldata : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $totaldata, ceil($totaldata / $this->config->get('config_limit_admin')));
		
		
		
		$data['action'] = $this->url->link('doctor/patients/detail');
		$data['prescription_action'] = $this->url->link('doctor/patients/prescription');
		$data['column_left'] = $this->load->controller('common/doctor_left');
		$data['footer'] = $this->load->controller('common/doctor_footer');
		$data['header'] = $this->load->controller('common/dheader');
		$this->response->setOutput($this->load->view('doctor/patients', $data));

	}


			public function add()
			{
				
			$this->load->language('doctor/patients');
			//$this->session->data['prescription_sucess']='';
			if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
			
			$this->document->setTitle($this->language->get('Add Patient'));
			$data['success'] = '';
			if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
				
			
			$data['doctor_id']=$this->customer->getId(); 
			$this->load->model('doctor/patients');
			
			$results = $this->model_doctor_patients->addPrescription($this->customer->getId(),$this->request->post['patient_id'], $this->request->post);
			$this->session->data['prescription_sucess']  = 'Prescription Added Sucessfully';
			
			$this->response->redirect($this->url->link('doctor/patients/prescription&patient_id='.$this->session->data['temp_patient_id'].''));
			
			}
			if (isset($this->error['title'])) {
			$data['error_title'] = $this->error['title'];
			} else {
				$data['error_title'] = '';
			}
			if (isset($this->error['medicin'])) {
			$data['error_medicin'] = $this->error['medicin'];
			} else {
				$data['error_medicin'] = '';
			}
			if (isset($this->error['medial_test'])) {
			$data['error_medial_test'] = $this->error['medial_test'];
			} else {
				$data['error_medial_test'] = '';
			}
			if (isset($this->error['precausion'])) {
			$data['error_precausion'] = $this->error['precausion'];
			} else {
				$data['error_precausion'] = '';
			}
			if (isset($this->error['diet'])) {
			$data['error_diet'] = $this->error['diet'];
			} else {
				$data['error_diet'] = '';
			}
			if (isset($this->error['description'])) {
			$data['error_description'] = $this->error['description'];
			} else {
				$data['error_description'] = '';
			}
			$data['back'] = $this->url->link('account/account', '', true);
			$data['action'] = $this->url->link('doctor/patients/add', '', true);
			$data['patient_id']        = $this->session->data['temp_patient_id'];
			$data['column_left'] = $this->load->controller('common/doctor_left');
			$data['footer'] = $this->load->controller('common/doctor_footer');
			$data['header'] = $this->load->controller('common/dheader');
			$this->response->setOutput($this->load->view('doctor/patient_pres', $data));
			}
			public function detail()
			{
				$this->session->data['patient_id'] = $this->request->get['patient_id'];
				$this->load->model('doctor/patients');
				$this->document->setTitle('Patient Detail');
				$results = $this->model_doctor_patients->getPatientDetail($this->request->get['patient_id']);
				$data['patients'] = $results;
				if(!empty($data['patients'])){
					foreach ($results as $result) {
						$pid=$result['customer_id'];
						
			   $this->load->model('tool/image');
			
			if (!empty($patient) && (is_file(DIR_IMAGE . $result['image']))) {
				$image = $this->model_tool_image->resize($result['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['patient_image'][$result['customer_id']]=$image;
			
			$data['book_appointment'][$result['customer_id']]=$this->url->link('doctor/appointment/booking/&cus_id='.$result['customer_id'].'', '', true);
			
					}
				}
				else
				{
					$data['patient_image']='';
				$data['book_appointment']='';
				}
					
					/*if(!empty($data['patients'])){
            foreach($data['patients'] as $patient)
			{
			   $this->load->model('tool/image');
			
			if (!empty($patient) && (is_file(DIR_IMAGE . $patient['image']))) {
				$image = $this->model_tool_image->resize($patient['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$data['patient_image'][$patient['cus_id']]=$image;
			
			$data['book_appointment'][$patient['cus_id']]=$this->url->link('doctor/appointment/booking/&cus_id='.$patient['cus_id'].'', '', true);
			}
			}
			else
			{
				
				
				$data['patient_image']='';
				$data['book_appointment']='';
			}*/
						
				$this->load->model('doctor/attachment');
				
				$doct_id = $this->customer->getId();
				$userid = $this->request->get['patient_id'];
				
				$datafiles=$this->model_doctor_attachment->getpatattached($userid,$doct_id);		
				
				$data['datafiles']		= 	$datafiles;	
				
				
				
				
				$data['action'] = $this->url->link('doctor/patients/prescription', '&patient_id=' . $this->request->get['patient_id']);
				$data['column_left'] = $this->load->controller('common/doctor_left');
				$data['footer'] = $this->load->controller('common/doctor_footer');
				$data['header'] = $this->load->controller('common/dheader');
    			$this->response->setOutput($this->load->view('doctor/patient_detail', $data));
			}
			public function addfile()
			{
				$this->load->language('doctor/attachment');
				$this->document->setTitle($this->language->get('heading_title'));
				$this->load->model('doctor/attachment');
				if ($this->request->server['REQUEST_METHOD'] == 'POST')  
				{
					$doct_id = $this->customer->getId();
					$userid = $this->request->get['patient_id'];
					$this->model_doctor_attachment->addAttachmentdocp($this->request->post,$doct_id,$userid);
					$this->session->data['success'] = $this->language->get('text_upload');
					
				}

				
			}
			
			
			
				public function view_doctor()
					{
						$data['patient_id']=$this->request->get['patient_id']; 
						$this->load->model('doctor/patients');
						$dresults = $this->model_doctor_patients->getDoctor($this->request->get['patient_id']);
						$data['doctors'] = $dresults;
							foreach ($dresults as $result) {
								$pid=$result['pid'];
							}
					
					//$results = $this->model_doctor_patients->addPrescription($this->request->get['patient_id'], $this->request->post);
					$data['action'] = $this->url->link('doctor/patients/prescription_detail');
					$data['column_left'] = $this->load->controller('common/doctor_left');
				    $data['footer'] = $this->load->controller('common/doctor_footer');
   					$data['header'] = $this->load->controller('common/dheader');
			    	$this->response->setOutput($this->load->view('doctor/prescriptions', $data));
			
			}
					
	
	
	public function prescription()
			{
				
				if (!$this->customer->isLogged()) {
			$this->session->data['redirect'] = $this->url->link('account/order', '', true);

			$this->response->redirect($this->url->link('account/login', '', true));
		}
				$this->session->data['temp_patient_id']=$this->request->get['patient_id'];
				$this->document->setTitle($this->language->get('Prescription Detail'));
				$this->load->model('doctor/patients');
				$pres_results = $this->model_doctor_patients->getPrescription($this->customer->getId(), $this->request->get['patient_id']);
				
				$data['pres_results'] = $pres_results;
				$data['action'] = $this->url->link('doctor/patients/add');
					if(isset($this->session->data['prescription_sucess']) && $this->session->data['prescription_sucess']!='')
					{
						$data['prescription_sucess']=$this->session->data['prescription_sucess'];
						$this->session->data['prescription_sucess']='';
					}
					else
					{
						$data['prescription_sucess']='';
					}
					$data['column_left'] = $this->load->controller('common/doctor_left');
					$data['footer'] = $this->load->controller('common/doctor_footer');
					$data['header'] = $this->load->controller('common/dheader');
					$this->response->setOutput($this->load->view('doctor/prescription_detail', $data));
					
					}
					
					public function  get_medicine_list() 
		{
			$this->load->model('catalog/category');
			$this->load->model('doctor/patients');
			$cat=strtolower("Medicine");
			$post=$this->request->post['keyword'];
			$res=explode("-",$post);
			$keyword=$res[0];
			$txtID=$res[1];
			$filter_data = array('keyword'=> $keyword);
			
			$results_cat = $this->model_catalog_category->getCategoriesIdByTitle($cat);
			$catID       =  $results_cat[0]['category_id'];
			$results = $this->model_doctor_patients->getMedicine($catID,$filter_data);
			
			if(!empty($results)){
    	echo '<ul>';
		foreach($results as $info)
			{ ?>

<li style="text-transform:uppercase; list-style:none;"><a href="javascript:void(0);" data-value="<?php echo $info['product_id'] ?>" data-title="<?php echo $info['name'] ?>" onclick="setindiv('<?php echo $info['name'];?>','<?php echo $txtID; ?>')">&raquo;&nbsp;<?php echo $info['name'];?></a></li>
<?php
	 }
    echo '</ul>';
    }}
	public function search() 
	{
		$this->load->model('doctor/patients');
		global $EventType;
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$data['events'] = array();

		$filter_data = array(
			'filter_name'              => $filter_name,
			'filter_status'            => $filter_status,
			'filter_created_on'        => $filter_created_on,
			'sort'                     => $sort,
			'order'                    => $order,
		);
		//$cid= $this->customer->getId();
		$action = $this->url->link('doctor/patients/detail');
		$results = $this->model_doctor_patients->getSearchPatient($this->request->get['term']);
		//$results = $this->model_patient_prescription->getSearchPrescription($this->request->get['term']);
		$return='';
	
	foreach($results as $result) {
		$i=1;
		$return.= '<tr><td class="sorting_1">'.$result['firstname'].' '.$result['lastname']. '</td>
							<td>'  .$result['email'].'</td>
                          <td>' .$result['telephone'].'</td>
                          <td>' .$result['age']. '</td>
                          <td>' .$result['date_added']. '</td>
						  <td>' .$result['fee']. '</td>
						   <td><a href="'. $action.'&patient_id='. $result['pid'].'">Detail</a></td>
                          </tr>';
						
	$i++;}
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
	
	public function get_prescription_detail() 
	{
		$this->load->model('doctor/patients');
		global $EventType;
		if (isset($this->request->post['id'])) {
			$filter_id = $this->request->post['id'];
		} else {
			$filter_id = null;
		}
		$filter_data = array(
			'id'                       => $filter_id,
		
		);
		$results      = $this->model_doctor_patients->getPrescriptionInfo($filter_data);
		$results_test = $this->model_doctor_patients->getMedicalTest($filter_data);
		$array        = array_column($results_test, 'title');
		$medicalTest  = implode(",",$array); 
		
		$this->load->model('doctor/account');
		
		$customer_info=$this->model_doctor_account->getCustomer($results[0]['patient_id']);
		$doctor_info=$this->model_doctor_account->getCustomer($results[0]['doctor_id']);
		
		$pname= ucwords($customer_info['firstname'].' '.$customer_info['lastname']);
		$dname= ucwords($doctor_info['firstname'].' '.$doctor_info['lastname']);
		
		$date = date_create($results[0]['added_date']);
		$pdate= date_format($date, 'd-m-Y');
		
		//print_r($results);
		//die();
		
		$return='';
		
		$return.='<div class="col-sm-12 mr_Tb5">';
		
		$return.='<span style="float: right"><strong>Date: </strong><u>';
		$return.= $pdate;
		
		$return.='</u><span></div>';
		
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Doctor Name : </strong>';
		$return.=$dname;
		$return.='</div>';
		
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Patient Name : </strong><u>';
		$return.=$pname;
		$return.='</u><span style="float: right"><strong>Age: </strong>';
		$return.= ' ';
		
		$return.='<span></div>';
		
		
		
		//$return.='<div class="col-sm-12 mr_Tb5">';
		//$return.='<strong>Date : </strong>';
		//$return.=$results[0]['title'];
		//$return.='</div>';
		
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Summay : </strong>';
		$return.=$results[0]['description'];
		$return.='</div>';
		
		if($results[0]['precausion']!=''){
		
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Precausion : </strong>';
		$return.=$results[0]['precausion'];
		$return.='</div>';
		
		}
		if($results[0]['diet']!=''){
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Diet : </strong>';
		$return.=$results[0]['diet'];
		$return.='</div>';
		}
		if($medicalTest!=""){
		$return.='<div class="col-sm-12 mr_Tb5">';
		$return.='<strong>Medical Test : </strong>';
		$return.=$medicalTest;
		$return.='</div>';
		}
		
		$return.='<div class="clearfix"></div>';
		$return.='<br>';
		$return.='<table class="table table-striped table-bordered dataTable no-footer">';
		$return.='<tr>';
		$return.='<th>';
		$return.='Medicine';
		$return.='</th>';
		$return.='<th>';
		$return.='Dose';
		$return.='</th>';
		$return.='<th>';
		$return.='Timing';
		$return.='</th>';
		
		$return.='</tr>';
		if(!empty($results)){
		foreach($results as $result) {
		$i=1;
		$return.= '<tr><td class="sorting_1"><strong>'.$result['medicine']. '</strong></td>
							<td>'  .$result['dose'].'</td>
							<td>'  .$result['medicine_timing'].'</td>
                            </tr>';
						 
						
	$i++;}
		}
		 $return.='</table>';
		
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		echo $return;
	}
	
	public function send_mail() 
	{
		$this->load->model('doctor/patients');
		global $EventType;
		if (isset($this->request->post['id'])) {
			$filter_id = $this->request->post['id'];
		} else {
			$filter_id = null;
		}
		$filter_data = array(
			'id'                       => $filter_id,
		
		);
		$patient_info = $this->model_doctor_patients->getPatientInfoByPrescriptionId($filter_id);
		$pat_Email    = $patient_info[0]['email'];  
		$results      = $this->model_doctor_patients->getPrescriptionInfo($filter_data);
		$results_test = $this->model_doctor_patients->getMedicalTest($filter_data);
		$array        = array_column($results_test, 'title');
		$medicalTest  = implode(",",$array); 
		$return='';
		$return.='<div class="col-sm-12">';
		$return.='<strong>Precausion : </strong>';
		$return.=$results[0]['precausion'];
		$return.='</div>';
		$return.='<div class="col-sm-12">';
		$return.='<strong>Diet : </strong>';
		$return.=$results[0]['diet'];
		$return.='</div>';
		 $return.='<div class="col-sm-12">';
		$return.='<strong>Medical Test : </strong>';
		$return.=$medicalTest;
		$return.='</div>';
		$return.='<br>';
		$return.='<table style="width:100%; border:1px solid #ddd; background-color:transparent;border-collapse:collapse;border-spacing:0">';
		$return.='<tr style="box-sizing:border-box;border:1px solid #ddd;">';
		$return.='<th style="color: #70777c;font-family: "Roboto",sans-serif;font-size: 13px;font-weight: normal;text-align: left;vertical-align: bottom; border:1px solid #ddd;">';
		$return.='Medicine';
		$return.='</th>';
		$return.='<th style="color: #70777c;font-family: "Roboto",sans-serif;font-size: 13px;font-weight: normal;text-align: left;vertical-align: bottom; border:1px solid #ddd;">';
		$return.='Dose';
		$return.='</th>';
		$return.='<th style="color: #70777c;font-family: "Roboto",sans-serif;font-size: 13px;font-weight: normal;text-align: left;vertical-align: bottom; border:1px solid #ddd;">';
		$return.='Timing';
		$return.='</th>';
		
		$return.='</tr>';
		if(!empty($results)){
		foreach($results as $result) {
		$i=1;
		$return.= '<tr style="background-color: #f9f9f9;color: #70777c;font-family: "Roboto",sans-serif;font-size: 12px;font-weight: 400;text-align: center;border: 1px solid #ddd;"><td style="border:1px solid #ddd; text-align: center">'.$result['medicine']. '</td>
							<td style="border:1px solid #ddd; text-align: center">'  .$result['dose'].'</td>
							<td style="border:1px solid #ddd; text-align: center">'  .$result['medicine_timing'].'</td>
                            </tr>';
						 
						
	$i++;}
		}
		 $return.='</table>';
		//$results = $this->model_patient_doctor->getSerachDoctors($this->request->get['term']);
		
		$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			
			
			$mail->setTo($pat_Email);
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode('Medivow', ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode(sprintf('Prescription Detail'), ENT_QUOTES, 'UTF-8'));
			$mail->setHtml($return);
			$mail->send();
			
	}
	public function validate() {
      
		 if ($this->request->post['title'] == '') {
			 
			$this->error['title'] = $this->language->get('error_title');
		 }
		
		  /*if ($this->request->post['medicin'] == '') {
			$this->error['medicin'] = $this->language->get('error_medicin');
		}
		if ($this->request->post['medial_test'] == '') {
			$this->error['medial_test'] = $this->language->get('error_medial_test');
		}*/
		if ($this->request->post['precausion'] == '') {
			$this->error['precausion'] = $this->language->get('error_precausion');
		}
		if ($this->request->post['diet'] == '') {
			$this->error['diet'] = $this->language->get('error_diet');
		}
		if ($this->request->post['description'] == '') {
			$this->error['description'] = $this->language->get('error_description');
		}
		 
		/*if ((utf8_strlen(trim($this->request->post['query'])) < 3) || (utf8_strlen(trim($this->request->post['query'])) > 200)) {
			$this->error['query'] = $this->language->get('error_query');
		}
			*/
			
		
		return !$this->error;
	}
}
