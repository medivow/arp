<?php 
/**
 * doctorlist.php
 *
 * Doctor Listing
 *
 * @author     Dhirendra
  */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');
class ControllerRestPatientdashboard extends RestController {
	/*
	* Get doctor dashboards 
	*/
	
	public function doctordashboard() {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
			$url='';
		$this->load->language('doctor/dailyessay');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('patient/patient');
		$this->load->model('doctor/dailyessay');
		$this->load->model('doctor/patients');
		$this->load->model('doctor/doctor');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['appointments'] = array();
		$json["data"]['casestudy'] = array();
		$json["data"]['patientlist'] = array();
		$json["data"]['dailyessay'] = array();
    	$data['breadcrumbs'] = array();
		$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		$results = $this->model_doctor_case->getInformations($doct_id);

		foreach ($results as $result) {
			
			$json['data']['casestudy'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/case/detail', 'case_id=' . $result['id'] . $url, true),
			);
		}
		$results_essay = $this->model_doctor_dailyessay->getDailyEssays($doct_id);
		 if(!empty($results_essay )){
		foreach ($results_essay as $essay) {
			
			$json['data']['dailyessay'][] = array(
				'id' => $result['id'],
				'title'          => $essay['title'],
				'description'   =>$essay['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/dailyessay/detail', 'essay_id=' . $result['id'] . $url, true),
				
			);
		}
		}
		   $this->document->setTitle($this->language->get('Dashboard'));
		    $filter_data=array();
		    $results_patent = $this->model_doctor_patients->getPatients($filter_data);
			$filter_data1 = array(
		            'doctor_id'                => $doct_id,
					
				);
			$results_event = $this->model_doctor_doctor->getEvents($filter_data1);
			$event = array();
			foreach ($results_event as $result) {
				$event[] = array(
					"id"=> $result['aid'],
					"title"	=> $result['firstname'].' '.$result['lastname'],
					"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
					"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
				);
			}
			$json['data']['patientlist']   =  $results_patent;
			$json['data']['appointments']   =  $event;
			$this->document->setTitle($this->language->get('text_error'));
			
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
  	public function getprescription() {
		$json = array('success' => true);
		if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}
		if(!isset($json["error"])){
		$this->load->language('patient/prescription');
		$this->load->model('patient/prescription');
		$this->document->setTitle($this->language->get('heading_title'));
		$p_id=$_REQUEST['patient_id'];	
		$json["data"]['prescriptions'] = array();
    	$data['breadcrumbs'] = array();
		$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		    $filter_data = array('customer_id'=>$this->customer->getId());
			$results = $this->model_patient_prescription->getPrescriptions($filter_data);
			$json["data"]['prescriptions']=$results;
			$this->document->setTitle($this->language->get('text_error'));
			$json["data"]['heading_title'] = $this->language->get('heading_title');
			$json["data"]['model_heading_title'] = $this->language->get('model_heading_title');
			$json["data"]['model_pres_title'] = $this->language->get('model_pres_title');
			$json["data"]['model_pres_medicine'] = $this->language->get('model_pres_medicine');
			$json["data"]['model_pres_precautions'] = $this->language->get('model_pres_precautions');
			$json["data"]['model_pres_diet'] = $this->language->get('model_pres_diet');
			$json["data"]['model_pres_description'] = $this->language->get('model_pres_description');
			$json["data"]['model_pres_adddate']=$this->language->get('model_pres_adddate');
			$json["data"]['model_pres_docname']=$this->language->get('model_pres_docname');
			$this->document->setTitle($this->language->get('heading_title'));
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	   public function getappointment()
	   {
		$json = array('success' => true);
		if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}
		if(!isset($json["error"])){
		$this->load->language('patient/patient');
		$this->document->setTitle($this->language->get('Calendar'));
		$this->load->model('patient/patient');
		$p_id=$_REQUEST['patient_id'];	
		$json["data"]['appointments'] = array();
    	$data['breadcrumbs'] = array();
		$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
		            'patient_id'                => $p_id,
					'filter_name'              => $filter_name,
					'filter_status'            => $filter_status,
					'filter_created_on'        => $filter_created_on,
					'sort'                     => $sort,
					'order'                    => $order,
				);
			$results = $this->model_patient_patient->getEvents($filter_data);
			$event = array();
			foreach ($results as $result) {
				$event[] = array(
					"id"=> $result['aid'],
					"title"	=> $result['firstname'].' '.$result['lastname'],
					"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
					"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
				);
			}
			$json['data']['appointments']   =  $event;
		
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	      }