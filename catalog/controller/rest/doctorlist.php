<?php 
/**
 * doctorlist.php
 *
 * Doctor Listing
 *
 * @author     Dhirendra
  */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestDoctorList extends RestController {
	
	/*
	* Get doctorlist
	*/
  	public function doctorlist() {

		$json = array('success' => true);

		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		
		if(!isset($json["error"])){
			$this->load->language('search/search');
		    $this->load->model('search/search');
			$this->load->model('tool/image');
			
			$json["data"]['doctors'] = array();

			$data['breadcrumbs'] = array();

		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/

		/*$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);*/
			$json['data']['doctors']= $this->model_search_search->getDoctorsList();
			if(!empty($data['doctors'])){
            foreach($data['doctors'] as $doctor)
			{
			$cus_services= $this->model_search_search->getCustomerSpecilityById($doctor['customer_id']);
			$json['data']['doctor_speciality'][$doctor['customer_id']]=implode(' , ',$cus_services);	
			$this->load->model('tool/image');
			if (!empty($doctor) && (is_file(DIR_IMAGE . $doctor['image']))) {
				$image = $this->model_tool_image->resize($doctor['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$json['data']['doctor_image'][$doctor['customer_id']]=$image;
			}
			}
			else
			{
				
				$json['data']['doctor_speciality']='';
				$json['data']['doctor_image']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$json['data']['heading_title'] = $this->language->get('doctor_list_title');

			$json['data']['text_error'] = $this->language->get('text_error');

			$json['data']['button_continue'] = $this->language->get('button_continue');


		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}

	
	
}
}