<?php 
/**
 * patientlist.php
 *
 * Patient Listing
 *
 * @author     Dhirendra
  */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestPatientList extends RestController {
	
	/*
	* Get patientlist
	*/
  	public function patientlist() {

		$json = array('success' => true);
	   if(!isset($json["error"])){
			$this->load->language('search/search');
		    $this->load->model('search/search');
			$this->load->model('tool/image');
			$json["data"]['patients'] = array();
	$data['breadcrumbs'] = array();

		$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
			$json['data']['patients']= $this->model_search_search->getPatients();
			if(!empty($data['patients'])){
            foreach($data['patients'] as $patient)
			{
			$this->load->model('tool/image');
			if (!empty($doctor) && (is_file(DIR_IMAGE . $patient['image']))) {
				$image = $this->model_tool_image->resize($patient['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$json['data']['patient_image'][$patient['customer_id']]=$image;
			}
			}
			else
			{
				
				$json['data']['doctor_image']='';
			}
			
			$this->document->setTitle($this->language->get('text_error'));

			$json['data']['heading_title'] = $this->language->get('patient_list_title');

			$json['data']['text_error'] = $this->language->get('text_error');

			$json['data']['button_continue'] = $this->language->get('button_continue');


		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}

	
	
}

public function patientdetail() {
		$json = array('success' => true);
      /*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
			$this->load->language('search/search');
		    $this->load->model('search/detail');
		    $patient_id=$_REQUEST['id'];
			$this->load->model('tool/image');
			$json["data"]['doctors'] = array();
			$data['breadcrumbs'] = array();
		    $json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		/*$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/information', 'information_id=' . $information_id)
			);*/
			$json['data']['patients']= $this->model_search_detail->getPatientDetail($patient_id);
			if(!empty($data['patients'])){
            foreach($data['patients'] as $patient)
			{
			$this->load->model('tool/image');
			if (!empty($patient) && (is_file(DIR_IMAGE . $patient['image']))) {
				$image = $this->model_tool_image->resize($patient['image'], 121, 149);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 121, 149);
			}
			$json['data']['patient_image'][$patient['customer_id']]=$image;
			}
			}
			else
			{
				
				$json['data']['patient_image']='';
			}
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('patient_detail_title');
			$json['data']['text_error'] = $this->language->get('text_error');
			$json['data']['button_continue'] = $this->language->get('button_continue');
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
	}

	
	
}
}