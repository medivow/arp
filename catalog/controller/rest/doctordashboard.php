<?php 
/**
 * doctorlist.php
 *
 * Doctor Listing
 *
 * @author     Dhirendra
  */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');
class ControllerRestDoctordashboard extends RestController {
	/*
	* Get doctor dashboards 
	*/
  	
	public function doctordashboard() {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
			$url='';
		$this->load->language('doctor/dailyessay');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/case');
		$this->load->model('doctor/dailyessay');
		$this->load->model('doctor/patients');
		$this->load->model('doctor/doctor');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['appointments'] = array();
		$json["data"]['casestudy'] = array();
		$json["data"]['patientlist'] = array();
		$json["data"]['dailyessay'] = array();
    	$data['breadcrumbs'] = array();
		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		$results = $this->model_doctor_case->getInformations($doct_id);

		foreach ($results as $result) {
			
			$json['data']['casestudy'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/case/detail', 'case_id=' . $result['id'] . $url, true),
			);
		}
		$results_essay = $this->model_doctor_dailyessay->getDailyEssays($doct_id);
		 if(!empty($results_essay )){
		foreach ($results_essay as $essay) {
			
			$json['data']['dailyessay'][] = array(
				'id' => $result['id'],
				'title'          => $essay['title'],
				'description'   =>$essay['description'],
				'added_dt'      =>$result['added_date'],
				'detail'           => $this->url->link('doctor/dailyessay/detail', 'essay_id=' . $result['id'] . $url, true),
				
			);
		}
		}
		   $this->document->setTitle($this->language->get('Dashboard'));
		    $filter_data=array();
		    $results_patent = $this->model_doctor_patients->getPatients($filter_data);
			$filter_data1 = array(
		            'doctor_id'                => $doct_id,
					
				);
			$results_event = $this->model_doctor_doctor->getEvents($filter_data1);
			$event = array();
			foreach ($results_event as $result) {
				$event[] = array(
					"id"=> $result['aid'],
					"title"	=> $result['firstname'].' '.$result['lastname'],
					"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
					"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
				);
			}
			$json['data']['patientlist']   =  $results_patent;
			$json['data']['appointments']   =  $event;
			$this->document->setTitle($this->language->get('text_error'));
			
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	
	public function getdailyessay() {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
		$this->load->language('doctor/dailyessay');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/dailyessay');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['dailyessays'] = array();
    	$data['breadcrumbs'] = array();
		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$url = '';
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}
		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		$results = $this->model_doctor_dailyessay->getDailyEssays($doct_id);
        if(!empty($results )){
		foreach ($results as $result) {
			$json['data']['dailyessays'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'edit'           => $this->url->link('doctor/dailyessay/edit', 'essay_id=' . $result['id'] . $url, true),
				'delete'           => $this->url->link('doctor/dailyessay/delete', 'essay_id=' . $result['id'] . $url, true),
			);
		}
		}
		else
		{
			$json['data']['dailyessays'] = array();
		}
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['text_list'] = $this->language->get('text_list');
			$json['data']['text_no_results'] = $this->language->get('text_no_results');
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['text_confirm'] = $this->language->get('text_confirm');
			$json['data']['text_error'] = $this->language->get('text_error');
			$json['data']['column_title'] = $this->language->get('column_title');
			$json['data']['column_action'] = $this->language->get('column_action');
			$json['data']['button_add'] = $this->language->get('button_add');
			$json['data']['button_edit'] = $this->language->get('button_edit');
			$json['data']['button_delete'] = $this->language->get('button_delete');
			$json['data']['button_continue'] = $this->language->get('button_continue');
		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	   public function getcasestudy() {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
		$this->load->language('doctor/case');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('doctor/case');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['dailyessays'] = array();
    	$data['breadcrumbs'] = array();
		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'id.title';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$results = $this->model_doctor_case->getInformations($doct_id);
        if(!empty($results )){
		foreach ($results as $result) {
			$json['data']['casestuday'][] = array(
				'id' => $result['id'],
				'title'          => $result['title'],
				'description'   =>$result['description'],
				'edit'           => $this->url->link('doctor/case/edit', 'case_id=' . $result['id'] . $url, true),
				'delete'           => $this->url->link('doctor/case/delete', 'case_id=' . $result['id'] . $url, true),
			);
		}
		}
		else
		{
			$json['data']['casestuday'] = array();
		}
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['text_list'] = $this->language->get('text_list');
			$json['data']['text_no_results'] = $this->language->get('text_no_results');
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['text_confirm'] = $this->language->get('text_confirm');
			$json['data']['text_error'] = $this->language->get('text_error');
			$json['data']['column_title'] = $this->language->get('column_title');
			$json['data']['column_action'] = $this->language->get('column_action');
			$json['data']['button_add'] = $this->language->get('button_add');
			$json['data']['button_edit'] = $this->language->get('button_edit');
			$json['data']['button_delete'] = $this->language->get('button_delete');
			$json['data']['button_continue'] = $this->language->get('button_continue');
			
			if (isset($this->error['warning'])) {
			$json['data']['error_warning'] = $this->error['warning'];
		} else {
			$json['data']['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$json['data']['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$json['data']['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$json['data']['selected'] = (array)$this->request->post['selected'];
		} else {
			$json['data']['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$json['data']['sort_title'] = $this->url->link('doctor/case','sort=id.title' . $url, true);
		$json['data']['sort_sort_order'] = $this->url->link('doctor/case', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	   public function getpatients() {
	
		$json = array('success' => true);
		if (!$this->customer->isLogged()) {			
			$json["success"] = true;		
			$json["error"] = "User is not logged!";		
		}
		
		if(!isset($json["error"])||isset($json["error"])){
		$this->load->language('doctor/case');
		$this->document->setTitle($this->language->get('Patients list'));
		$this->load->model('doctor/patients');
		$doct_id=$_REQUEST['doct_id'];
		$json["data"]['dailyessays'] = array();
    	/*$data['breadcrumbs'] = array();
		$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}
		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
		$filter_data = array(
		            'doctor_id'                => $doct_id,
					'filter_name'              => $filter_name,
					'filter_status'            => $filter_status,
					'filter_created_on'        => $filter_created_on,
					'sort'                     => $sort,
					'order'                    => $order,
				);
			$this->load->model('doctor/patients');
			$results = $this->model_doctor_patients->getPatients($filter_data);
			$json['data']['patients']   =  $results;
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['action']        = $this->url->link('doctor/patients/detail');
			$json['data']['text_no_results'] = $this->language->get('text_no_results');
			$json['data']['heading_title'] = $this->language->get('heading_title');
			/*$json['data']['text_confirm'] = $this->language->get('text_confirm');*/
			$json['data']['text_error'] = $this->language->get('text_error');
			$json['data']['column_title'] = $this->language->get('column_title');
			$json['data']['column_action'] = $this->language->get('column_action');
			$json['data']['button_add'] = $this->language->get('button_add');
			$json['data']['button_edit'] = $this->language->get('button_edit');
			$json['data']['button_delete'] = $this->language->get('button_delete');
			$json['data']['button_continue'] = $this->language->get('button_continue');
			
			if (isset($this->error['warning'])) {
			$json['data']['error_warning'] = $this->error['warning'];
		} else {
			$json['data']['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$json['data']['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$json['data']['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$json['data']['selected'] = (array)$this->request->post['selected'];
		} else {
			$json['data']['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$json['data']['sort_title'] = $this->url->link('doctor/case','sort=id.title' . $url, true);
		$json['data']['sort_sort_order'] = $this->url->link('doctor/case', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	   public function getcalendar()
	   {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
		$this->load->language('doctor/case');
		$this->document->setTitle($this->language->get('Calendar'));
		$this->load->model('doctor/doctor');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['appointments'] = array();
    	$data['breadcrumbs'] = array();
		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$filter_data = array(
		            'doctor_id'                => $doct_id,
					'filter_name'              => $filter_name,
					'filter_status'            => $filter_status,
					'filter_created_on'        => $filter_created_on,
					'sort'                     => $sort,
					'order'                    => $order,
				);
			$results = $this->model_doctor_doctor->getEvents($filter_data);
			$event = array();
			foreach ($results as $result) {
				$event[] = array(
					"id"=> $result['aid'],
					"title"	=> $result['firstname'].' '.$result['lastname'],
					"start"	=> date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time'])),
					"end"	=>  date('Y-m-d\TH:i:s', strtotime($result['app_date'].' '.$result['app_time']))
				);
			}
			$json['data']['appointments']   =  $event;
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['action']        = $this->url->link('doctor/patients/detail');
			$json['data']['text_no_results'] = $this->language->get('text_no_results');
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['text_confirm'] = $this->language->get('text_confirm');
			$json['data']['text_error'] = $this->language->get('text_error');
			$json['data']['column_title'] = $this->language->get('column_title');
			$json['data']['column_action'] = $this->language->get('column_action');
			$json['data']['button_add'] = $this->language->get('button_add');
			$json['data']['button_edit'] = $this->language->get('button_edit');
			$json['data']['button_delete'] = $this->language->get('button_delete');
			$json['data']['button_continue'] = $this->language->get('button_continue');
			
			if (isset($this->error['warning'])) {
			$json['data']['error_warning'] = $this->error['warning'];
		} else {
			$json['data']['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$json['data']['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$json['data']['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$json['data']['selected'] = (array)$this->request->post['selected'];
		} else {
			$json['data']['selected'] = array();
		}

		$url = '';

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$json['data']['sort_title'] = $this->url->link('doctor/case','sort=id.title' . $url, true);
		$json['data']['sort_sort_order'] = $this->url->link('doctor/case', 'sort=i.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}


		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
	   
	   public function getbilling()
	   {
		$json = array('success' => true);
		/*if (!$this->customer->isLogged()) {			
			$json["success"] = false;		
			$json["error"] = "User is not logged!";		
		}*/
		if(!isset($json["error"])){
		$this->load->language('doctor/billing');
		$this->document->setTitle($this->language->get('Calendar'));
		$this->load->model('doctor/billing');
		$doct_id=$_REQUEST['doct_id'];	
		$json["data"]['billing'] = array();
    	$data['breadcrumbs'] = array();
		/*$json["data"]['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);*/
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['filter_created_on'])) {
			$filter_created_on = $this->request->get['filter_created_on'];
		} else {
			$filter_created_on = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}


		$filter_data = array(
		            'doctor_id'                => $doct_id,
					'filter_name'              => $filter_name,
					'filter_status'            => $filter_status,
					'filter_created_on'        => $filter_created_on,
					'sort'                     => $sort,
					'order'                    => $order,
				);
			$results = $this->model_doctor_billing->getBillingInfo($filter_data);
		
		$data['billing'] = $results;
			$event = array();
			$json['data']['billing']   =  $event;
			$this->document->setTitle($this->language->get('text_error'));
			$json['data']['heading_title'] = $this->language->get('heading_title');
			$json['data']['action']        = $this->url->link('doctor/billing/add');
			
			if (isset($this->error['patient'])) {
			$json['data']['error_patient'] = $this->error['patient'];
		} else {
			$json['data']['error_patient'] = '';
		}

		if (isset($this->error['title'])) {
			$json['data']['error_title'] = $this->error['title'];
		} else {
			$json['data']['error_title'] = '';
		}
		
		if (isset($this->error['description'])) {
			$json['data']['error_description'] = $this->error['description'];
		} else {
			$json['data']['error_description'] = '';
		}
		
		if (isset($this->error['amount'])) {
			$json['data']['error_amount'] = $this->error['amount'];
		} else {
			$json['data']['error_amount'] = '';
		}

		if (isset($this->error['discount'])) {
			$json['data']['error_discount'] = $this->error['discount'];
		} else {
			$json['data']['error_discount'] = '';
		}


		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
    	}
       }
    }