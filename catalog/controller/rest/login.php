<?php  

/**
 * login.php
 *
 * Login management
 *
 * @author     Makai Lajos
 * @copyright  2015
 * @license    License.txt
 * @version    2.0
 * @link       http://opencart-api.com/product/opencart-restful-api-pro-v2-0/
 * @see        http://newapi.opencart-api.com/demo/
 */
require_once(DIR_SYSTEM . 'engine/restcontroller.php');

class ControllerRestLogin extends RestController {

	const FACEBOOK_USER_INFORMATION_URL = 'https://graph.facebook.com/me?fields=email,name';
	const GOOGLE_USER_INFORMATION_URL = 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json';

	/*
	* Login user
	*/
	public function login() { 
		$this->checkPlugin();
		$json = array('success' => true);
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){
			
			$requestjson = file_get_contents('php://input');
		
			$requestjson = json_decode($requestjson, true);

			$post		 = $requestjson;
           
			$this->language->load('checkout/checkout');
			$this->load->model('account/customer_group');	
			if ($this->customer->isLogged()) {
				$json['error']		= "User already is logged";			
				$json['success']	= false;			
			}

			/*if ((!$this->cart->hasProducts() && empty($this->session->data['vouchers'])) || (!$this->cart->hasStock() && !$this->config->get('config_stock_checkout'))) {
				$json['error']		= "Something went wrong";			
				$json['success']	= false;			
			}*/	
			
			if ($json['success']) {
				//if (!$this->customer->login($post['email'], $post['password'])) {
					if(!$this->customer->login($_POST['username'], $_POST['password'])){
					$json['error']['warning'] = $this->language->get('error_login');
					$json['success']	= false;
				}
			
				$this->load->model('account/customer');
			
				$customer_info = $this->model_account_customer->getCustomerByUsername($_POST['username']);
				$Customer_group_Type = $this->model_account_customer_group->getCustomerGroupsNameByID($customer_info['customer_group_id']);
				if ($customer_info && !$customer_info['approved']) {
					$json['error']['warning'] = $this->language->get('error_approved');
					$json['success']	= false;
				}		
			}
			
			if ($json['success']) {
				unset($this->session->data['guest']);
				// Default Addresses
				$this->load->model('account/address');
				$address_info = $this->model_account_address->getAddress($this->customer->getAddressId());
				if ($address_info) {
					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_country_id'] = $address_info['country_id'];
						$this->session->data['shipping_zone_id'] = $address_info['zone_id'];
						$this->session->data['shipping_postcode'] = $address_info['postcode'];	
					}
					
					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_country_id'] = $address_info['country_id'];
						$this->session->data['payment_zone_id'] = $address_info['zone_id'];
					}
				} else {
					unset($this->session->data['shipping_country_id']);	
					unset($this->session->data['shipping_zone_id']);	
					unset($this->session->data['shipping_postcode']);
					unset($this->session->data['payment_country_id']);	
					unset($this->session->data['payment_zone_id']);	
				}
				

				unset($customer_info['password']);
				unset($customer_info['token']);
				unset($customer_info['salt']);
				$customer_info["session"] = session_id();
				
				
				
				$json['data'] = $customer_info;
				$json['data']['userid'] =  $this->customer->getId();
				$json['data']['customer_group']=$Customer_group_Type[0]['name'];
				//$json['data']['usergroup'] = $this->customer->getCustomerGroupId();
			}
						
		}else {
				$json["error"]		= "Only POST request method allowed";
				$json["success"]	= false;
		}

		if ($this->debugIt) {
			echo '<pre>';
			print_r($json);
			echo '</pre>';
		} else {
			$this->response->setOutput(json_encode($json));
		}
	
	}

	/*
	Only for testing
	Depracted
	http:/opencart3.my/api/rest/checkuser/key/123
	*/	
	public function checkuser() {

		$json = array('success' => true);	

		if (!$this->customer->isLogged()) {
				$json['error']		= "User is not logged";			
				$json['success']	= false;			
		}else {
			$json['data'] 		= $this->customer->getEmail();			
		}
		
		if ($this->debugIt) {
				echo '<pre>';
				print_r($json);
		} else {
				$this->response->setOutput(json_encode($json));
		}
    }

	/*
	* Login user
	*/
	public function sociallogin() {

		$this->checkPlugin();

		$json = array('success' => true);
		$customer_info = array();
		$firstname = "";
		$lastname = "";
		if ( $_SERVER['REQUEST_METHOD'] === 'POST' ){

			$input = file_get_contents('php://input');

			$post = json_decode($input, true);

			$this->language->load('checkout/checkout');

			if ($this->customer->isLogged()) {
				$json['error']		= "User already is logged";
				$json['success']	= false;
			}

			if ($json['success']) {
				$this->load->model('account/customer');

				$customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);

				if(!isset($post['provider']) || ($post['provider'] != 'facebook' && $post['provider'] != 'google')) {
					$json['error']		= "Invalid social provider";
					$json['success']	= false;
				}

				if(isset($post['provider']) && ($post['provider'] == 'facebook' || $post['provider'] == 'google')) {
					$social = $this->requestUserDataFromProvider($post['provider'], $post['access_token']);

					if (empty($social) || $social['email'] != $post['email']) {
						$json['error'] = "Social email and posted email mismatch";
						$json['success'] = false;
					} else {
						if(isset($social['name'])){
							$exploded = explode(' ', $social['name']);
							$firstname = array_shift($exploded);
							$lastname= implode(' ', $exploded);
						}
					}
				}

				if($json['success']) {
					//if email does not exist, register as a new customer
					if (!$customer_info) {
						$data['email'] = $post['email'];
						$data['firstname'] = $firstname;
						$data['lastname'] = $lastname;
						$data['telephone'] = "";
						$data['address_1'] = "";
						$data['city'] = "";
						$data['postcode'] = "";
						$data['country'] = "";
						$data['zone_id'] = "";
						$data['approved'] = 1;
						$data['password'] = md5(microtime());

						$this->model_account_customer->addCustomer($data);
						$customer_info = $this->model_account_customer->getCustomerByEmail($post['email']);
					}

					if ($customer_info) {
						if (!$this->customer->login($post['email'], "", true)) {
							$json['error']['warning'] = $this->language->get('error_login');
							$json['success'] = false;
						}
					}

					if ($customer_info && !$customer_info['approved']) {
						$json['error']['warning'] = $this->language->get('error_approved');
						$json['success'] = false;
					}
				}
			}

			if ($json['success'] && !empty($customer_info)) {
				unset($this->session->data['guest']);

				// Default Addresses
				$this->load->model('account/address');

				$address_info = $this->model_account_address->getAddress($this->customer->getAddressId());

				if ($address_info) {
					if ($this->config->get('config_tax_customer') == 'shipping') {
						$this->session->data['shipping_country_id'] = $address_info['country_id'];
						$this->session->data['shipping_zone_id'] = $address_info['zone_id'];
						$this->session->data['shipping_postcode'] = $address_info['postcode'];
					}

					if ($this->config->get('config_tax_customer') == 'payment') {
						$this->session->data['payment_country_id'] = $address_info['country_id'];
						$this->session->data['payment_zone_id'] = $address_info['zone_id'];
					}
				} else {
					unset($this->session->data['shipping_country_id']);
					unset($this->session->data['shipping_zone_id']);
					unset($this->session->data['shipping_postcode']);
					unset($this->session->data['payment_country_id']);
					unset($this->session->data['payment_zone_id']);
				}


				unset($customer_info['password']);
				unset($customer_info['token']);
				unset($customer_info['salt']);
				$customer_info["session"] = session_id();
				$json['data'] = $customer_info;
			}

		}else {
			$json["error"]		= "Only POST request method allowed";
			$json["success"]	= false;
		}

		$this->sendResponse($json);
	}

	private function requestUserDataFromProvider($provider, $access_token) {
		$ch = curl_init();
		if($provider == 'facebook'){
			curl_setopt($ch, CURLOPT_URL, static::FACEBOOK_USER_INFORMATION_URL);
		} elseif ($provider == 'google') {
			curl_setopt($ch, CURLOPT_URL, static::GOOGLE_USER_INFORMATION_URL);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		$headers = array("Authorization: Bearer " . $access_token);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		// execute the CURL request.
		$data = curl_exec($ch);
		if(!curl_errno($ch)){
			curl_close($ch);
			return json_decode($data, true);
		} else {
			curl_close($ch);
			return NULL;
		}
	}
}