<?php
class ControllerAccountRegister1 extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('account/register');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');
			$this->load->model('account/customer');
			
		if(isset($this->request->request['package'])) {
		
			$customer_id = $this->model_account_customer->getCustomerId();
			$cust_id= $this->session->data['cust_id']; 
			$customer_id = $this->model_account_customer->addCustomerPackage($cust_id, $this->request->request['package']);
			
			
			$this->response->redirect($this->url->link('account/register2'));
		}
			$data['action_free'] = $this->url->link('account/register1', 'package=free', true);
			$data['action_premium'] = $this->url->link('account/register1', 'package=Premium', true);
			$data['action_business'] = $this->url->link('account/register1', 'package=Business Solution', true);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['slider'] = $this->load->controller('common/slider');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('account/register1', $data));
	}
	
	
}