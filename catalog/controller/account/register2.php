<?php
class ControllerAccountRegister2 extends Controller {
		private $error = array();

	public function index() {
		if ($this->customer->isLogged()) {
			$this->load->model('account/customer_group');
				$Customer_group_Type = $this->model_account_customer_group->getCustomerGroupsNameByID($this->customer->getGroupId());

				if(strtolower($Customer_group_Type[0]['name'])=='doctors'){
				$this->response->redirect($this->url->link('doctor/doctor', '', true));
				}
				else
				{
				$this->response->redirect($this->url->link('patient/patient', '', true));	
				}
		}

		$this->load->language('account/register');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/moment.js');
		$this->document->addScript('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css');

		$this->load->model('account/customer');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$cust_id= $this->session->data['cust_id']; 
			$customer_id = $this->model_account_customer->updateCustomer($cust_id, $this->request->post);
			// Add to activity log
			$this->load->model('account/activity');

			$activity_data = array(
				'customer_id' => $this->customer->getId(),
				'customer_group_id' => $this->customer->getGroupId(),
				'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName()
			);

			$this->model_account_activity->addActivity('login', $activity_data);
			// Added strpos check to pass McAfee PCI compliance test (http://forum.opencart.com/viewtopic.php?f=10&t=12043&p=151494#p151295)
			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], $this->config->get('config_url')) !== false || strpos($this->request->post['redirect'], $this->config->get('config_ssl')) !== false)) {
				$this->response->redirect(str_replace('&amp;', '&', $this->request->post['redirect']));
			} else {
				$this->load->model('account/customer_group');
				$Customer_group_Type = $this->model_account_customer_group->getCustomerGroupsNameByID($activity_data['customer_group_id']);
			
				$this->response->redirect($this->url->link('doctor/doctor', '', true));
			
				
				
			}
			$baseurl= $this->data['base'] = $this->config->get('config_url').'admin';
			$this->response->redirect($baseurl);
		}
			$this->load->model('localisation/country');
   			$countries = $this->model_localisation_country->getCountries();
						foreach ($countries as $country) {
					$data['countries'][] = $country;
			}
			$this->load->model('account/customer');
		$data['doc_speciality'] = $this->model_account_customer->getSpeciality();
		$data['heading_title'] = $this->language->get('heading_title');
        $data['specialities']    = $data['doc_speciality'];
		
		$data['text_account_already'] = sprintf($this->language->get('text_account_already'), $this->url->link('account/login', '', true));
		$data['text_your_details'] = $this->language->get('text_your_details');
		$data['text_your_address'] = $this->language->get('text_your_address');
		$data['text_your_password'] = $this->language->get('text_your_password');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_loading'] = $this->language->get('text_loading');
		$data['text_Availability'] = $this->language->get('text_Availability');

		$data['entry_customer_group'] = $this->language->get('entry_customer_group');
		$data['entry_firstname'] = $this->language->get('entry_firstname');
		$data['entry_lastname'] = $this->language->get('entry_lastname');
		$data['entry_username'] = $this->language->get('entry_username');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_speciality'] = $this->language->get('entry_speciality');
		$data['entry_timing'] = $this->language->get('entry_timing');
		$data['entry_telephone'] = $this->language->get('entry_telephone');
		$data['entry_fax'] = $this->language->get('entry_fax');
		$data['entry_company'] = $this->language->get('entry_company');
		$data['entry_address_1'] = $this->language->get('entry_address_1');
		$data['entry_address_2'] = $this->language->get('entry_address_2');
		$data['entry_postcode'] = $this->language->get('entry_postcode');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_country'] = $this->language->get('entry_country');
		$data['entry_zone'] = $this->language->get('entry_zone');
		$data['entry_newsletter'] = $this->language->get('entry_newsletter');
		$data['entry_password'] = $this->language->get('entry_password');
		$data['entry_confirm'] = $this->language->get('entry_confirm');

		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_upload'] = $this->language->get('button_upload');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		
		
		if (isset($this->error['office_timing'])) {
			$data['error_office_timimg'] = $this->error['office_timing'];
		} else {
			$data['error_office_timimg'] = '';
		}

		if (isset($this->error['telephone'])) {
			$data['error_telephone'] = $this->error['telephone'];
		} else {
			$data['error_telephone'] = '';
		}
		
		if (isset($this->error['ima'])) {
			$data['error_ima'] = $this->error['ima'];
		} else {
			$data['error_ima'] = '';
		}
		
		
		
		if (isset($this->error['fax'])) {
			$data['error_fax'] = $this->error['fax'];
		} else {
			$data['error_fax'] = '';
		}
		
		if (isset($this->error['about_me'])) {
			$data['error_about_me'] = $this->error['about_me'];
		} else {
			$data['error_about_me'] = '';
		}

		if (isset($this->error['address_1'])) {
			$data['error_address_1'] = $this->error['address_1'];
		} else {
			$data['error_address_1'] = '';
		}

		if (isset($this->error['city'])) {
			$data['error_city'] = $this->error['city'];
		} else {
			$data['error_city'] = '';
		}
		
		if (isset($this->error['availability'])) {
			$data['error_availability'] = $this->error['availability'];
		} else {
			$data['error_availability'] = '';
		}

		if (isset($this->error['postcode'])) {
			$data['error_postcode'] = $this->error['postcode'];
		} else {
			$data['error_postcode'] = '';
		}

		if (isset($this->error['country'])) {
			$data['error_country'] = $this->error['country'];
		} else {
			$data['error_country'] = '';
		}

		if (isset($this->error['zone'])) {
			$data['error_zone'] = $this->error['zone'];
		} else {
			$data['error_zone'] = '';
		}

		if (isset($this->error['custom_field'])) {
			$data['error_custom_field'] = $this->error['custom_field'];
		} else {
			$data['error_custom_field'] = array();
		}
		$data['action'] = $this->url->link('account/register2', '', true);

		if (isset($this->request->post['office_timing'])) {
			$data['office_timing'] = $this->request->post['office_timing'];
		} else {
			$data['office_timing'] = '';
		}
		
		if (isset($this->request->post['about_me'])) {
			$data['about_me'] = $this->request->post['about_me'];
		} else {
			$data['about_me'] = '';
		}
		
		if (isset($this->request->post['ima'])) {
			$data['ima'] = $this->request->post['ima'];
		} else {
			$data['ima'] = '';
		}
		if (isset($this->request->post['speciality'])) {
			$data['special'] = $this->request->post['speciality'];
		} else {
			$data['special'] = '';
		}

		if (isset($this->request->post['telephone'])) {
			$data['telephone'] = $this->request->post['telephone'];
		} else {
			$data['telephone'] = '';
		}

		if (isset($this->request->post['fax'])) {
			$data['fax'] = $this->request->post['fax'];
		} else {
			$data['fax'] = '';
		}

		if (isset($this->request->post['company'])) {
			$data['company'] = $this->request->post['company'];
		} else {
			$data['company'] = '';
		}

		if (isset($this->request->post['address_1'])) {
			$data['address_1'] = $this->request->post['address_1'];
		} else {
			$data['address_1'] = '';
		}

		if (isset($this->request->post['address_2'])) {
			$data['address_2'] = $this->request->post['address_2'];
		} else {
			$data['address_2'] = '';
		}

		if (isset($this->request->post['postcode'])) {
			$data['postcode'] = $this->request->post['postcode'];
		} elseif (isset($this->session->data['shipping_address']['postcode'])) {
			$data['postcode'] = $this->session->data['shipping_address']['postcode'];
		} else {
			$data['postcode'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} else {
			$data['city'] = '';
		}

		if (isset($this->request->post['country_id'])) {
			$data['country_id'] = (int)$this->request->post['country_id'];
		} elseif (isset($this->session->data['shipping_address']['country_id'])) {
			$data['country_id'] = $this->session->data['shipping_address']['country_id'];
		} else {
			$data['country_id'] = $this->config->get('config_country_id');
		}

		if (isset($this->request->post['zone_id'])) {
			$data['zone_id'] = (int)$this->request->post['zone_id'];
		} elseif (isset($this->session->data['shipping_address']['zone_id'])) {
			$data['zone_id'] = $this->session->data['shipping_address']['zone_id'];
		} else {
			$data['zone_id'] = '';
		}

		$this->load->model('localisation/country');

		$data['countries'] = $this->model_localisation_country->getCountries();
        
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
		
		$this->response->setOutput($this->load->view('account/register2', $data));
	}
	

	private function validate() {
		if ((strlen(utf8_decode($this->request->post['telephone'])) < 10) || (strlen(utf8_decode($this->request->post['telephone'])) > 32) || preg_match('/[^\d]/is', $this->request->post['telephone'])) {
            $this->error['telephone'] = $this->language->get('error_telephone');
       }
	   
	   if ((utf8_strlen(trim($this->request->post['about_me'])) < 10) || (utf8_strlen(trim($this->request->post['about_me'])) > 500)) {
			$this->error['about_me'] = $this->language->get('error_about_me');
		}
		if (!isset($_POST['availability'])) {
			$this->error['availability'] = $this->language->get('error_availability');
		}

		if ((utf8_strlen(trim($this->request->post['city'])) < 2) || (utf8_strlen(trim($this->request->post['city'])) > 128)) {
			$this->error['city'] = $this->language->get('error_city');
		}
		if ((utf8_strlen(trim($this->request->post['ima'])) < 1) || (utf8_strlen(trim($this->request->post['ima'])) > 128)) {
			$this->error['ima'] = $this->language->get('error_ima');
		}
		// Check how many login attempts have been made.
		$login_info = $this->model_account_customer->getLoginAttempts($this->session->data['username']);

		if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
			$this->error['warning'] = $this->language->get('error_attempts');
		}

		// Check if customer has been approved.
		$customer_info = $this->model_account_customer->getCustomerByUsername($this->session->data['username']);

		if ($customer_info && !$customer_info['approved']) {
			$this->error['warning'] = $this->language->get('error_approved');
		}

		if (!$this->error) {
			
		
			if (!$this->customer->login($this->session->data['username'], $this->session->data['password'])) {
				$this->error['warning'] = $this->language->get('error_login');

				$this->model_account_customer->addLoginAttempt($this->session->data['username']);
			} else {
				$this->model_account_customer->deleteLoginAttempts($this->session->data['username']);
			}
		}
		
		return !$this->error;
	}

	public function customfield() {
		$json = array();

		$this->load->model('account/custom_field');

		// Customer Group
		if (isset($this->request->get['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($this->request->get['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $this->request->get['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$custom_fields = $this->model_account_custom_field->getCustomFields($customer_group_id);

		foreach ($custom_fields as $custom_field) {
			$json[] = array(
				'custom_field_id' => $custom_field['custom_field_id'],
				'required'        => $custom_field['required']
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
