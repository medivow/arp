<?php
class ControllerCommonDoctorLeft extends Controller {
	public function index() {
		
                $this->load->language('doctor/case');
		$data=array();
             $this->load->model('doctor/account');
	    $customer_info=$this->model_doctor_account->getCustomer($this->customer->getId());
		$this->load->model('tool/image');
		if (!empty($customer_info) && (is_file(DIR_IMAGE . $customer_info['image']))) {
			$image = $this->model_tool_image->resize($customer_info['image'], 80, 80);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 80, 80);
		}
		$data['image'] = $image;
		$data['text_case_studay']=$this->language->get('title_caseStudy');
		$data['text_daily_essay']=$this->language->get('title_dailyEssay');
		$data['welcome_message']='Welcome ' . $this->customer->getFirstName().' '.$this->customer->getLastName() ;
		$data['action_cse_studay'] = $this->url->link('doctor/case', '', true);
		$data['action_daily_essay'] = $this->url->link('doctor/dailyessay', '', true);
		$data['action_patient'] = $this->url->link('doctor/patients', '', true);
		$data['action_register'] = $this->url->link('account/register', '', true);
		$data['action_calendar'] = $this->url->link('doctor/doctor/calendar', '', true);
		$data['action_billing'] = $this->url->link('doctor/billing', '', true);
		$data['action_attachment'] = $this->url->link('doctor/attachment', '', true);
		return $this->load->view('common/doctor_left', $data);
	}
}
