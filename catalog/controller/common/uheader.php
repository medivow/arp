<?php
class ControllerCommonUheader extends Controller {
	public function index() {
		// Analytics
		$data['breadcrumbs'] = array();
		$this->load->model('doctor/account');
		$customer_info=$this->model_doctor_account->getCustomer($this->customer->getId());
		$this->load->model('tool/image');
		if (!empty($customer_info) && (is_file(DIR_IMAGE . $customer_info['image']))) {
			$image = $this->model_tool_image->resize($customer_info['image'], 80, 80);
		} else {
			$image = $this->model_tool_image->resize('no_image.png', 80, 80);
		}
		$data['image'] = $image;
        $data['action_password'] = $this->url->link('patient/password', '', true);
		$data['action_account'] = $this->url->link('patient/account', '', true);
		$data['welcome_message']='Welcome ' . $this->customer->getFirstName().' '.$this->customer->getLastName() ;
        $data['action_logout'] = $this->url->link('account/logout', '', true);
		$data['action_dashboard'] = $this->url->link('patient/patient', '', true);
		return $this->load->view('common/user_header', $data);
	}
}
