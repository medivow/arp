<?php echo $header; ?>

<?php echo $column_left; ?>

</nav>

<div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
      <div class="col-sm-12">
      	<h2 class="appointments">Select Services</h2>
      </div>
      
      <div class="col-md-6 col-md-offset-3 pad_Tb30">
        <div class="col-sm-6 patient_box mr_Tb30">
          <h2 class="appointments2 serv">Free</h2>
          <div class="service_fp text-center">
          <a class="btn btn-success" href="index.php?route=patient/appointment/free">Submit</a>
          </div>
        </div>
        <div class="col-sm-6 patient_box mr_Tb30">
          <h2 class="appointments2 serv">Paid</h2>
          <div class="service_fp text-center">
          <a class="btn btn-success" href="index.php?route=patient/appointment/paid">Submit</a>
          </div>
        </div>
        </div>
      </div>

    </div>
  </div>

<?php echo $footer; ?>