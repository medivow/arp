<?php echo $header; ?><?php echo $column_left; ?>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="appointments">calendar Appointments</h2>
        <div class="cont101">
          <link href='fullcalender/fullcalendar.css' rel='stylesheet' />
          <link href='fullcalender/fullcalendar.print.css' rel='stylesheet' media='print' />
          <script src='fullcalender/lib/moment.min.js'></script> 
          <script src='fullcalender/fullcalendar.min.js'></script> 
          <script>



              $(document).ready(function() {

              

                $('#calendar').fullCalendar({

                  height: 600,

                  header: {

                    left: 'prev,next today',

                    center: 'title',

                    right: 'month,agendaWeek,agendaDay',

                  },

                  editable: false,

                  eventLimit: true, // allow "more" link when too many events

                  events: {

                    url: '<?=$eventjson?>',

                    error: function() {

                      $('#script-warning').show();

                    }

                  },

                  loading: function(bool) {

                    $('#loading').toggle(bool);

                  }

                });

                

              });



            </script>
          <div id='script-warning'> </div>
          <div id='loading'>loading...</div>
          <div id='calendar'></div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper --> 

</script> 
<script type="text/javascript"><!--

$('.date').datetimepicker({

  pickTime: false

});

//--></script> 

<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(5)').addClass("active");
    })
</script>
<?php echo $footer; ?>