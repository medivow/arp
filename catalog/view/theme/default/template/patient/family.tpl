<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-5">
      <div class="clearfix">
       <div class="text-danger"><?php echo $success;  ?></div>
        <h2 class="appointments"><?php echo $text_list; ?> </h2>

        <div class="pull-right">

      <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>

      </div>

        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

          <div class="input-group">

            <input id="searchval" class="form-control" placeholder="Search for..." type="text">

            <span class="input-group-btn">

            <button class="btn btn-default" type="button">Go!</button>

            </span> 

            </div>

        </div>

      </div>

<div class="clearfix">

          <div class="x_panel">

            <div class="x_content">

              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">

                <div class="row">

                  <div class="col-sm-12">

                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">

                      <thead>

                <tr>

                 

                  <td class="text-left"><?php if ($sort == 'id.title') { ?>

                    <strong><?php echo $column_title; ?></strong>

                    <?php } else { ?>

                    <strong><?php echo $column_title; ?></strong>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($entry_description == 'entry_description') { ?>
                    <?php echo $column_sort_order; ?>
                    <?php } else { ?>
                   <strong> <?php echo $entry_description; ?></strong>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
                      <tbody class="odd">
                        <?php 
                           if(count($families)>0 && !empty($families)){
                          foreach ($families as $family) {
                        ?>
                        <tr class="odd1" role="row" id="family">
                          <td class="sorting_1"><?php echo $family['patient']; ?></td>
                          <td><?php echo $family['relation']; ?></td>
                          <td><!--<a href="<?php echo $family['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>-->
                          <a href="<?php echo $family['delete']; ?>" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                          
                          <a href="#" data-value="<?php echo $family['message_action']; ?>" id="sendMessage"  data-toggle="modal" data-target="#myModal<?php echo $family['id'];?>" title="<?php echo $send_message; ?>" class="btn bac-gry"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></a>
                          </td>
                          <div class="modal fade" id="myModal<?php echo $family['id'];?>" role="dialog">
                        <div class="modal-dialog"> <div class="modal-content">
                        <div class="modal-header"> 
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Send Message<!--<?php  echo $model_heading_title; ?>--></h4>
                        </div>
                        <div class="modal-body">
                        <span id="sendmailsucess" class="mrglef" style="display:none"> Message Send Successfully....</span>
                      <div class="tab-content">
                        <div id="account" class="tab-pane fade in active">
                        <form action="<?php echo $family['message_action']; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
        
        
        <div class="col-xs-12">
        	<div class="col-sm-4 search11">
        		<label>Entry Message</label>
        	</div>
        	<div class="col-sm-8 search11">
            <!--<textarea class="form-control11"></textarea>-->
             <textarea name="message" placeholder="<?php echo $entry_message; ?>" id="input-description" class="form-control11"><?php echo isset($description) ? $description : ''; ?></textarea>
            <div class="clearfix"></div>
            <button class="btn btn-Submit12" data-value="<?php echo $family['email']; ?>" id="submitMessage" type="submit">Submit</button>
            <input type="hidden" name="email" value="<?php echo $family['email']; ?>" />
                 <input type="hidden" name="patientName" value="<?php echo $family['patient']; ?>" />
            </div>
        </div>
              <!--  <div class="form-group marRl0">
                  <label for="message"><?php echo $entry_message; ?></label>
                  <textarea name="message" placeholder="<?php echo $entry_message; ?>" id="input-description" class="form-control summernote"><?php echo isset($description) ? $description : ''; ?></textarea>
                 <input type="hidden" name="email" value="<?php echo $family['email']; ?>" />
                 <input type="hidden" name="patientName" value="<?php echo $family['patient']; ?>" />
                </div>-->
                <div class="clearfix"></div>
               </form>
                        </div>
                      </div>
                        </div>
                        <!--<div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>--></div>
                        </div></div>
                        </tr>
                        <?php }} else { 
              ?>
<tr class="odd1" role="row" id="family">
              <td class="sorting_1" colspan="4">Result Not found</td>
</tr>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                  <div class="col-sm-12">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
	  <div class="col-sm-5">
          <h2 class="appointments">Notifications</h2>
          <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped">
              <thead>
                <tr>
                  <th>Sender Name</th>
                  <th>Relation</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody class="odd">
                        <?php 
                           if(count($notification)>0 && !empty($notification)){
                          foreach ($notification as $notf) {
                        ?>
                        <tr class="odd1" role="row" id="notification">
                          <td class="sorting_1"><?php echo $notf['patient']; ?></td>
                          <td><?php echo $notf['relation']; ?></td>
                          <td> 
                           <a href="#" id="acceptRel" data-value="<?php echo $notf['id']; ?>" data-toggle="tooltip" title="Accept" class="btn "><img src="catalog/view/theme/default/image/ok.png" /></a>
                          <a href="#" data-toggle="tooltip" data-value="<?php echo $notf['id']; ?>" title="Reject" class="btn"><img src="catalog/view/theme/default/image/reject.png" /></a>
                          </td>
                        </tr>
                        <?php }} else { 
              ?>
              <tr class="odd1" role="row" id="notification">
              <td class="sorting_1"  colspan="4">Result Not found</td>
              </tr>
            <?php
                         } ?>
                      </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
</div>


<script>
		$(document).on('click', '#submitMessage', function(e) {
					 e.preventDefault();
					 var data = $('form').serialize();
		var email= $(this).attr('data-value');
		 $.ajax({
			 data:data,
			 type:'POST',
      url: 'index.php?route=patient/family/sendmessage&email=' + email,
      dataType: 'html',
      success: function(htmlText) {
      //  $('.odd').html(htmlText);
	 /* var array = htmlText.split("+");
	  $("#family").empty();
	  $("#family").append(array[0]);
	  $("#notification").empty();
	  $("#notification").append(array[1]);*/
	  $('#sendmailsucess').css('display','block');
	 
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		
		});


</script>
<script>
		$(document).on('click', '#acceptRel', function(e) {
		 e.preventDefault();
		var id= $(this).attr('data-value');
		 $.ajax({
      url: 'index.php?route=patient/family/chnagerelstatus&id=' + id,
      dataType: 'html',
      success: function(htmlText) {
      //  $('.odd').html(htmlText);
	  var array = htmlText.split("+");
	  $("#family").empty();
	  $("#family").append(array[0]);
	  $("#notification").empty();
	  $("#notification").append(array[1]);
	 
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/family/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>