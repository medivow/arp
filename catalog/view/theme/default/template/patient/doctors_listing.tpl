<?php echo $header; ?><?php echo $column_left; ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
       <h2 class="appointments">Doctor List </h2>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <!--<div class="input-group">
          <input id="searchval"  class="form-control" placeholder="Search for..." type="text">
          <span id="searchdoc"></span> <span class="input-group-btn">
          <button id="searchdoctor" class="btn btn-default" type="button">Go!</button>
          </span> </div>-->
        </div>
      </div>
      
      
      <section class="">
	<div class="">
    <?php if(!empty($doctors)) { 
    $i=1;
            foreach($doctors as $doctor){
            ?>
            <?php if($i==1){?>
            
        <article class="col-xs-12">
             <?php } ?>
             <div class="col-md-6 col-lg-4 wbreaks">
            
           <div class="media patient_list min_h286">
            	<div class="media-left media_sec text-center"><img class="patient_img" src="<?php echo $doctor_image[$doctor['doc_id']] ?>"></div>
       				<div class="media-body">
            			<span>Name : <p><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p> </span>
              			<?php if($doctor['qualification']!=''){?>  <p class="BDS"><?php echo $doctor['qualification'] ?></p><?php } ?>
              			 <?php if($doctor['experience']!=''){ ?>  <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p><?php } ?>
                         <?php if(isset($doctor_speciality[$doctor['doc_id']])){?> <p class="BDS"><?php echo $doctor_speciality[$doctor['doc_id']] ?></p><?php } ?> 
                         <?php if(isset($doctor['locality'])){?> <p class=""> <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $doctor['locality'] ?></p><?php } ?> 
       				<div class="text-right mr_tp20">
                    	<a class="btn btn-primary" href="<?php echo $doctor_detail[$doctor['doc_id']];?>">View profile</a>
                	</div>
           </div>
           </div>
         </div>
         
         <?php if($i==3){
           $i=0;
           ?>
        </article>
        <?php } ?>
         <?php $i=$i+1;}} ?>
    </div>
</section>
   </div>

  </div>   
      
      
      
      
        <!--<section class="">
	<div class="">
    <?php if(!empty($doctors)) { 
    $i=1;
            foreach($doctors as $doctor){
            ?>
            <?php if($i==1){?>
            
        <article class="col-xs-12">
             <?php } ?>
            <div class="col-sm-3 dr_profile">
            <div class="doc-lst">
            	<p class="drk"> <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $doctor['locality'] ?></p>
                <div class="profileBox text-center"><img src="<?php echo $doctor_image[$doctor['doc_id']] ?>" /></div>
                <p class="BDS"><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
              <?php if($doctor['qualification']!=''){?>  <p class="BDS"><?php echo $doctor['qualification'] ?></p><?php } ?>
              <?php if($doctor['experience']!=''){ ?>  <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p><?php } ?>
                <p class="BDS"><?php echo $doctor_speciality[$doctor['doc_id']] ?></p>
                <div class="line"></div>
         		<!--<ul class="star mrgn0">
                	<a class="view_profile"  href="<?php echo $doctor_appointment[$doctor['doc_id']];?>"> Book Appointment</a>
                </ul>-->
               <!--<a class="view_profile"  href="<?php echo $doctor_detail[$doctor['doc_id']];?>">View profile</a>
               </div>
            </div>
            <?php }} ?>
        </article>
    </div>
</section>-->
    

  <!-- /.container-fluid --> 

</div>
<script>
$(document).ready(function(){
var inp = $("#searchval");
if (inp.val().length > 0) {
   $("odd1").html() 
}
});
</script> 
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/doctors/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script> 
<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(4)').addClass("active");
    })
</script> 
<!--  <script src="catalog/view/javascript/jquery.js"></script>-->
<!--<script src="catalog/view/javascript/bootstrap-datetimepicker.min.js"></script>-->
      <script>
	  $( document ).ready(function() {
	  $('#idTourDateDetails input').datepicker({
    });
	});
	  </script>

<?php echo $footer; ?>