<?php echo $header; ?><?php echo $column_left; ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
       <h2 class="appointments">Doctor Detail </h2>
        <!--<div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
          <input id="searchval"  class="form-control" placeholder="Search for..." type="text">
          <span id="searchdoc"></span> <span class="input-group-btn">
          <button id="searchdoctor" class="btn btn-default" type="button">Go!</button>
          </span> </div>
        </div>-->
      </div>
        <section class="pad_Tb20">
	<div class="">
        <article class="col-xs-12 dr_list wbreaks">
             <?php if(!empty($doctors)) { 
            foreach($doctors as $doctor){
           // print_r($doctor);
            ?>
           
            <div>
            <div class="new">
            <div class="media overcal">
              <div class="media-left">
              	<div class="profileBox1"><img src="<?php echo $doctor_image[$doctor['doc_id']] ?>" /></div>
              </div>
              <div class="media-body overcal">
              	<div class="col-md-6 cont">
                	<p><span>Name: </span><?php echo ucfirst($doctor['firstname']) . ' ' .ucfirst($doctor['lastname']); ?></p>
                  <?php if($doctor['email']!=''){ ?>  <p><span>email: </span><?php echo $doctor['email']; ?></p><?php } ?>
                    <?php if($doctor['qualification']!=''){ ?>  <p><span>Qualification: </span><?php echo $doctor['qualification']; ?></p><?php } ?>
                    <?php if($doctor['experience']!=''){ ?>  <p><span>Experience: </span><?php echo $doctor['experience']; ?></p><?php } ?>
                     <?php if($doctor['office_timing']!=''){ ?> <p><span>Office Timing: </span><?php echo $doctor['office_timing']; ?></p><?php } ?>
                     
                </div>
                <div class="col-md-6 cont">
                	<?php if($doctor['telephone']!=''){ ?> <p><span>Phone: </span><?php echo $doctor['telephone']; ?></p><?php } ?>
                   <?php if($doctor['fax']!=''){ ?> <p><span>Fax: </span><?php echo $doctor['fax']; ?></p><?php } ?>
                    <?php if($doctor['mr_timimg']!=''){ ?> <p><span>MR Meeting Timing: </span><?php echo $doctor['mr_timimg']; ?></p><?php } ?>
                     
                </div>
             <div class="col-sm-12 cont">
              <!--<div class="input-group date bookApnt" id="datetimepicker1">
                                <input type="text"   class="form-control clsDatePicker"> <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>
                                </div>-->
                                <form action="<?php echo $book_appointment[$doctor['doc_id']] ?>" method="post" id="form-booking" class="form-horizontal marTp_15">
                                <div class="input-group date bookApnt" id="datetimepicker1">
                      <input type="text" required="required" class="form-control" id="date" name="booking_date" placeholder="Appointment Date" >
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span> </span> </div>

                         <input type="hidden" name="DocID" value="<?php echo $Doctor_Id; ?>"   /> 
                     <input type="submit" value="Book Appointment" class="btn btn-primary btnapnt" />
                     </form>
             </div>
             <div class="col-sm-12 cont">
              <p><?php echo $doctor['about_doctor']; ?></p>
              </div>
              </div>
                
             <?php if($doctor['about_doctor']!=''){ ?>
              <?php } ?>
             </div>
			</div>
            
            
            
            <div class="dr_profile1">
            	
               </div>
            </div>
           <?php }} ?>
        </article>
    </div>
</section>
    </div>

  </div>

  <!-- /.container-fluid --> 

</div>
<script>
$(document).ready(function(){
var inp = $("#searchval");
if (inp.val().length > 0) {
   $("odd1").html() 
}
});
</script> 
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/doctors/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script> 
<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(4)').addClass("active");
    })
</script> 
<!--  <script src="catalog/view/javascript/jquery.js"></script>-->
<!--<script src="catalog/view/javascript/bootstrap-datetimepicker.min.js"></script>-->
     

<?php echo $footer; ?>

<style>
.new { margin: 0px; padding: 0px;} 
.new .media-left { text-align: left;  width: 0%;}
.new .profileBox1 img { width: 190px; }
.cont { margin-top: 36px;}
.new .media-body .cont p{ font-size: 16px; font-weight: 500;}
.new .media-body .cont p span { font-size: 16px; font-weight: 600; padding-right: 8px;}
.new .btn-primary { margin-top: 15px;}
.bookApnt{width:30%; float:left;}
.btnapnt{ margin-bottom: 4px;margin-left: 5px;margin-top: 0px !important;}


</style>