<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
        
        <div class="col-sm-12">
       <?php if (isset($success)) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
   <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
          <h2 class="appointments"><?php echo $heading_title; ?></h2>
          <?php if ($attachment_update) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $attachment_update; ?></div>
  <?php } ?>
  
  <div class="pad_Tb30">
  <div class="col-sm-6 col-sm-offset-3 pad_Tb15 fborder">
  <h3 class="text-center">Fill Attachment Form</h3>
  <hr />
  
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
                <div class="form-group marRl0">
                <label for="comment"><?php echo $entry_title; ?></label>
                 <input type="text" name="title" value="<?php echo isset($title) ? $title : ''; ?>" placeholder="<?php echo $entry_title; ?>" id="input-title" class="form-control" />
                   <?php if ($error_title) { ?>
              <div class="text-danger"><?php echo $error_title; ?></div>
              <?php } ?>
    </div>

                <div class="form-group marRl0">
                  <label for="comment"><?php echo $entry_attachment; ?></label>
                  <br /> <?php echo isset($attachment) ? $attachment : ''; ?>
                  <input type="file" id="attachment" name="attachment">
                  <?php if ($error_attachment) { ?>
              <div class="text-danger"><?php echo $error_attachment; ?></div>
              <?php } ?>
                </div>
                
                <div class="form-group marRl0">
                  <label for="comment"><?php echo $entry_share; ?></label>
                  </br>
                  <?php if($share == 1) { ?>
                  <input type="radio" name="share" value="1" checked id="share_yes"/><?php echo $entry_yes; ?></br>
                  <input type="radio" name="share" value="0" id="share_yes"/><?php echo $entry_no; ?></br>
                  <?php } elseif ($share == 0 ) { ?>
                  <input type="radio" name="share" value="1" id="share_yes"/><?php echo $entry_yes; ?></br>
                 <input type="radio" name="share" checked value="0" id="share_yes"/><?php echo $entry_no; ?></br>
                 <?php } else { ?>
                 <input type="radio" name="share" value="1" id="share_yes"/><?php echo $entry_yes; ?></br>
                 <input type="radio" name="share"   value="0"  id="share_no"/><?php echo $entry_no; ?></br>
                 <?php } ?>
                  <?php if ($error_share) { ?>
              <div class="text-danger"><?php echo $error_share; ?></div>
              <?php } ?>
                </div>

                
          <button class="btn btn-default" type="submit">Submit</button>
       
                    
                   
                  
                  
               </form>
  </div>
  </div>
        </div>
      </div>
      
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper -->

<?php echo $footer; ?>