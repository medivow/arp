<?php echo $header; ?>

<?php echo $column_left; ?>

</nav>
<script>
$(document).ready(function(){
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showLocation);
    } else { 
        $('#location').html('Geolocation is not supported by this browser.');
    }
});

function showLocation(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
	$("#cur_lat").val(latitude);
	$("#cur_lag").val(longitude);
  
}
</script>
<div id="page-wrapper">

    <div class="container-fluid">

      <div class="row">

       		<div class="col-xs-12"> 

          <h2 class="appointments"><?php echo $entry_consult_doctor; ?></h2>

          <div style="text-align:center;">

          <span class="success-msg"> <?php echo $success; ?> </span>  </div>

<div class="pad_Tb30">
  <div class="col-sm-6 col-sm-offset-3 pad_Tb15 fborder">
  <h3 class="text-center">Please Fill Form</h3>
  <hr />
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
          <input type="hidden" id="cur_lat" name="cur_lat" />
          <input type="hidden" name="cur_lag" id="cur_lag" />

              <!--  <div class="form-group marRl0 required">

                <label for="sel1"><?php echo $entry_doctors; ?></label>

                <section id="intro">

                <select id="doctor" name="doctor">

                   <?php if ($customerlist) { ?>

                    <?php foreach ($customerlist as $customer) { ?>

                    <option value="<?php echo $customer['customer_id'];?>" ><?php echo $customer['name'];?></option>  

                    <?php } } ?>

                </select>

                </section>

                 <?php if ($error_doctor) { ?>

              <div class="text-danger"><?php echo $error_doctor; ?></div>

              <?php } ?>

                </div>-->

              <!--  <div class="form-group marRl0 required">

                    <label for="dtp_input1" class=""><?php echo $entry_appointment_date; ?></label>

                    <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd / HH:ii p" data-link-field="dtp_input1">

                        <input class="form-control" size="16" type="text" name="appointment_date" value="" readonly>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>

                    </div>

                    <input type="hidden" id="dtp_input1" value="" /><br/>

               <?php if ($error_appointment_date) { ?>

              <div class="text-danger"><?php echo $error_appointment_date; ?></div>

              <?php } ?>

                </div>-->

                <!--<div class="form-group marRl0 required">

                    <label for="dtp_input1" class=""><?php echo $entry_appointment_date; ?></label>

                    <div class="input-group date form_datetime" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd / HH:ii p" data-link-field="dtp_input1">

                        <input class="form-control" size="16" type="text" name="appointment_date" value="" readonly>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>

                        <span class="input-group-addon"><span class="glyphicon glyphicon-th"></span></span>

                    </div>

                    <input type="hidden" id="dtp_input1" value="" /><br/>

               <?php if ($error_appointment_date) { ?>

              <div class="text-danger"><?php echo $error_appointment_date; ?></div>

              <?php } ?>

                </div>-->

                

                <!--<div class="form-group marRl0 required">

                  <label for="comment"><?php echo $entry_about_patient; ?></label>

                  <textarea class="form-control" rows="5" id="about_patient" name="about_patient"></textarea>

                <?php if ($error_about_patient) { ?>

              <div class="text-danger"><?php echo $error_about_patient; ?></div>

              <?php } ?>

                </div>-->

 <div class="clearfix"></div>
          <div class="form-group sel_pad">
            <label>Speciality</label>
            <input type="text" name="keyword" id="keyword" class="form-control _pad_lft35" placeholder="Specialities" autocomplete="off"/>
          </div>
          <div id='autocomplete_s' class="autoPopup"></div>

       			<button class="btn btn-default" type="submit"><?php echo $entry_submit; ?></button>

   

                  

               </form>
               </div>
               </div>

        </div>

      </div>

      

      <!-- /.row --> 

    </div>

    <!-- /.container-fluid --> 

    

  </div>
  <script>
$(document).ready(function(){
	$("#keyword").keyup(function(){
		$("#autocomplete_s").hide();
		if($("#autocomplete_s").val()==''){
			$("#autocomplete_s").hide();
			}
		$.ajax({
		type: "POST",
		url: "index.php?route=doctor/doctor/readSpacilities",
		data:'keyword='+$(this).val(),
		<!--beforeSend: function(){$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");},-->
		success: function(data){
			$("#autocomplete_s").show();
			$("#autocomplete_s").html(data);
		}
		});
	});
	return false;
});
//To select country name
function setindiv(val){
	
  $("#keyword").val(val);
  
  $("#autocomplete_s").hide();
 
 }
</script>

<?php echo $footer; ?>