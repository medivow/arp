<?php echo $header; ?><?php echo $column_left; ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
       <h2 class="appointments">Doctor List </h2>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
          <input id="searchval"  class="form-control" placeholder="Search for..." type="text">
          <span id="searchdoc"></span> <span class="input-group-btn">
          <button id="searchdoctor" class="btn btn-default" type="button">Go!</button>
          </span> </div>
        </div>
      </div>
        <section class="">
	<div class="">
        <article class="col-xs-12 dr_list">
             <?php if(!empty($doctors)) { 
            foreach($doctors as $doctor){
            ?>
            <div class="col-sm-3 dr_profile">
            <div class="doc-lst">
            	<p class="drk"> <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $doctor['locality'] ?></p>
                <div class="profileBox text-center"><img src="<?php echo $doctor_image[$doctor['doc_id']] ?>" /></div>
                <p class="BDS"><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
              <?php if($doctor['qualification']!=''){?>  <p class="BDS"><?php echo $doctor['qualification'] ?></p><?php } ?>
              <?php if($doctor['experience']!=''){ ?>  <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p><?php } ?>
                <p class="BDS"><?php echo $doctor_speciality[$doctor['doc_id']] ?></p>
                <div class="line"></div>
         		<ul class="star mrgn0">
                	<a class="view_profile"  href="<?php echo $doctor_appointment[$doctor['doc_id']];?>"> Book Appointment</a>
                </ul>
               <a class="view_profile" data-toggle="modal" href="#" data-target="#myModal<?php echo $doctor['doc_id']; ?>">View profile</a>
               </div>
            </div>
            <div id="myModal<?php echo $doctor['doc_id']; ?>" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Doctor Detail</h4>
      </div>
      <div class="modal-body">
       <div class="profileBox text-center"><img src="<?php echo $doctor_image[$doctor['doc_id']] ?>" /></div>
       <div>
       <p class="BDS"><?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
        <?php if($doctor_speciality[$doctor['doc_id']]!=''){?>  <p class="BDS"><?php echo $doctor_speciality[$doctor['doc_id']] ?></p><?php } ?>
              <?php if($doctor['experience']!=''){ ?>  <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p><?php } ?>
       </div>
       
    <div class="col-md-12">
                    <div class="row">
                        <label >Appointment Date:</label>
                        <div class="form-group">
                            <div class="input-group" id="idTourDateDetails">
                                <input type="text"   class="form-control clsDatePicker"> <span class="input-group-addon"><i id="calIconTourDateDetails" class="glyphicon glyphicon-th"></i></span>

                            </div>
                        </div>
                    </div>
                    </div>
      </div>
          <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     
    </div>

  </div>
</div>
            <?php }} ?>
        </article>
    </div>
</section>


    </div>

  </div>

  <!-- /.container-fluid --> 

  

</div>
<script>
$(document).ready(function(){
var inp = $("#searchval");
if (inp.val().length > 0) {
   $("odd1").html() 
}
});
</script> 
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/doctors/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script> 
<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(4)').addClass("active");
    })
</script> 
<!--  <script src="catalog/view/javascript/jquery.js"></script>-->
<!--<script src="catalog/view/javascript/bootstrap-datetimepicker.min.js"></script>-->
      <script>
	  $( document ).ready(function() {
	  $('#idTourDateDetails input').datepicker({
    });
	});
	  </script>

<?php echo $footer; ?>