<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <h2 class="appointments"><?php echo $heading_title; ?>  </h2>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input id="searchval" class="form-control" placeholder="Search for..." type="text">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>
        </div>
      </div>
        <div class="col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">

                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Title</th>
                          <th>Doctor Name </th>
                          <th>Medicine</th>
                          <th>Precautions </th>
                          <th>Diet </th>
                          
                        </tr>
                      </thead>
                      <tbody class="odd">
                        <?php 
                           if(!empty($prescription)){
                           $i=1;
                           foreach($prescription as $prlist) {
                        ?>
                        <tr class="odd1" role="row">
                          <td class="sorting_1"><?php echo $prlist['title']; ?></td>
                          <td><?php echo $prlist['firstname']. ' '.$prlist['lastname']; ?></td>
                          <td><?php echo $prlist['medicin']; ?></td>
                          <td><?php echo $prlist['precausion']; ?></td>
                          <td><?php echo $prlist['diet']; ?></td>
                          <td><a href="<?php echo $action; ?><?php echo "&patient_id=". $prlist['id']; ?>" data-toggle="modal" data-target="#myModal<?php echo $i;?>">Detail</a></td>

                        </tr>

                        
                        <?php $i++;}} else { 
              ?>
              <td class="sorting_1" colspan="4">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
<?php 
                           if(!empty($prescription)){
                           $i=1;
                           foreach($prescription as $prlist1) {
                        ?>
                        

                        <div class="modal fade" id="myModal<?php echo $i;?>" role="dialog">
                        <div class="modal-dialog"> <div class="modal-content">
                        <div class="modal-header modal_back"> 
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center"><?php echo $model_heading_title; ?></h4>
                        </div>
                        <div class="modal-body">
                      <div class="tab-content">
                        <div id="account" class="tab-pane fade in active table-responsive">
                        <table class="table table-bordered">
                      	<thead>
                        	<tr class="active">
                            	<th><?php echo $model_pres_title; ?></th>
                                <th><?php echo $model_pres_docname; ?></th>
                                <th><?php echo $model_pres_medicine; ?></th>
                                <th><?php echo $model_pres_precautions; ?></th>
                                <th><?php echo $model_pres_diet; ?></th>
                                <th><?php echo $model_pres_description; ?></th>
                                <th><?php echo $model_pres_adddate; ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td><?php echo $prlist1['title']?></td>
                                <td> <?php echo $prlist1['firstname']. ' '.$prlist1['lastname']; ?></td>
                                <td><?php echo $prlist1['medicin']; ?></td>
                                <td><?php echo $prlist1['precausion']; ?></td>
                                <td><?php echo $prlist1['diet']; ?></td>
                                <td><?php echo $prlist1['description']; ?></td>
                                <td><?php echo $prlist1['added_date']; ?></td>
                            </tr>
                       	</tbody>
                      </table>
                        </div>
                        
                      </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div></div>
                        </div></div>
                        <?php $i++;}}  ?>
                  </div>

                </div>



                  <div class="col-sm-12">

                    <!--<div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">

                      <ul class="pagination">

                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>

                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>

                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>

                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>

                      </ul>

                    </div>-->

                  </div>


              </div>

            </div>

          </div>

        </div>



    </div>

  </div>

  <!-- /.container-fluid --> 

  

</div>
  
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=patient/prescription/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>