<?php echo $header; ?>
<!------------Dr. Ramin Rak start------------>
  <?php if(!empty($doctors)) { 
		 
            foreach($doctors as $doctor){
                  $firstname=$doctor['firstname'];
				  $lastname=$doctor['lastname'];
				  $username=$doctor['username'];
				  $email=$doctor['email'];
				  $telephone=$doctor['telephone'];
				  $fax=$doctor['fax'];
				  $speciality=$doctor['speciality'];
				  $office_timimg=$doctor['office_timimg'];
				  $country=$doctor['country'];
				  $state=$doctor['state'];
				  $city=$doctor['city'];
				  $locality=$doctor['locality'];
				  $doc_address=$doctor['doc_address'];
				  $office_timing=$doctor['office_timing'];
				  $experience=$doctor['experience'];
				  $qualification=$doctor['qualification'];
				  $ranking=$doctor['ranking'];
				  $contact=$doctor['contact'];
                  $about_me=$doctor['about_me'];
			}
            
             $longitude=array();     $latitude=array();
             $locality1=array();
           foreach($doctor_clinic as $clinic_name ){
                $longitude[] =  $clinic_name['longitude'];
                $latitude[]  =  $clinic_name['latitude'];
                $locality1[]  =  $clinic_name['locality'];
            }
	 $clinic_count = count($longitude);
     $js_address = '';
    for($i=0; $i <$clinic_count; $i++){
        $js_address.= "['".$locality1[$i]."', ".$latitude[$i].",".$longitude[$i].", $i],";
       }
    
   }
      ?>
     <script src="https://maps.google.com/maps/api/js?key=AIzaSyBWyNfay5nhhW1sO4pvVsr_8oVZIQZ0uGk&sensor=false"
          type="text/javascript"></script>
<section class="_style_bg banner1" style="background-image:url(catalog/view/theme/default/image/details_banner_bg.png);">
	<div class="RaminBox text-center">
    	<img src="<?php if(!empty($doctor_image[$doctor['customer_id']])){ echo $doctor_image[$doctor['customer_id']];}else{ echo $doctor_image[$doctor['customer_id']];} ?>">
    	<h2><?php if(!empty($firstname)){ echo $firstname;} ?> <?php if(!empty($lastname)){ echo $lastname; } ?></h2>
        <p><?php  if(!empty($speciality)){ echo $speciality;} ?></p>
    </div>
</section>
<section>
	<div class="container">
    	<div class="infoBox">
        <div class="col-sm-2"><p><?php  if(!empty($firstname)){ echo $firstname;} ?> <?php if(!empty($lastname)){ echo $lastname; }?> </p></div>
        <div class="col-sm-3"><i class="fa fa-map-marker" aria-hidden="true"></i><span><?php if(!empty($locality)){echo $locality;} ?> <?php if(!empty($city)){ echo $city;} ?> </span></div>
        <div class="col-sm-2"><p class="ask">Ask</p></div>
        <div class="col-sm-2">
        	<ul class="icon">
            	<li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star" aria-hidden="true"></i></li>
                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>
            </ul>
        </div>
        <div class="col-sm-3"><p><?php if(!empty($speciality)){echo $speciality;} ?></p></div>
        </div>
    </div>
</section>
<!------------Dr. Ramin Rak end------------>
<!-----------Dr. detail start---------->
<section class="martp_sticky dr_details _pad_tp20">
	<div class="container _pad0">
        <div class="col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>A LITTLE ABOUT ME</h4>
                </div>
                <div class="panel-body txt_hgt _pad_tp0"><?php if(!empty($about_me)){ echo $about_me;}?></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’M LOCATED AT</h4>
                   <!-- <img src="catalog/view/theme/default/image/map.png" class="img-responsive"/>-->
                    
       <!---Multipal Marker Map---->
       <div id="map" style="width: 620px; height: 312px;"></div>
     <script type="text/javascript">
        var locations = [  <?php echo substr_replace($js_address,"",-1);?> ];
        var map='';
        var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(  <?php echo $latitude[0];?> , <?php echo $longitude[0];?>),
        mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        
        var infowindow = new google.maps.InfoWindow();
        
        var marker, i;
        
        for (i = 0; i < locations.length; i++) {  
        marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
        });
        
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
        infowindow.setContent(locations[i][0]);
        infowindow.open(map, marker);
        }
        })(marker, i));
        }
        </script>
                    
                    <!----Map Close ------>
                    
                    
                    <p class="location"> <i class="fa fa-map-marker" aria-hidden="true"></i> <span> <?php if(!empty($locality)){echo $locality;}?> <?php if(!empty($city)){echo $city;} ?>  </span> </p>
                    <p class="location"> <i class="fa fa-phone" aria-hidden="true"></i><span>Call : </span><?php if(!empty($telephone)){echo $telephone;}?> </p>
                    <p class="location"> <i class="fa fa-globe" aria-hidden="true"></i><span>Visit Website </span> </p>
                    <p class="location"> <i class="fa fa-info-circle" aria-hidden="true"></i><span>More Information </span> </p>
                </div>
            </div>
           	<div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’ M AFFILIATED WITH</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>Hospitals</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                         labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Organisations</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I’ VE PUBLISHED</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>How to cure cancer</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et 
                         dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel">
                <div class="panel-heading _pad_btm0">
                    <h4>Claim The Listing</h4>
                    <form>
  						<div class="form-group">
      						<input type="listing" class="form-control"  placeholder="Claim the listing">
                            <textarea class="form-control" placeholder="Enter message"></textarea>
                            <button type="button" class="btn btn-primary btn-lg active">Submit claim</button>
  						</div>
                    </form>
                    <div class="clearfix"></div>
                </div>
             </div>
        </div>
        <div class="col-sm-5">
        	<div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>MY ANSWERS AND INSIGHTS HAVE:</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">Saved 256 lives</li>
                    <li class="list-group-item">Helped 955.714 People</li>
                    <li class="list-group-item">Received 714 doctor agress</li>
                    <li class="list-group-item">Received 5614 thanks</li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>I SPECIALIZE IN</h4>
                </div>
                <ul class="list-group _pad_btm20">
                    <li class="list-group-item"><?php if(!empty($speciality)){ echo $speciality;} ?></li>
                    <li class="list-group-item"><?php if(!empty($speciality)){ echo $speciality;}?></li>
                    <li class="list-group-item"><?php if(!empty($speciality)){ echo $speciality;}?></li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>WON THE FOLLOWING AWARD</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><strong>Top National Doctor</strong></li>
                    <li class="list-group-item"><strong>Best Pediatrician</strong></li>
                    <li class="list-group-item"><strong>Best Cardiology</strong></li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>BEEN IN PRACTICE FOR</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item"><?php if(!empty($experience)){ echo $experience;} ?></li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>SOME KIND WORD FROM OTHERS</h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                    	<h5>Dr. Vivek Jain</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut 
                         labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Dr. Nilesh Jain</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                    <li class="list-group-item">
                    	<h5>Dr. Aman Gupta</h5>
                         <p class="txt_hgt">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                         ut labore et dolore magna aliqua.</p> 
                    </li>
                </ul>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading _pad_btm0">
                    <h4>ADDITIONAL LINKS</h4>
                </div>
                <ul class="list-group link_hover">
                    <li class="list-group-item"><a href="#">My website</a></li>
                    <li class="list-group-item"><a href="#">My hospital website</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-----------Dr. detail end---------->
 
<?php echo $footer; ?>
