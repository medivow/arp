<?php echo $header; ?>
<?php echo $slider; ?>
<!-----------listing Start---------------->
<section class="container">
	<div class="row">
    	<aside class="col-sm-3 side_filter">
        	<div class="panel-group" role="tablist">
  				<div class="panel panel-default">
        			<div class="panel-heading accordion" role="tab">
          				<h4 class="panel-title">
            			<a role="button" data-toggle="collapse" data-parent=".accordion" href=".collapseOne" aria-expanded="true" aria-controls="collapseOne">
              			<i class="fa fa-caret-down" aria-hidden="true"></i> Appointment	</a></h4>
        			</div>
   					<div class="panel-collapse collapse in collapseOne" role="tabpanel">
      					<div class="panel-body">
                        	<input type="checkbox"> Check me out
  					    </div>
    				</div>
  				</div>
            </div>
            <div id="mouse">
            <h4 class="text-center">Availability</h4>
            <ul class="days_filter">
            	<li class="active">ANY</li>
                <li><input type="checkbox" class="checkbox" value="mon" name="availability[]" />M</li>
                <li><input type="checkbox" class="checkbox" value="tue" name="availability[]" />T</li>
                <li><input type="checkbox" class="checkbox" value="wed" name="availability[]" />W</li>
                <li><input type="checkbox" class="checkbox" value="thr" name="availability[]" />T</li>
                <li><input type="checkbox" class="checkbox" value="fri" name="availability[]" />F</li>
                <li><input type="checkbox" class="checkbox" value="sat" name="availability[]" />S</li>
                <li><input type="checkbox" class="checkbox" value="sun" name="availability[]" />S</li>
            </ul>
            <div class="clearfix"></div>
            <div class="time"  id="time-range">
            	<p><span class="slider-time">09:00 AM</span> - <span class="slider-time2">18:00 PM</span></p>
    			<div class="sliders_step1">
        			<div class="slider_range" id="slider-range"></div>
    			</div>
			</div>
            </div>
            <div class="medvow_Consult">
            	<i class="fa fa-comments-o" aria-hidden="true"></i>
                <div class="clearfix"></div>
                <img src="catalog/view/theme/default/image/logo5.png" />
                <p>Unanswered Health Questions, Answered by Doctors</p>
                <a class="btn btn_blue tp_mar _mar_btm10" href="#">ASK FREE QUESTION</a>
			</div>
            <div class="clearfix"></div>
            <div class="latest_news">
            <h4 class="text-center">LATEST NEWS</h4>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            <p class="month">May 26, 2011</p>
            <p>Phasellus tempor tristique augue non malesuada. Donec pellentesque varius <a href="#">More</a></p>
            </div>
        </aside>
        
        <article class="col-sm-9 dr_list">
        	<h3>Featured Listing</h3>
        	<div id="all_dr_list">
             <?php if(!empty($doctors)) {
              $i=1;
              //print_r($doctors); die;
            foreach($doctors as $doctor){
            
           ?>
           <?php
            if($i==1){?>
        <div class="col-sm-12">
             <?php } ?>
            
            <div class="col-md-6 col-lg-6 wbreaks">
            
           <div class="media patient_list">
            	<div class="media-left media_sec text-center"><img class="patient_img" src="<?php echo $doctor_image[$doctor['doc_id']] ?>">
                
                </div>
       				<div class="media-body">
                    	<div class="cont_h">
            				<p>Name:<?php echo $doctor['firstname'] ?> <?php echo $doctor['lastname'] ?></p>
              				<?php if($doctor['qualification']!=''){?>  <p><?php echo $doctor['qualification'] ?></p><?php } ?>
              				<?php if($doctor['experience']!=''){ ?>  <p class="BDS"><?php echo $doctor['experience'] ?> years experience</p><?php } ?>
                        	<p><?php echo $doctor_speciality[$doctor['doc_id']] ?></p>
                        	<p> <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $doctor['locality'] ?></p>
                        <ul class="star">
                			<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                    		<li><i class="fa fa-star-o" aria-hidden="true"></i></li>
                		</ul>
                        </div> 
                        

           </div>
           <div class="clearfix"></div>
            <a class="btn btn-book_now" href="<?php echo $book_now_href; ?>/&doc_id=<?=$doctor['doc_id']?>">Book Now</a>
            <a class="btn btn-primary2" href="<?php echo $detail_href; ?>&doctor_id=<?=$doctor['doc_id']?>">View profile</a>
           </div>
           
           </div>
            
           
             <?php if($i==2){
           $i=0;
           ?>
        </div>
        <?php } ?>
         <?php $i=$i+1; ?>
            
            <?php }} else { echo "No Result Found"; } ?>
            </div>
        </article>
    </div>
</section>

<!-----------listing end---------------->


<script>
$(".checkbox").change(function() {
	var stime=$(".slider-time").text();
	var etime=$(".slider-time2").text();
	//alert(day);
	var data = { 'availability[]' : []};
 $("input:checked").each(function() {
 
 data['availability[]'].push($(this).val()); 
 });
 
 var cb = [];
 $.each($('.checkbox:checked'), function() {
 cb.push($(this).val()); 
 });
    $.ajax({
        type: 'POST',
        url: 'index.php?route=search/search/searchtest',
        data: {availability:cb,stime:stime,etime:etime},
        success:function(html){
			//alert(html);
          $('#all_dr_list').html(html);
        }
    });
});
</script>
<?php echo $footer; ?>
