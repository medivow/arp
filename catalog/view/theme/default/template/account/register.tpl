<?php echo $header; ?>
      <section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
    
		<h3 class="text-center _pad_tb20">Create an Account</h3>
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 _mar_btm35" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        	<h4 class="text-center _mar_btm35">It's nice to meet you</h4>
            <?php if ($error_firstname) { $myclass="has-error"; } else {$myclass="";}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname"/>
                <i class="fa fa-user"></i>
                <?php if ($error_firstname) { ?>
              <div class="text-danger"><?php echo $error_firstname; ?></div>
              <?php } ?>
            </div>
            <?php if ($error_lastname) { $myclass="has-error"; } else {$myclass="";}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control"name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>"/>
                <i class="fa fa-user"></i>
                <?php if ($error_lastname) { ?>
              <div class="text-danger"><?php echo $error_lastname; ?></div>
              <?php } ?>
            </div>
            
            
             <?php if ($error_username) { $myclass="has-error"; } else {$myclass="";} if(isset($username)){$username=$username;}else{$username='';}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username"/>
                <i class="fa fa-user"></i>
                <?php if ($error_username) { ?>
              <div class="text-danger"><?php echo $error_username; ?></div>
              <?php } ?>
            </div>
           <?php if ($error_email) { $myclass="has-error"; } else {$myclass="";}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <?php if ($error_email) { ?>
              <div class="text-danger"><?php echo $error_email; ?></div>
              <?php } ?>
            </div>
            <?php if ($error_mobile) { $myclass="has-error"; } else {$myclass="";}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control" name="mobile" value="<?php echo $mobile; ?>" placeholder="Mobile" id="input-email"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <?php if ($error_mobile) { ?>
              <div class="text-danger"><?php echo $error_mobile; ?></div>
              <?php } ?>
            </div>
            <?php if ($error_password) { $myclass="has-error"; } else {$myclass="";}?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="password" class="form-control" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
                <?php if ($error_password) { ?>
              <div class="text-danger"><?php echo $error_password; ?></div>
              <?php } ?>
            </div>
            <div class="input_styl">
            	<input type="password" class="form-control" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
           <!-- <h4 class="text-center _pad_tp20">Are you a doctor?</h4>
            <div class="checkbox text-center _mar_btm35">
  				<label><input data-toggle="toggle" name="doctor" type="checkbox"></label>
			</div>-->
            <input type="hidden" name="customer_group_id_reg" value="<?php echo $customer_group_id_reg; ?>" />
            <div class="text-center _mar_btm30"><input type="checkbox" name="agree" value="1" class="chck_mar"/>I have read and agree to the Terms of use
             <?php if ($error_warning) { ?>
              <div class="text-danger"><?php echo $error_warning; ?></div>
              <?php } ?>
            </div>
         
            <div class="text-center _mar_btm30">Already Registered?<a href="<?php echo $login; ?>"> Login Now!</a></div>
            
            <div class="clearfix _mar_btm30"><button type="submit" class="btn btn_blue col-sm-6  col-sm-offset-3 col-xs-12">Sign Up</button></div>
        </form>
    </div>
</section>
      
    <?php //echo $column_right; ?></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript"><!--
// Sort the custom fields
	

$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {						
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
						
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>

<?php echo $footer; ?>
<script src="catalog/view/javascript/bootstrap-toggle.min.js"></script>
<!--<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(5)').addClass("active");
    })
</script>-->