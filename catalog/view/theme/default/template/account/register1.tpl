<?php echo $header; ?>
<link href="catalog/view/theme/default/stylesheet/pricing.css" rel="stylesheet">
<!-----------signup form start---------->
<section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
		<h3 class="text-center">Create an Account step 2</h3>
               
    
    <div class="row _mar_tp30">
        <div class="container" id="pricing-table">
	        
            <div class="clearfix"></div>
            <div class="clearfix mT40 pricing">
                <table class="table-responsive" width="100%" cellspacing="0" cellpadding="5">
                        <tbody><tr>
                            <th scope="row"><h2 class="text-center"><strong>Select Package!</strong></h2></th>
                            <td class="free-directory pckg-free" width="20%" align="center">
                                <h4 class="pckg-font18 clr-wht">Medivow Profile</h4>
                                <div class="price mrgbtn3"><span class="monthly-rate clr-wht">Free </span></div>
                                <br /><br />
                                <div class="change-plan"><a href="<?php echo $action_free; ?>" class="upgrade-plan">Get Started</a></div>
                            </td>
                            <td id="premiumDirectory" class="premium-directory pckg-premium" width="20%" align="center">
                                <h4 class="pckg-font18 clr-wht">Medivow Premium Profile</h4>
                                <div class="price"><span class="monthly-rate clr-wht">Rs 500 </span><span class="clr-wht">/Month</span></div>
                                <br />
                                <div class="change-plan"><a href="<?php //echo $action_premium; ?>" class="upgrade-plan"  data-toggle="modal" data-target="#myModal1">Get Started</a></div>
                            </td>
									
                              <div class="modal fade" id="myModal1" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><h3 class="panel-title display-td text-center">Payment Process</h3></h4>
                                    </div>
                                    <div class="modal-body">
                                    <div class="clearfix">&nbsp;</div>
                                    <p>
                                    <div class="col-xs-12 col-md-12">
        
     	  <div id="infopanel">
				<div id="befourpayment" class="text-center">
                </div>
          <div id="donepayment">
          <p > <span style="color:green;" class="text-center">Your Payment Successful.</span>
          <br> 
          <a href="<?php echo $action_premium;?>" class="subscribe btn btn-success btn-lg btn-block" style="color:#fff;">Proceed Now.</a>
          </p></div> </div>
          
     	  <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box" id="bxpanel">
                <div class="panel-heading display-table">
                    <div class="row display-tr">
                        <div class="display-td">                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardNumber">CARD NUMBER</label>
                                    <div class="input-group">
                                        <input class="form-control" name="cardNumber" placeholder="Valid Card Number" autocomplete="cc-number" required="" autofocus="" type="tel" value="4444333322221111">
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                    <input class="form-control" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required="" type="tel" value="12/32">
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cardCVC">CV CODE</label>
                                    <input class="form-control" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required="" type="password" value="324">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="couponCode">PAYMENT FOR PACKAGE </label>
                                    <input class="form-control" name="package" type="text" value="Medivow Premium Profile: Rs 500/Month" disabled="disabled">
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                         <a href="#" class="btn btn-success btn-lg btn-block" style="color:#fff;" id="pay500">Pay Secure</a>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
            <!-- CREDIT CARD FORM ENDS HERE -->
		<script type="text/javascript">
        //THIS  CODE FOR 500 PAYMENT  BUTTON ONE 
        $(document).ready(function(){
        $("#donepayment").hide();
        i = 6;
        function onTimer() {
        document.getElementById('befourpayment').innerHTML = "<img src='image/catalog/loader.gif'><br><p>Processing...</p>";
        i--;
        if(i<0)
        {	
        $("#donepayment").show('slow');
        $("#befourpayment").hide('slow');
        }
        else { 	
        setTimeout(onTimer, 1000);
        }
        }
        $("#infopanel").hide();
        $("#pay500").click(function(){
        $("#bxpanel").hide();
        $("#infopanel").show('slow');
        onTimer();
        });
        });
        </script>
            
        </div>
                                    </p>
                                    <div class="clearfix">&nbsp;</div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>       
                                    
                            <td id="masterTrainer" class="master-trainer pckg-business" width="20%" align="center">
                                <h4 class="pckg-font18 clr-wht" >Medivow Calendar</h4>
                                <div class="price"><span class="monthly-rate clr-wht">Rs 1,900</span><span class="clr-wht">/Month</span></div>
                                <br /><br />
                                <div class="change-plan"><a href="<?php echo 'javascript:void(0)';//$action_business; ?>" class="upgrade-plan" data-toggle="modal" data-target="#myModal">Get Started</a></div>
                            </td>
                            
                                    <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><h3 class="panel-title display-td text-center">Payment Process</h3></h4>
                                    </div>
                                    <div class="modal-body">
                                    <div class="clearfix">&nbsp;</div>
                                    <p>
                                    <div class="col-xs-12 col-md-12">
        
                <div id="infopanel_1">
                <div id="befourpayment1" class="text-center">
                </div>
                <div id="donepayment1">
                <p > <span style="color:green;" class="text-center">Your Payment Successful.</span>
                <br> 
                <a href="<?php echo $action_business;?>" class="subscribe btn btn-success btn-lg btn-block" style="color:#fff;">Proceed Now.</a>
                </p></div> </div>

        
            <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box" id="bxpanel_1">
                <div class="panel-heading display-table">
                    <div class="row display-tr">
                        <div class="display-td">                            
                            <img class="img-responsive pull-right" src="http://i76.imgup.net/accepted_c22e0.png">
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="cardNumber">CARD NUMBER</label>
                                    <div class="input-group">
                                        <input class="form-control" name="cardNumber" placeholder="Valid Card Number" autocomplete="cc-number" required="" autofocus="" type="tel" value="4444333322221111">
                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label for="cardExpiry"><span class="hidden-xs">EXPIRATION</span><span class="visible-xs-inline">EXP</span> DATE</label>
                                    <input class="form-control" name="cardExpiry" placeholder="MM / YY" autocomplete="cc-exp" required="" type="tel" value="12/29">
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cardCVC">CV CODE</label>
                                    <input class="form-control" name="cardCVC" placeholder="CVC" autocomplete="cc-csc" required="" type="password" value="365">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="couponCode">PAYMENT FOR PACKAGE </label>
                                    <input class="form-control" name="package" type="text" value="Medivow Calendar: Rs 1,900/Month" disabled="disabled">
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                               <a href="#" class="subscribe btn btn-success btn-lg btn-block" style="color:#fff;" id="pay1900">Pay Secure</a>
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
            <!-- CREDIT CARD FORM ENDS HERE -->
            
            <script type="text/javascript">
        //THIS  CODE FOR 1900 PAYMENT  BUTTON ONE 
        $(document).ready(function(){
        $("#donepayment1").hide();
        i = 6;
        function onTimer1() {
        document.getElementById('befourpayment1').innerHTML = "<img src='image/catalog/loader.gif'><br><p>Processing...</p>";
        i--;
        if(i<0)
        {	
        $("#donepayment1").show('slow');
        $("#befourpayment1").hide('slow');
        }
        else { 	
        setTimeout(onTimer1, 1000);
        }
        }
        $("#infopanel_1").hide();
        $("#pay1900").click(function(){
        $("#bxpanel_1").hide();
        $("#infopanel_1").show('slow');
        onTimer1();
        });
        });
        </script>
          
            
            
            
        </div>
                                    </p>
                                    <div class="clearfix">&nbsp;</div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                            
                        </tr>
                        <tr>
                             <th scope="row">Doctor Listing</th>
                            <td class="free-directory" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            <td class="premium-directory" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            
                        </tr>
                        <tr>
                            <th scope="row">Reviews</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            
                        </tr>
                        <tr>
                            <th scope="row">Patient Profiles</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                           
                        </tr>
                        <tr>
                            <th scope="row">Notifications and Alerts for Patients</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"></td>
                            <td class="master-trainer" align="center"></td>
                            
                        </tr>
                        <tr>
                            <th scope="row">Premium Listing</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            
                        </tr>
                        <tr>
                            <th scope="row">Custom Profile Editing</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            
                        </tr>
                       
                        <tr>
                            <th scope="row">Appointment Calendar</th>
                            <td class="free-directory" align="center"></td>
                            <td class="premium-directory" align="center"></td>
                            <td class="master-trainer" align="center"><span class="glyphicon glyphicon-ok"></span></td>
                            
                        </tr>
                      
                </tbody></table>
            </div>
        </div>
    </div>

    </div>
</section>
<!-----------singup form end---------->

      
    <?php //echo $column_right; ?></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


<?php echo $footer; ?>
<script src="catalog/view/javascript/bootstrap-toggle.min.js"></script>
<!--<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(5)').addClass("active");
    })
</script>-->