<?php echo $header; ?>
<section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
		<h3 class="text-center _pad_tb20"> Login as a reception</h3>
         <?php if ($success) { ?>
  <div class="alert alert-success col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3"><i class="fa fa-check-circle"></i> <?php echo $success; ?></div>
  <?php } ?>
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 _mar_btm35" action="index.php?route=account/login/replogin&type=rep" method="post" enctype="multipart/form-data">
        	<h4 class="text-center _mar_btm35">Great, you're back!</h4>
            
            
            <div class="input_styl">
            	<input type="text" class="form-control" name="username" required placeholder="User Name"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
            <div class="input_styl">
            	<input type="password" name="password" class="form-control" required placeholder="Password"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
            
           <div class="text-center _mar_btm30 w25 flt"> <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></div>
           <div class="text-center _mar_btm30 w25 flt mrl45" > <a href="<?php echo $register_doctor; ?>"><?php echo $text_register_doctor; ?></a></div>
            <div class="text-center _mar_btm30 w25 flr"> <a href="<?php echo $forgotten; ?>">Forgot password?</a></div>
            <div class="clearfix _mar_btm30"><button class="btn btn_blue col-sm-6  col-sm-offset-3 col-xs-12" type="submit">Login</button></div>
            
        </form>
    </div>
</section>
  
<?php echo $footer; ?>
<!--<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(6)').addClass("active");
    })
</script>-->