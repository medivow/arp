<?php echo $header; ?>
<link rel="stylesheet" href="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.css">
<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
<!-----------signup form start---------->
<section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
		<h3 class="text-center _pad_tb20">Create an Account step 3</h3>
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 _mar_btm35" action="<?php echo $action; ?>" method="post">
        	<h4 class="text-center _mar_btm35">It’s nice to meet you</h4>
            <div class="input_styl">
            	<textarea rows="6" cols="4" class="form-control _mar_btm10" name="about_me" placeholder=" A little about me"><?php echo $about_me; ?></textarea>
                <?php if ($error_about_me) { ?>
              <div class="text-danger mr_btm20"><?php echo $error_about_me; ?></div>
              <?php } ?>
            </div>
            <?php if ($error_ima) { $myclass="has-error"; }?>
            <div class="input_styl <?php echo $myclass; ?>">
            	<input type="text" class="form-control" value="<?php echo $ima; ?>" name="ima" placeholder="Registration Number"/>
                <i class="fa fa-user"></i>
                 <?php if ($error_ima) { ?>
              <div class="text-danger mr_btm20"><?php echo $error_ima; ?></div>
              <?php } ?>
            </div>
            <div class="input_styl">
             <select name="speciality" class="form-control " required>
             <option value="">--- Select Speciality ---</option>
             <?php 
             if(!empty($specialities)){
             foreach($specialities as $speciality) { 
             if($special==$speciality['id']){ $selected='selected'; } else { $selected='';}
             ?>
             <option  value="<?php echo $speciality['id'] ?>" <?php echo $selected; ?>><?php echo ucfirst($speciality['title']) ?></option>
             <?php }} ?>
             </select>
            	
            </div>
            <?php if ($error_telephone) { $myclass="has-error"; }?>
            <div class="input_styl _mar_tp10">
            	<input type="text" class="form-control" value="<?php echo $telephone; ?>" name="telephone" placeholder="Telephone"/>
                <i class="fa fa-user"></i>
                <?php if ($error_telephone) { ?>
              <div class="text-danger mr_btm20"><?php echo $error_telephone; ?></div>
              <?php } ?>
            </div>
            <div class="input_styl">
            	<input type="text" class="form-control" value="<?php echo $fax; ?>" name="fax" placeholder="Fax"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
            <div style="position:relative;z-index:1;">
<div class="table-responsive">
<table class="table" id="tbl">
	  <tbody>
      <tr><td colspan="5"><strong>Add Clinic Address</strong></td></tr>
     	<tr>
        	<td><input placeholder="Locality" class="form-control input-small " type="text" name="locality"  id="autocomplete" onFocus="geolocate()">
            <input type="hidden" name="latitude" value="" id="latitude">
            <input type="hidden" name="longitude" value="" id="longitude">
            </td>
            <td><input id="timepicker1" type="text" class="form-control input-small timepicker1" placeholder="start time" name="stime">
            
            </td>
            <td><input type="text" class="form-control input-small timepicker1" placeholder="end time" name="etime"></td>
            <!--<td><a id='addClinic' title="" class="addRow"><i class="fa fa-plus-square" aria-hidden="true"></i></a> </td>-->
         </tr>
         <tr>
         <label for="comment"><?php echo $text_Availability;  ?></label>
              <input type="checkbox" name="availability[]" value="sun" /> Sunday
              <input type="checkbox" name="availability[]" value="mon" /> Monday
              <input type="checkbox" name="availability[]" value="tue" /> Tuesday
              <input type="checkbox" name="availability[]" value="wed" /> Wednesday
              <input type="checkbox" name="availability[]" value="thr" /> Thrusday
              <input type="checkbox" name="availability[]" value="fri" /> Friday
              <input type="checkbox" name="availability[]" value="sat" /> Saturday
              <?php if ($error_availability) { ?>
              <div class="text-danger">
                <?php  echo $error_availability; ?>
              </div>
              <?php } ?>
         </tr>
      </tbody>
</table>
<table class="table" >
	  <tbody>
      <tr><td colspan="5"><strong>MR Meeting Time</strong></td></tr>
     	<tr>
        	<td><input id="mrtime" type="text" class="form-control input-small mrtime" placeholder="Start TIme" name="mrtimefrom">
            
            </td>
            <td><input id="mrtimeto" type="text" class="form-control input-small mrtime" placeholder="End Time" name="mrtimefromto"></td>
           
         </tr>
      </tbody>
</table>
</div>
</div>
            
             <div class="input_styl">
            	<input type="text" class="form-control" name="city" placeholder="City" id="locality" />
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
             <div class="input_styl">
            	<input type="text" class="form-control" name="state" placeholder="State" id="administrative_area_level_1" />
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
             <div class="input_styl">
             <select name="country" class="form-control">
             <?php //foreach($countries as $country) {  ?>
             <option value="<?php echo  "India"; ?>"><?php echo "India"; ?></option>
             <?php //} ?>
             </select>
            	
            </div>
           
            <div class="text-center"></div>
            <div class="text-center _mar_btm30"></div>
              <div class="clearfix _mar_btm30"><button class="btn btn_blue col-sm-6  col-sm-offset-3 col-xs-12" type="submit">Next</button></div>
            
        </form>
    </div>
</section>
<!-----------singup form end---------->
    <?php //echo $column_right; ?></div>
    
    <script type="text/javascript">
            $('.timepicker1').timepicker({
					'minTime': '7:00am',
					'maxTime': '11:30pm'
				});
			$('.mrtime').timepicker({
					'minTime': '7:00am',
					'maxTime': '11:30pm'
				});
        </script>
        <script>
function deleteRow(row)
{
    var i=row.parentNode.parentNode.rowIndex;
    document.getElementById('tbl').deleteRow(i);
}


    var counter = 1;
	$(document).on('click', '#addClinic', function(e) {
   <!-- $('a.addRow').click(function(event){-->
        e.preventDefault();
        var newRow = $('<tr><td><input placeholder="Locatity" class="form-control input-small" type="text" name="locality[' +
            counter + ']"/></td><td><input type="text" class="form-control input-small timepicker1" placeholder="start time" name="stime['+counter+']"/></td><td><input type="text" class="form-control input-small timepicker1" placeholder="end time" name="etime['+counter+']"/></td><td><a href="javascript:void(0);" onclick="deleteRow(this);"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td></tr>');
            counter++;
        $('#tbl').append(newRow);
	 
    });
	
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript"><!--
// Sort the custom fields
	

$('#account .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#account .form-group').length) {
		$('#account .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#account .form-group').length) {
		$('#account .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#account .form-group').length) {
		$('#account .form-group:first').before(this);
	}
});

$('#address .form-group[data-sort]').detach().each(function() {
	if ($(this).attr('data-sort') >= 0 && $(this).attr('data-sort') <= $('#address .form-group').length) {
		$('#address .form-group').eq($(this).attr('data-sort')).before(this);
	}

	if ($(this).attr('data-sort') > $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') == $('#address .form-group').length) {
		$('#address .form-group:last').after(this);
	}

	if ($(this).attr('data-sort') < -$('#address .form-group').length) {
		$('#address .form-group:first').before(this);
	}
});

$('input[name=\'customer_group_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/customfield&customer_group_id=' + this.value,
		dataType: 'json',
		success: function(json) {
			$('.custom-field').hide();
			$('.custom-field').removeClass('required');

			for (i = 0; i < json.length; i++) {
				custom_field = json[i];

				$('#custom-field' + custom_field['custom_field_id']).show();

				if (custom_field['required']) {
					$('#custom-field' + custom_field['custom_field_id']).addClass('required');
				}
			}


		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script>
<script type="text/javascript"><!--
$('button[id^=\'button-custom-field\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$(node).parent().find('.text-danger').remove();

					if (json['error']) {						
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
						
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script type="text/javascript"><!--
$('select[name=\'country_id\']').on('change', function() {
	$.ajax({
		url: 'index.php?route=account/account/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
		},
		complete: function() {
			$('.fa-spin').remove();
		},
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('input[name=\'postcode\']').parent().parent().addClass('required');
			} else {
				$('input[name=\'postcode\']').parent().parent().removeClass('required');
			}

			html = '<option value=""><?php echo $text_select; ?></option>';

			if (json['zone'] && json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
					html += '<option value="' + json['zone'][i]['zone_id'] + '"';

					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
						html += ' selected="selected"';
					}

					html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}

			$('select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
       
<?php echo $footer; ?>
<script src="catalog/view/javascript/bootstrap-toggle.min.js"></script>

<!----GEO SCRIPT GOOGLE API---->
<script type="text/javascript">
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
      
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
		
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        //for (var component in componentForm) {
	      document.getElementById('locality').value = '';
		  document.getElementById('locality').disabled = false;
		  document.getElementById('administrative_area_level_1').value = '';
		  document.getElementById('administrative_area_level_1').disabled = false;
       // }
		
      // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
         
		  var addressType = place.address_components[i].types[0];
           if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV8IR0pybsjbjBMPs3kVUsvdlamzdcQw4&libraries=places&callback=initAutocomplete"
        async defer></script>

<!--- GEO SCRIPT GOOGLE API---->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript">
$( document ).ready(function() {
	$( "#autocomplete" ).blur(function() {
	
	var geocoder = new google.maps.Geocoder();
	var address = $('#autocomplete').val(); 
geocoder.geocode( { 'address': address}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
	var longitude = results[0].geometry.location.lng();
   $('#longitude').val(longitude);
   $('#latitude').val(latitude);
   
    } 
}); 
});
});
</script>

