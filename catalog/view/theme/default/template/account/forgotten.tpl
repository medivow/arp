<?php echo $header; ?>
<section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="container">
		<h3 class="text-center _pad_tb20"> <?php echo $heading_title; ?> </h3>
         
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  
  
  
  
  
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 _mar_btm35" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        	
            
            
            <div class="input_styl">
            	<input type="text" class="form-control" name="username" placeholder="User Name"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
            
            
           
            <div class="clearfix _mar_btm30"><button class="btn btn_blue col-sm-6  col-sm-offset-3 col-xs-12" type="submit"><?php echo $button_continue?></button></div>
            
        </form>
    </div>
</section>

<?php echo $footer; ?>
<!--<script>
    $( document ).ready(function() {
        $('.nav.navbar-nav li:nth-child(6)').addClass("active");
    })
</script>-->