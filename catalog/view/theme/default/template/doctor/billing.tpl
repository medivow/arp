<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">

  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        <h2 class="appointments">Billing list </h2>

        <div class="pull-right">

      <a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>

      </div>

        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">

          <div class="input-group">
            <input id="searchval" class="form-control" placeholder="Search for..." type="text">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>

        </div>

      </div>



        <div class="col-xs-12">

          <div class="x_panel">

            <div class="x_content">

              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">

                <div class="row">

                  <div class="col-sm-12">

                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Title</th>
                          <th>Amount</th>
                          <th>Discount</th>
                          <th>Added Date</th>
                        </tr>
                      </thead>
                      <tbody class="odd">
                        <?php 
                           if(count($billing)>0){
                           foreach($billing as $plist) {
                        ?>
                        <tr class="odd1" role="row">
                          <td class="sorting_1"><?php echo $plist['title']; ?></td>
                          <td><?php echo $plist['amount']; ?></td>
                          <td><?php echo $plist['discount']; ?></td>
                          <td><?php echo $plist['added_date']; ?></td>
                          <td> <button type="button" data-value="<?php echo $plist['bid']; ?>" class="btn btn-info btn-lg" id="detail" value="<?php echo $plist['bid']; ?>" data-toggle="modal" data-target="#myModal" >Detail</button></td>
                        </tr>
                        <?php }} else { 
              ?>
              <td class="sorting_1" colspan="4">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>

                  </div>

             <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Billing Detail</h4>
        </div>
        <div class="modal-body">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
                </div>

<div class="col-sm-12"><a href="<?php echo $action; ?>" class="btn btn-default pull-right mrgtp6 mrgrt14">Add Invoice</a></div>

                  <div class="col-sm-12">

                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">

                       <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>

                    </div>

                  </div>


              </div>

            </div>

          </div>

        </div>



    </div>

  </div>

  <!-- /.container-fluid --> 

  

</div>
  <!-- /.container-fluid --> 
  
</div>
<script>

	
		$(document).on('click', '#detail', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=doctor/billing/detail&billing_id=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.modal-body').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=doctor/billing/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>