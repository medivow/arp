<?php echo $header; ?>
<?php echo $column_left; ?>



<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <!--<div class="page-title">
        <h3>Prescription Details </h3>
      </div>-->
      <!--<div class="row">-->
        <div class="col-md-12 col-sm-12 col-xs-12">
         <h2 class="appointments">Prescription Details</h2>
        
         <?php if (isset($prescription_sucess)&& $prescription_sucess!='') { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $prescription_sucess; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
        <div class="pull-right">

      	<a href="<?php echo $action; ?>" data-toggle="tooltip" title="<?php echo $action; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
		<!--<a href="<?php echo $action; ?>" data-toggle="tooltip" title="<?php echo $action; ?>" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>-->
        
      </div>
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Title</th>
                          <th>Precausion</th>
                          <th>Diet</th>
                          <th>Patient</th>
                          <th>Added date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                           if(count($pres_results)>0){
                           $i=1;
                           foreach($pres_results as $prescription) {
                        ?>
                        <tr class="odd" role="row">
                          <td class="sorting_1"><?php echo $prescription['title']; ?></td>
                          <td><?php echo $prescription['precausion']; ?></td>
                          <td><?php echo $prescription['diet']; ?></td>
                          <td><?php echo $prescription['pat_name']; ?></td>
                         <td><?php echo $prescription['added_date']; ?></td>
                         <td><a href="" id="PresDetail" data-toggle="modal" data-target="#PresDetailModel" data-value="<?php echo $prescription['id']; ?>">Detail</a></td>
                          <!--<td><a href="<?php echo $action; ?><?php echo "&doctor_id=". $plist['customer_id']; ?><?php echo "&patient_id=". $plist['pid']; ?>">View Prescription</a></td>-->
                        </tr>
                       
                        
                        <?php $i=$i+1;}} else { 
              ?>
              <td class="sorting_1" colspan="6">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                  <div id="modalDiv"><div class="modal fade" id="PresDetailModel" role="dialog">
                        <div class="modal-dialog"> <div class="modal-content">
                        <div class="modal-header modal_back"> 
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-center">Prescription Detail</h4>
                        
                        <!--<span class="success-msg alert alert-success fr wd100" id="mailSucess"></span>-->
                        
                        </div>
                        <div class="modal-body">
                        <div class="success-msg alert alert-success fr wd100 txlc" id="mailSucess" style="display:none;"><i class="fa fa-check-circle"></i> <?php echo $prescription_sucess; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
                      <div class="tab-content">
                        <div id="MedicineDetailArea" class="tab-pane fade in active">
                        
                        </div>
                        
                      </div>
                        </div>
                        <div class="modal-footer">
                        <a href="javascript:window.print();" id="print" data-value='' class="btn btn-default">Print</a>
						<a href="" id="SendMail" data-value='' class="btn btn-default">Email</a>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div></div>
                        </div></div></div>
                  
                <!--<div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                <!--</div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    <!--</div>-->
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
  
</div>
<script>

		$(document).on('click', '#PresDetail', function(e) {
 		 e.preventDefault();
		 $("#mailSucess").css("display", "none");
		var id=$(this).attr('data-value');
		$("#SendMail").attr("data-value", id);
		 $.ajax({
		type: "POST",
		url: "index.php?route=doctor/patients/get_prescription_detail",
		data:'id='+id,
		<!--beforeSend: function(){$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");},-->
		success: function(data){
			$("#MedicineDetailArea").html();
			$("#MedicineDetailArea").html(data);
		}
		});
		});


</script>
<script>
		$(document).on('click', '#SendMail', function(e) {
		 e.preventDefault();
		var id=$(this).attr('data-value');
		 $.ajax({
		type: "POST",
		url: "index.php?route=doctor/patients/send_mail",
		data:'id='+id,
		<!--beforeSend: function(){$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");},-->
		success: function(data){
			//$("#MedicineDetailArea").html();
			$("#mailSucess").css("display", "block");
			$("#mailSucess").html('Email Send on Patient Mail..');
		}
		});
		});


</script>
<?php echo $footer; ?>