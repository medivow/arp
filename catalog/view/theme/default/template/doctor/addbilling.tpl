<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
    <div class="container-fluid">
      <div class="row">
        
        <div class="col-sm-12">
       
  <!--<div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php //echo $success; ?>
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>-->
  
   
    <!--<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php //echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>-->
    
          <h2 class="appointments"><?php //echo $heading_title; ?>Billing</h2>
          
          <section class="pad_Tb30">
          <div class="col-sm-8 col-sm-offset-2 fborder padall30">
          <h3 class="text-center">Fill Billing Form</h3>
          <hr />
          <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
          <div class="form-group marRl0 required">
                <label for="sel1"><?php //echo $entry_doctors; ?>Patients</label>
                <section id="intro">
                <select id="doctor" name="patient" class="form-control">
                   <?php 
                           if(count($presults)>0){
                           foreach($presults as $plist) {
                        ?>
                    <option value="<?php echo $plist['customer_id'];?>" ><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></option>  
                    <?php } } ?>
                </select>
                </section>
                 <?php if ($error_patient) { ?>
              <div class="text-danger"><?php echo $error_patient; ?></div>
              <?php } ?>
                </div>
           
                <div class="form-group marRl0">
                <label for="comment"><?php //echo $entry_title; ?>Title</label>
                 <input type="text" name="title" value="<?php //echo isset($title) ? $title : ''; ?>" placeholder="<?php //echo $entry_title; ?>" id="input-title" class="form-control" />
                  
               <?php if ($error_title) { ?>
              <div class="text-danger"><?php echo $error_title; ?></div>
              <?php } ?>
              
    </div>
    <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Description</label>
                  <textarea name="description" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Description" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 
               <?php if ($error_description) { ?>
              <div class="text-danger"><?php echo $error_description; ?></div>
              <?php } ?>
              
                </div>
                  <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Amount</label>
                  <textarea name="amount" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Amount" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 <?php if ($error_amount) { ?>
              <div class="text-danger"><?php echo $error_amount; ?></div>
              <?php } ?>
                </div>
                
                   <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Discount</label>
                  <textarea name="discount" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Discount" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 
              <?php if ($error_discount) { ?>
              <div class="text-danger"><?php echo $error_discount; ?></div>
              <?php } ?>
              
                </div>

                

                
          <button class="btn btn-default" type="submit">Submit</button>
      
               </form>
               </div>
               </section>
        </div>
      </div>
      
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper -->

<?php echo $footer; ?>