<?php echo $header; ?>
<?php echo $column_left; ?>
<link href="catalog/view/theme/default/stylesheet/mulitimage.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery.form.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('#upload').on('click',function(){
		$('#upload_form').ajaxForm({
			target:'#preview',
			beforeSubmit:function(e){
				$('.progress').show();
			},
			success:function(e){
				$('.progress').hide();
				location.reload();
			},
			error:function(e){
			}
		}).submit();
	});
});
</script> 
<div id="page-wrapper">
  <div  class="right_col" role="main">
    <div class="">
      <div class="page-title">
        <h3 class="mr_btm20">Patients Details </h3>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                <div class="col-md-1 text-center">
                    	
                        <?php if( $patient_image[$patients[0]['cus_id']]!=''){?>
                        <img src="<?php echo $patient_image[$patients[0]['cus_id']] ?>" />
                        <?php } else{ ?>
                        <img src="catalog/view/theme/default/image/user.png" />
                        <?php } ?>
                    </div>
                  	<div class="col-md-8">
                  	<div class="col-sm-10 col-sm-offset-1">
                  	
                    
                    
                      <?php 
                           if(count($patients)>0){
                            //echo $patients['firstname']; die;
                           foreach($patients as $plist) {
                           //echo $plist['firstname']; die;
                        ?>
                    <div class="col-sm-6">
                   
                    	<div class="row">
                        	    <div class="col-sm-4"><label>Name:</label></div>
                                <div class="col-sm-8"><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></div>
                           </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Age:</label></div>
                                <div class="col-sm-8"><?php echo $plist['age']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Email:</label></div>
                                <div class="col-sm-8"><?php echo $plist['email']; ?></div>
                             </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Telephone:</label></div>
                                <div class="col-sm-8"><?php echo $plist['telephone']; ?></div>
                            </div>
                           <div class="row">
                            	<div class="col-sm-4"><label>Fax:</label></div>
                                <div class="col-sm-8"><?php echo $plist['fax']; ?></div>
                            </div>
                            </div>
                            <div class="col-sm-6">
                           <div class="row">
                            	<div class="col-sm-4"><label>Locality:</label></div>
                                <div class="col-sm-8"><?php echo $plist['locality']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>City:</label></div>
                                <div class="col-sm-8"><?php echo $plist['city']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>State:</label></div>
                                <div class="col-sm-8"><?php echo $plist['state']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>Country:</label></div>
                                <div class="col-sm-8"><?php echo $plist['country']; ?></div>
                             </div>
                             <div class="row">
                            	<div class="col-sm-4"><label>Date Added:</label></div>
                                <div class="col-sm-8"><?php echo $plist['date_added']; ?></div>
                             </div>
                             
  
                        </div>
                        
                        <?php } } ?>


                      
                    </div>
                  </div>
                  
                  <div class="col-md-3">
                  <form action="<?php echo $book_appointment[$patients[0]['cus_id']] ?>" method="post" id="form-booking" class="form-horizontal mr_Tb5">
                                <div class="input-group date bookApnt col-md-10 mr_Tb5" id="datetimepicker1">
                      <input type="text" required="required" class="form-control" id="date" name="booking_date" placeholder="Appointment Date" >
                      <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span> </span> </div>

                         <input type="hidden" name="DocID" value="<?php echo $Doctor_Id; ?>"   /> 
                     <input type="submit" value="Book Appointment" class="btn btn-primary btnapnt col-md-10 mr_Tb5" />
                     </form>
                  	<div><a href="<?php echo $action; ?>" class="btn btn-default col-md-10 mr_Tb5">View Prescription</a></div>
                    </div>
                      
                </div>
                  
                  
                </div>
     
<div class="page-title"><br>
        <h3 class="mr_btm20">Patient important File(s) </h3>
      </div>	 
				
	<div style="margin-top:50px;">
	<div class="upload_div">
	
	<?php
	if(isset($_GET['cus_id']))
	{
		$patient=$_GET['cus_id'];
	}
	else{
		$patient=$_GET['patient_id'];
	}
	?>
	
    <form method="post" name="upload_form" id="upload_form" enctype="multipart/form-data" action="index.php?route=doctor/patients/addfile&patient_id=<?php echo $patient;?>">
    	<input type="hidden" name="form_submit" value="1"/>
            <label>Choose Image</label>
            <input type="file" name="images[]" id="upload-images" multiple >
            
        <div class="progress none">         
         <img src="catalog/view/theme/default/image/uploading.gif"/>
        </div>
		<input type="button" name="upload" id="upload" value="Upload">
    </form>
    </div>
	
    <div class="gallery" id="preview">
	<?php	
	
	if(!empty($datafiles)){ $count=0;
		foreach($datafiles as $datafiles){ $count++?>
			<ul class="reorder_ul reorder-photos-list">
            	<li id="image_li_<?php echo $count; ?>" class="ui-sortable-handle">
                	<a href="image/attachment/<?php echo $datafiles['attachment']; ?>" target="blank" style="float:none;" class="image_link"><img src="image/attachment/<?php echo $datafiles['attachment']; ?>" alt=""></a>
               	</li>
          	</ul>
	<?php }
	}
	
	
	?>
	
	</div>
</div>
				
				
				
				
                <!--<div class="row">
                  <div class="col-sm-7">
                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">
                      <ul class="pagination">
                        <li id="datatable_previous" class="paginate_button previous disabled"><a tabindex="0" data-dt-idx="0" aria-controls="datatable" href="#">Previous</a></li>
                        <li class="paginate_button active"><a tabindex="0" data-dt-idx="1" aria-controls="datatable" href="#">1</a></li>
                        <li class="paginate_button "><a tabindex="0" data-dt-idx="2" aria-controls="datatable" href="#">2</a></li>
                        <li id="datatable_next" class="paginate_button next"><a tabindex="0" data-dt-idx="7" aria-controls="datatable" href="#">Next</a></li>
                      </ul>
                    </div>
                  </div>
                </div>-->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.container-fluid --> 
 
</div>

<?php echo $footer; ?>