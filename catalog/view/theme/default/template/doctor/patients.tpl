<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
 <h2 class="appointments">Patients list</h2>
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
          <div class="input-group">
            <input id="searchval" class="form-control" placeholder="Search for..." type="text">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span> </div>
        </div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <div class="dataTables_wrapper form-inline dt-bootstrap no-footer" id="datatable_wrapper">
                <div class="row">
                  <div class="col-sm-12">
                    <table aria-describedby="datatable_info" role="grid" id="datatable" class="table table-striped table-bordered dataTable no-footer">
                      <thead>
                        <tr role="row">
                          <th>Name</th>
                          <th>Email</th>
                          <th>Phone</th>
                          <th>Age</th>
                          <th>Start date</th>
                          <th>Fee</th>
						  <th>Action</th>
                        </tr>
                      </thead>
                      <tbody class="odd">
                        <?php 
                           if(count($patients)>0){
                           foreach($patients as $plist) {
                          
                        ?>
                        <tr class="odd1" role="row">
                          <td class="sorting_1"><?php echo $plist['firstname']. ' '.$plist['lastname']; ?></td>
                          <td><?php echo $plist['email']; ?></td>
                          <td><?php echo $plist['telephone']; ?></td>
                          <td><?php echo $plist['age']; ?></td>
                          <td><?php echo $plist['date_added']; ?></td>
                          <td><?php echo $plist['fee']; ?></td>
                          <td><a href="<?php echo $action; ?><?php echo "&patient_id=". $plist['doc_id']; ?>">Detail</a> || 
                          <a href="<?php echo $prescription_action; ?><?php echo "&patient_id=". $plist['doc_id']; ?>">Prescription</a>
                          </td>
                        </tr>
                        <?php }} else { 
              ?>
              <td class="sorting_1" colspan="7">Result Not found</td>
            <?php
                         } ?>
                      </tbody>
                    </table>
                  </div>
                  
                  

                   <div class="col-sm-12">

                    <div id="datatable_paginate" class="dataTables_paginate paging_simple_numbers">

                       <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>

                    </div>

                  </div>

                
                
                
                
                
                
                
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  <!-- /.container-fluid --> 
  </div>
</div>
<script>
    $( document ).ready(function() {
        $('.side-nav li:nth-child(2)').addClass("active");
    })
</script>
<script>
		$(document).on('keyup', '#searchval', function(e) {
 		 e.preventDefault();
		//alert("test");
		//alert($(this).attr('data-value'));
		//alert("val");
		 $.ajax({
      url: 'index.php?route=doctor/patients/search&term=' + this.value,
      dataType: 'html',
      success: function(htmlText) {
        $('.odd').html(htmlText);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
      }
    });
		});


</script>
<?php echo $footer; ?>