<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper" class="min-page-ht">
<?php $service_row = 0; ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="appointments"><?php //echo $heading_title; ?>Prescription</h2>
          <div style="text-align:center;">

          <span class="success-msg"> <?php echo $success; ?> </span>  </div>
		  
		  <div class="col-md-6 col-md-offset-3 pad_Tb20">
		  
          <form action="<?php echo $action; ?>" method="POST" enctype="multipart/form-data" id="form-information" class="form-horizontal marTp_15">
                <div class="form-group marRl0">
                <label for="comment"><?php //echo $entry_title; ?>Title</label>
                 <input type="text" name="title" value="<?php //echo isset($title) ? $title : ''; ?>" placeholder="<?php //echo $entry_title; ?>" id="input-title" class="form-control" />
                   <?php if ($error_title) { ?>
              <div class="text-danger"><?php echo $error_title; ?></div>
              <?php } ?>
    </div>
    <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Description</label>
                  <textarea name="description" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Description" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 
              <?php if ($error_description) { ?>
              <div class="text-danger"><?php echo $error_description; ?></div>
              <?php } ?>
              
                </div>
    <div class="form-group marRl0">
    <div class="wd50">
                  <label for="comment"><?php //echo $entry_description; ?>Medicin</label>
                 <input type="text" name="medicin[]" id="medicinTitle" value="" placeholder="Medicin" class="form-control medicin txtwdt" />
         </div>
         <div class="wd50">        
              <label for="comment"><?php //echo $entry_description; ?>Dose</label>
                 <input type="text" name="dose[]" id="dose" value="" placeholder="Medicin" class="form-control dose txtwdt" />
                </div>
                <div class="wd50" id="divresTiming">        
              <label for="comment"><?php //echo $entry_description; ?>Timing</label>
                <select name="timing[]" id="res_timing" class="form-control medicin txtwdt">
                <option value="">Select The Timing</option>
                <option value="Before Breakfast">Before Breakfast</option>
                <option value="After Breakfast">After Breakfast</option>
                <option value="Before Lunch">Before Lunch</option>
                <option value="After Lunch">After Lunch</option>
                <option value="Before Dinner">Before Dinner</option>
                <option value="After Dinner">After Dinner</option>
                </select>
                </div>
                <a href="#" id="AddPres" data-toggle="tooltip" title="" class="btn btn-primary mrgl10 mrgt25" data-original-title="Add New"><i class="fa fa-plus"></i></a>
                </div>
                
                <div class="form-group marRl0" id="aditionalPrescription">
   
                </div>
                
                <div id='autocomplete_s' class="autoSugest"></div>
                <div class="form-group marRl0" id="medicineArea"></div>
                <div class="form-group marRl0" id="MedTest">
                <div class="MdTest" id="MdTest">
                <label for="comment">Medical Test</label>
                
                 <input type="text" name="medial_test[]"  value="" placeholder="Medical Test" id="input-title" class="form-control wd100" />
                 </div>
                  <a href="#" id="AddMedTest" data-toggle="tooltip" title="" class="btn btn-primary mrgl10 mrgt25" data-original-title="Add New"><i class="fa fa-plus"></i></a>
                
              <div class="text-danger"><?php //echo $error_title; ?></div>
              
    </div>
     
                <div class="form-group marRl0" id="aditionalMedicalTest">
   
                </div>
                  <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Precausion</label>
                  <textarea name="precausion" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Precausion" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 
             <?php if ($error_precausion) { ?>
              <div class="text-danger"><?php echo $error_precausion; ?></div>
              <?php } ?>
              
                </div>
                
                   <div class="form-group marRl0">
                  <label for="comment"><?php //echo $entry_description; ?>Diet</label>
                  <textarea name="diet" placeholder="<?php //echo $entry_description; ?>" id="input-description" placeholder="Diet" class="form-control summernote"><?php //echo isset($description) ? $description : ''; ?></textarea>
                 
              <?php if ($error_diet) { ?>
              <div class="text-danger"><?php echo $error_diet; ?></div>
              <?php } ?>
              
                </div>

                

                <input type="hidden" name="patient_id" value="<?php echo $patient_id ?>" />
          <button class="btn btn-default btn_center" type="submit">Submit</button>
       
                    
                   
                  
                  
               </form>
			</div>
        </div>
      </div>
      
      <!-- /.row --> 
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  <!-- /#page-wrapper -->
  
  <script>
$(document).ready(function(){
	var service_row = <?php echo $service_row; ?>;
	$("#AddPres").click(function(){
	var apndtext='';
	apndtext+='<div class="form-group marRl0 " id="prescription-row' + service_row + '">';
	apndtext+='<div class="wd50" >';
	apndtext += '<label  for="comment">Medicin</label>';
	apndtext += '<input type="text" placeholder="Medicin" id="medicinTitle' + service_row + '" class="form-control medicin txtwdt" name="medicin[]">';
	apndtext += '</div>';
	apndtext+='<div class="wd50" >';
	apndtext += '<label  for="comment">Dose</label>';
	apndtext += '<input type="text" placeholder="Dose" id="dose" class="form-control dose txtwdt" name="dose[]">';
	apndtext += '</div>';
	apndtext += ' <div class="wd50" id="divresTiming"> ';
	apndtext+=$("#divresTiming").clone().html();
	apndtext += '</div>';
	apndtext += '<button type="button" onclick="$(\'#prescription-row' + service_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger  mrgl10 mrgt25"><i class="fa fa-minus-circle"></i></button>';
	apndtext += '</div>';
	$("#aditionalPrescription").append(apndtext);
	//$("#service-row"+service_row+ " select").addClass("servicearea");
	service_row++;

		
		});
	
});
//To select country name
/*function setindiv(val){
 $("#keyword").val(val);
  $("#autocomplete_s").hide();
 }*/
</script>
<script>
$(document).ready(function(){
	var service_row = <?php echo $service_row; ?>;
	$("#AddMedTest").click(function(){
	var apndtext='';
	apndtext+='<div class="form-group marRl0 " id="MedTest-row' + service_row + '">';
	apndtext+='<div class="MdTest">';
	apndtext+=$("#MdTest").clone().html();
	apndtext += '</div>';
	
	apndtext += '<button type="button" onclick="$(\'#MedTest-row' + service_row  + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger  mrgl10 mrgt25"><i class="fa fa-minus-circle"></i></button>';
	
	apndtext += '</div>';
	$("#aditionalMedicalTest").append(apndtext);

	//$("#service-row"+service_row+ " select").addClass("servicearea");
	service_row++;

		
		});
	
});
//To select country name
/*function setindiv(val){
 $("#keyword").val(val);
  $("#autocomplete_s").hide();
 }*/
</script>
  <script>
$(document).ready(function(){
	
	$(document).on('keyup', '.medicin', function() {
		//alert($(this).attr('id'));
		var txtID = $(this).attr('id');
		var postVal = $(this).val()+'-'+txtID;
		$("#autocomplete_s").hide();
		if($("#autocomplete_s").val()==''){
			$("#autocomplete_s").hide();
			}
		$.ajax({
		type: "POST",
		url: "index.php?route=doctor/patients/get_medicine_list",
		data:'keyword='+postVal,
		<!--beforeSend: function(){$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");},-->
		success: function(data){
			$("#autocomplete_s").show();
			$("#autocomplete_s").html(data);
		}
		});
	});
	return false;
});
//To select country name
function setindiv(val,divid){
  var txtID="#"+divid;
 $("#"+divid).val(val);
  $("#autocomplete_s").hide();
 }
</script>

<?php echo $footer; ?>