<?php echo $header; ?>
<?php echo $column_left; ?>
</nav>
<div id="page-wrapper">
    <div class="container-fluid">
      <div class="row">
       		<div class="col-sm-12"> 
          <h2 class="appointments"><?php echo $entry_select_timing; ?></h2>
          <div style="text-align:center;">
          <span class="success-msg"> <?php echo $success; ?> </span>  </div>
<?php
function selectTimesOfDay($time,$exist_appointment,$location_id) {
$ret = explode('-', $time);
$tm=$ret[0];
$startTime=str_replace("am","",$tm);
$startTime=str_replace("pm","",$startTime);
$data = $time;    
$whatIWant = substr($data, strpos($data, "-") + 1);  
$endtime=str_replace("am","",$whatIWant);
$endtime=str_replace("pm","",$endtime);  
//echo $endtime;
    $open_time = strtotime($startTime);
    $close_time = strtotime($endtime);
    $now = time();
    $output = "";


    for( $i=$open_time; $i<$close_time; $i+=900) {
   
       if (in_array(date("H:i",$i), $exist_appointment)){
        $output .= "<li class='col-md-3 col-sm-4 text-center'><div class='btn_style btn btn-warning disabled'>".date("H:i",$i)."</div></li>";
	   }
	   else
	   {
		  $output .= "<li class='col-md-3 col-sm-4 text-center'><a  href='#' data-toggle='modal' data-target='#paymentModal' id='AppTimming'  class='btn btn-primary app_time btn_style' data-value='".date("H:i",$i)."' data-location='".$location_id."'>".date("H:i",$i)."</a></li>"; 
	   }
       
    }
    return $output;
}
?>
<?php 
if ($availabilitylist) { ?>

<?php foreach ($availabilitylist as $availability) { 

if(!empty($availability)){
$timing=selectTimesOfDay($availability['timing'],$exist_appointment,$availability['id']);

?>
<div class="mr_Tb30 avality_box">
<h3 align="center" class="list_heading"><?php echo $availability['locality'];?></h3>
<div class="row">

<ul class="list-unstyled col-sm-12">

      
     <?php  echo selectTimesOfDay($availability['timing'],$exist_appointment,$availability['id']);?>
  </ul> </div> </div><?php }} ?> <?php }?>

              
<div class="modal fade" id="paymentModal" role="dialog">
                                    <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                    <div class="modal-header modal_back">
                                    <button type="button"  class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title display-td text-center">Payment Process</h4>
                                    </div>
                                    <div class="modal-body">
                                    <div class="clearfix">&nbsp;</div>
                                    <p>
                                    <div class="col-xs-12 col-md-12">
        
     	  <div id="infopanel">
				<div id="befourpayment" class="text-center">
                </div>
          <div id="donepayment">
          <p > <span style="color:green;" class="text-center">Your Payment Successful.</span>
          <br> 
          <a href="<?php echo $action_premium;?>" class="subscribe btn btn-success btn-lg btn-block" style="color:#fff;">Proceed Now.</a>
          </p></div> </div>
          
     	  <!-- CREDIT CARD FORM STARTS HERE -->
            <div class="panel panel-default credit-card-box" id="bxpanel">
                <div class="panel-heading display-table">
                    <div class="row display-tr">
                        <div class="display-td">                            
                            
                        </div>
                    </div>                    
                </div>
                <div class="panel-body">
                    <form role="form" id="payment-form" method="POST" action="javascript:void(0);">
                        
                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="couponCode">AMOUNT </label>
                                    <input class="form-control" name="package" type="text" placeholder="Amount ?" >
                                </div>
                            </div>                        
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-4">
                         <a href="#" class="btn btn-success btn-block" style="color:#fff;" id="bookAppointment">Pay </a>
                         <input type="hidden" id="cus_id" name="cus_id" />
                         <input type="hidden" id="timing" name="timing" />
                         <input type="hidden" id="appint_loc" name="locationID" />
                         <input type="hidden" id="booking_date" name="booking_date" value="<?php echo $booking_date; ?>" />
                            </div>
                        </div>
                        <div class="row" style="display:none;">
                            <div class="col-xs-12">
                                <p class="payment-errors"></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>      
            <div class="panel-default credit-card-box" id="bxSucess" style="display:none; background-color:#F0F8FF">
               
                </div>      
            <!-- CREDIT CARD FORM ENDS HERE -->
		<script type="text/javascript">
        //THIS  CODE FOR 500 PAYMENT  BUTTON ONE 
        $(document).ready(function(){
        $("#donepayment").hide();
        i = 6;
        function onTimer() {
        document.getElementById('befourpayment').innerHTML = "<img src='image/catalog/loader.gif'><br><p>Processing...</p>";
        i--;
        if(i<0)
        {	
        $("#donepayment").show('slow');
        $("#befourpayment").hide('slow');
        }
        else { 	
        setTimeout(onTimer, 1000);
        }
        }
        $("#infopanel").hide();
        $("#pay500").click(function(){
        $("#bxpanel").hide();
        $("#infopanel").show('slow');
        onTimer();
        });
        });
        </script>
            
        </div>
                                    </p>
                                    <div class="clearfix">&nbsp;</div>
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-default"  data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
 
        </div>

      </div>

      

      <!-- /.row --> 

    </div>
 
    <!-- /.container-fluid --> 

    

  </div>
<script>
$(".app_time").click(function(){
var appoint_time =	$(this).attr("data-value");
var appoint_location =	$(this).attr("data-location");
var docID= '<?php echo $appoint_cus; ?>';
    $("#timing").val(appoint_time);
	$("#appint_loc").val(appoint_location);
	$("#cus_id").val(docID);});
</script>
<script>
$("#bookAppointment").click(function(){
var appoint_time =	$(this).attr("data-value");
var data = $('form').serialize();
$.ajax({
type: "POST",
url: "index.php?route=doctor/appointment/payment",
data: data,
cache: false,
success: function(result){
	jQuery('#bxpanel').css("display","none")
	jQuery('#bxSucess').css("display","block")
	$( "#bxSucess" ).append(result);

//window.location = 'index.php?route=patient/patient';
}
});
	});
</script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<?php echo $footer; ?>
<script src="catalog/view/javascript/bootstrap-toggle.min.js"></script>
