<!-----------banner start---------->

<section class="_style_bg banner input-group" style="background-image:url(catalog/view/theme/default/image/banner.png);"> 
    <div class="_bg_overlay white"></div>
    <div class="media-body media-middle _upr_lyr ovrfl">
        <div class="container">
            <h1 class="text-center">Find and book a doctor</h1>
            <div class="search_bar col-md-10 col-md-offset-1 clearfix">
             <form class="" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" >
            	<div class="col-sm-5 _pad_lft0">
                	<div class="txt_box loc">
                    	<input type="text"  name="autocomplete"class="form-control _pad_lft35" placeholder="Location"  id="autocomplete" onFocus="geolocate()"/>
                        <input type="hidden" name="latitude" value="" id="latitude">
                        <input type="hidden" name="longitude" value="" id="longitude">
                        <input type="text" name="Locality" class="form-control _pad_lft35 seprator" placeholder="Locality" />
                        <i class="fa fa-compass fa-2x icon_pos" aria-hidden="true"></i>
                    </div>
                </div>
                <div class="col-sm-5 _pad_lft0">
                	<div class="txt_box spec">
                    	<input type="text" name="keyword" id="keyword" class="form-control _pad_lft35" placeholder="Specialities, Doctors, Clinics, Hospitals, Labs, Spas" autocomplete="off"/>
                        
          <input type="hidden" name="keyword_id" id="keyword_id" value=""/>
                        <div id='autocomplete_s' class="search"></div>
                        <i class="fa fa-2x fa-stethoscope icon_pos" aria-hidden="true"></i>

                    </div>
                </div>
                <div class="col-sm-2 srch_btn">
                	<button type="submit" class="btn btn_blue regbtn">SEARCH</button>
                </div>
                
                     </form>
                
            </div>
        </div>
    </div> 
</section> 

<!-----------banner end----------> 
<style>
#autocomplete_s{position:absolute; top:50px; z-index:99; background:#fff; width:100%; border:1px solid #3CF; border-radius:8px; padding-top:10px; left:0px;}
#autocomplete_s li a{text-decoration:none;} 
#autocomplete_s { display:none;}
.ovrfl{overflow:auto;}
.lst{}
</style>
<!----GEO SCRIPT GOOGLE API---->
<script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
      
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
		  $('#autocomplete').focus();
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV8IR0pybsjbjBMPs3kVUsvdlamzdcQw4&libraries=places&callback=initAutocomplete" async defer></script>
<script type="text/javascript">
$( document ).ready(function() {
	$( "#autocomplete" ).on('blur focusout', function() {
	
	var geocoder = new google.maps.Geocoder();
	$('#autocomplete').focus();
	var address = $('#autocomplete').val(); 
geocoder.geocode( { 'address': address}, function(results, status) {
if (status == google.maps.GeocoderStatus.OK) {
    var latitude = results[0].geometry.location.lat();
	var longitude = results[0].geometry.location.lng();
   $('#longitude').val(longitude);
   $('#latitude').val(latitude);
   
    } 
}); 
});
});
</script>
<!--- GEO SCRIPT GOOGLE API---->
<script>
$(document).ready(function(){
	$("#keyword").keyup(function(){
		$("#autocomplete_s").hide();
		if($("#autocomplete_s").val()==''){
			$("#autocomplete_s").hide();
			}
		$.ajax({
		type: "POST",
		url: "index.php?route=doctor/doctor/readSpacilities",
		data:'keyword='+$(this).val(),
		<!--beforeSend: function(){$("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");},-->
		success: function(data){
			$("#autocomplete_s").show();
			$("#autocomplete_s").html(data);
		}
		});
	});
	return false;
});
//To select country name
function setindiv(val){
	
  $("#keyword").val(val);
  
  $("#autocomplete_s").hide();
 
 }
</script>