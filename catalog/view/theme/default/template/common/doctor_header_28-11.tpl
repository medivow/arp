<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php //echo $direction; ?>" lang="<?php //echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; ?></title>
<base href="<?php //echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>


<link href="catalog/view/theme/default/stylesheet/bootstrap.min.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/bootstrap-datetimepicker.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/sb-admin.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/style-admin.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/front-dashboard.css" rel="stylesheet">
<link href="catalog/view/theme/default/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="catalog/view/theme/default/stylesheet/list.css" rel="stylesheet">
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
</head>
<body>
<div id="wrapper"> 
<div id="AddUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header modal_back">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <!--<h4 class="modal-title">Modal Header</h4>-->
        <h4 class="text-center _pad_tb20">Add Patient</h4>
        
      </div>
      <div class="modal-body ">
      <div id="response" class="txlc">
        </div>
      <section class="medivow_signup martp_sticky _pad_tp20 _pad_btm30">
	<div class="row">
		
        <form class="form_mdw  col-xs-10 col-xs-offset-1 col-md-8 col-md-offset-2 _mar_btm35" method="POST" action="javascript:void(0);">
        	<div class="input_styl">
            	<input type="text" required
                 class="form-control" name="firstname" value="" placeholder="First Name" id="input-firstname"/>
                <i class="fa fa-user"></i>
            </div>
            <div class="input_styl">
            	<input type="text" class="form-control"name="lastname" required value="" placeholder="Last Name"/>
                <i class="fa fa-user"></i>
            </div>
            <div class="input_styl">
            	<input type="text" class="form-control" required name="username" value="" placeholder="User Name" id="input-username"/>
                <i class="fa fa-user"></i>
            </div>
           
           <div class="input_styl">
            	<input type="text" class="form-control" name="mobile" required value="" placeholder="Mobile" id="input-moobile"/>
                <i class="fa fa-mobile" aria-hidden="true"></i>
            </div>
            <div class="input_styl">
            	<input type="text" class="form-control" name="email" required value="" placeholder="Email" id="input-email"/>
                <i class="fa fa-envelope" aria-hidden="true"></i>
            </div>
            <div class="input_styl">
            	<input type="password" class="form-control" name="password" required value="" placeholder="Password" id="input-password"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
            <div class="input_styl">
            	<input type="password" class="form-control" name="confirm" required value="" placeholder="Confirm Password" id="input-confirm"/>
                <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
           <!-- <h4 class="text-center _pad_tp20">Are you a doctor?</h4>
            <div class="checkbox text-center _mar_btm35">
  				<label><input data-toggle="toggle" name="doctor" type="checkbox"></label>
			</div>-->
            <input type="hidden" name="customer_group_id_reg" value="<?php echo $customer_group_id_reg; ?>" />
            
            <div class="mr_btm20"><input type="checkbox" name="agree" value="1" class="chck_mar mrgr10"/>I have read and agree to the Terms of use
             
            </div>
                   
            <div class="clearfix _mar_btm30"><button type="submit" id="AddPatient" class="btn btn-default col-sm-6  col-sm-offset-3 col-xs-12">Save</button></div>
        </form>
       </div>
        </section>
    </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
  <!-- Navigation -->
  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="<?php echo $action_dashboard; ?>">Dashboard</a> </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
     <li class="dropdown"> <a href="<?php echo $action_dashboard; ?>" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?php echo $image; ?>"> <?php echo $welcome_message; ?> </a></li>
      <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account <b class="caret"></b></a>
      	<ul class="dropdown-menu">
          <li> <a href="<?php echo $action_account; ?>"><i class="fa fa-fw fa-user"></i> Profile</a> </li>
          <li> <a href="<?php echo $action_password; ?>"><i class="fa fa-fw fa-lock"></i>  Password</a> </li>
         <!-- <li> <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a> </li>-->
          <li class="divider"></li>
          <!--<li> <a href="<?php echo $action_logout; ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a> </li>-->
        </ul>
      
      </li>
     
      <li class=""> <a href="<?php echo $action_logout; ?>" class="" ><i class="fa fa-power-off"></i> Log Out </a>
        
      </li>
    </ul>
    