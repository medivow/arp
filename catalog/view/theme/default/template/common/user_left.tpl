<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

    <div class="collapse navbar-collapse navbar-ex1-collapse">
      <div class="side-nav">
      <ul class="nav navbar-nav" id="sideFotr">
        <li> <a>
          <!--<div class="user text-center"> <img src="<?php echo $image; ?>">
            <p><?php echo $welcome_message; ?></p>
          </div>-->
          </a> </li>
        <li> <a href="<?php echo $action_home; ?>"><i class="fa fa-home" aria-hidden="true"></i></i> Home</a> </li>
        <li> <a href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> View Hospitals</a> </li>
        <li> <a href="<?php echo $action_doctors ;?>"><i class="fa fa-user-md" aria-hidden="true"></i> View Doctors</a> </li>
        <li> <a href="<?php echo $action_appointments ; ?>"><i class="fa fa-clock-o" aria-hidden="true"></i> Appointments</a> </li>
        <li> <a href="<?php echo $action_prescription; ?>"><i class="fa fa-lock" aria-hidden="true"></i> View Prescriptions</a> </li>
        <li> <a href="<?php echo $action_billing; ?>"> <i class="fa fa-credit-card" aria-hidden="true"></i> View Past Payments</a> </li>
         <li> <a href="<?php echo $action_attachment; ?>"> <i class="fa fa-file-o" aria-hidden="true"></i> Attachments </a> </li>
        <!--<li> <a href="<?php echo $action_family; ?>"> <i class="fa fa-female" aria-hidden="true"></i> View Family Dashboard</a> </li>-->
        <li> <a href="index.php?route=patient/appointment/package">
          <div class="Consult text-center"> <img src="catalog/view/theme/default/image/doctor.png">
            <p>Consult a Doctor</p>
          </div>
          </a> </li>
      </ul>
      </div>
    </div>
    <!-- /.navbar-collapse --> 
  </nav>
  