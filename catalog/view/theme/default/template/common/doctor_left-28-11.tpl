
    <div class="collapse navbar-collapse navbar-ex1-collapse">
    
    <div class="side-nav">
      <ul class="nav navbar-nav" id="sideFotr">
     
        <li > <a  href="#" data-toggle="modal" data-target="#AddUser"><i class="fa fa-user" aria-hidden="true"></i> Add Patient</a> </li>
        <li > <a href="<?php echo $action_patient; ?>"><i class="fa fa-list-alt" aria-hidden="true"></i> Patients list</a> </li>
        <li> <a href="<?php echo $action_cse_studay; ?>"><i class="fa fa-home" aria-hidden="true"></i></i> Case Study</a> </li>
        <li> <a href="<?php echo $action_daily_essay; ?>"><i class="fa fa-file-text-o" aria-hidden="true"></i> Daily Essay</a> </li>
        <li> <a href="<?php echo $action_calendar; ?>"><i class="fa fa-calendar" aria-hidden="true"></i> Calendar</a> </li>
        <li> <a href="<?php echo $action_billing; ?>"> <i class="fa fa-hand-o-down" aria-hidden="true"></i> Billings</a> </li>
         <li> <a href="<?php echo $action_attachment; ?>"> <i class="fa fa-file-o" aria-hidden="true"></i> Attachments </a> </li>
        <!-- <li> <a href="#" data-toggle="modal" data-target="#AddUser">
          <div class="Consult text-center"> <img width="60" height="59" src="catalog/view/theme/default/image/add_user.png">
            <p>Add User</p>
          </div>
          </a> </li>-->
        <li> <a href="index.php?route=doctor/appointment" >
          <div class="Consult text-center"> <img src="catalog/view/theme/default/image/doctor.png">
            <p>Offline Appointment</p>
          </div>
          </a> </li>
      </ul>
    </div>
    </div>
    
    <!-- /.navbar-collapse --> 
  </nav>