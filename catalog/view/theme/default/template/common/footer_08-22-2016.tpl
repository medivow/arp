<footer class="medivow_footer">
    <section class="container _pad_tp20">
        <div class="row">
            <div class="col-sm-3 _mar_btm30">
                <h4>for pateints</h4>
                <ul>
                    <li><a href="#">Medivow search</a></li>
                    <li><a href="#">Medivow Appointment</a></li>
                    <li><a href="#">Medivow WellBeing</a></li>
                    <li><a href="#">Medivow Health Profile</a></li>
                    <li><a href="#">Medivow Diet Chart</a></li>
                    <li><a href="#">Medivow health tips</a></li>
                    <li><a href="#">Medivow Doctor Consult</a></li>
                    <li><a href="#">Medivow Ancillary</a></li>
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>for doctors</h4>
                <ul>
                  <!--  <li><a href="#">Medivow Consult</a></li>
                    <li><a href="#">Medivow Health Feed</a></li>-->
                    <li><a href="#">Medivow Profile</a></li>
                    <li><a href="#">Medivow Premium Profile</a></li>
                    <li><a href="#">Medivow Clinic</a></li>
                    <li><a href="#">Medivow Hospital</a></li>
                    <li><a href="#">Medivow Multispecialty</a></li>
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>download</h4>
                <ul>
                    <li><a href="#"><img src="catalog/view/theme/default/image/android.png"/></a></li>
                    <!--<li><a href="#"><img src="catalog/view/theme/default/image/appstore.png"/></a></li>-->
                </ul>
            </div>
            <div class="col-sm-3 _mar_btm30">
                <h4>contact</h4>
                <ul>
                    <li><a href="#">Phone no. :98373837876</a></li>
                    <li><a href="#">Email: example@medivow.com</a></li>
                    <li><h4>CONECT WITH US</h4></li>
                    <li class="medivow_social _mar_tp20">
                    	<a href="#"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>
                        <a href="#" class="_mar_lr10"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-skype fa-2x" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <section class="medivow_copyright text-center">
    	<div class="container">
            <ul>
            	<!--<li class="copyright_text">MediVOW © 2016</li>-->
                <li class="copyright_text">Medivow Technology pvt. ltd.</li>
                <li><a href="#">Terms of Use</a></li>
                <li><a href="#">Privacy Policy</a></li>
                <li><a href="#">Sitemap</a></li>
            </ul>
        </div>
    </section>
</footer>
<!-----------footer end---------->
<!-----------scroll to top start---------->
<div><a href="#" class="scrollToTop"><i class="fa fa-angle-up fa-2x" aria-hidden="true"></i></a></div>
<!-----------scroll to top end---------->

<script src="catalog/view/javascript/jquery-3.0.0.min.js"></script>
<script src="catalog/view/javascript/bootstrap.min.js"></script>
<script src="catalog/view/javascript/owl.carousel.min.js"></script>
<script src="catalog/view/javascript/jquery-ui.js"></script>
<script src="catalog/view/javascript/main.js"></script>
<script>
	$("#slider-range").slider({
    range: true,
    min: 0,
    max: 1440,
    step: 15,
    values: [540, 1020],
    slide: function (e, ui) {
        var hours1 = Math.floor(ui.values[0] / 60);
        var minutes1 = ui.values[0] - (hours1 * 60);

        if (hours1.length == 1) hours1 = '0' + hours1;
        if (minutes1.length == 1) minutes1 = '0' + minutes1;
        if (minutes1 == 0) minutes1 = '00';
        if (hours1 >= 12) {
            if (hours1 == 12) {
                hours1 = hours1;
                minutes1 = minutes1 + " PM";
            } else {
                hours1 = hours1 - 12;
                minutes1 = minutes1 + " PM";
            }
        } else {
            hours1 = hours1;
            minutes1 = minutes1 + " AM";
        }
        if (hours1 == 0) {
            hours1 = 12;
            minutes1 = minutes1;
        }



        $('.slider-time').html(hours1 + ':' + minutes1);

        var hours2 = Math.floor(ui.values[1] / 60);
        var minutes2 = ui.values[1] - (hours2 * 60);

        if (hours2.length == 1) hours2 = '0' + hours2;
        if (minutes2.length == 1) minutes2 = '0' + minutes2;
        if (minutes2 == 0) minutes2 = '00';
        if (hours2 >= 12) {
            if (hours2 == 12) {
                hours2 = hours2;
                minutes2 = minutes2 + " PM";
            } else if (hours2 == 24) {
                hours2 = 11;
                minutes2 = "59 PM";
            } else {
                hours2 = hours2 - 12;
                minutes2 = minutes2 + " PM";
            }
        } else {
            hours2 = hours2;
            minutes2 = minutes2 + " AM";
        }

        $('.slider-time2').html(hours2 + ':' + minutes2);
    }
});
	</script>
</body>

</body></html>