<?php
// Heading
$_['heading_title']     = 'Specialities';

// Text
$_['text_success']      = 'Success: You have modified Specialities!';
$_['text_list']         = 'Speciality List';
$_['text_add']          = 'Add Speciality';
$_['text_edit']         = 'Edit Speciality';

// Column
$_['column_name']       = 'Speciality Name';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Speciality Name';
$_['entry_description'] = 'Description';
$_['entry_approval']    = 'Approve New Customers';
$_['entry_status']      = 'Status';

// Help
$_['help_approval']     = 'Customers must be approved by an administrator before they can login.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify Specialities!';
$_['error_name']         = 'Speciality Name must be between 3 and 32 characters!';
$_['error_default']      = 'Warning: This Speciality cannot be deleted as it is currently assigned as the default store Speciality!';
$_['error_store']        = 'Warning: This Speciality cannot be deleted as it is currently assigned to %s stores!';
$_['error_customer']     = 'Warning: This Speciality cannot be deleted as it is currently assigned to %s customers!';