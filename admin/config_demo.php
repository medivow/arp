<?php
// HTTP
define('HTTP_SERVER', 'http://conversaint.com/medivow/admin/');
define('HTTP_CATALOG', 'http://conversaint.com/medivow/');

// HTTPS
define('HTTPS_SERVER', 'http://conversaint.com/medivow/admin/');
define('HTTPS_CATALOG', 'http://conversaint.com/medivow/');

// DIR
define('DIR_APPLICATION', '/home/khushi1/public_html/conversaint.com/medivow/admin/');
define('DIR_SYSTEM', '/home/khushi1/public_html/conversaint.com/medivow/system/');
define('DIR_IMAGE', '/home/khushi1/public_html/conversaint.com/medivow/image/');
define('DIR_LANGUAGE', '/home/khushi1/public_html/conversaint.com/medivow/admin/language/');
define('DIR_TEMPLATE', '/home/khushi1/public_html/conversaint.com/medivow/admin/view/template/');
define('DIR_CONFIG', '/home/khushi1/public_html/conversaint.com/medivow/system/config/');
define('DIR_CACHE', '/home/khushi1/public_html/conversaint.com/medivow/system/storage/cache/');
define('DIR_DOWNLOAD', '/home/khushi1/public_html/conversaint.com/medivow/system/storage/download/');
define('DIR_LOGS', '/home/khushi1/public_html/conversaint.com/medivow/system/storage/logs/');
define('DIR_MODIFICATION', '/home/khushi1/public_html/conversaint.com/medivow/system/storage/modification/');
define('DIR_UPLOAD', '/home/khushi1/public_html/conversaint.com/medivow/system/storage/upload/');
define('DIR_CATALOG', '/home/khushi1/public_html/conversaint.com/medivow/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'medivow');
define('DB_PASSWORD', 'medivow');
define('DB_DATABASE', 'medivow');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
