<?php
class ModelCatalogDoctor extends Model {
	public function addDoctor($data) {
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "doctors SET name = '" . $this->db->escape($data['name']) . "', speciality = '" . $this->db->escape($data['speciality']) . "', mobile = '" . $this->db->escape($data['mobile']) . "', email = '" . $this->db->escape($data['email']) . "', office_timing = '" . $this->db->escape($data['timing']) . "', country = 'india', state = '" . $this->db->escape($data['state']) . "', city = '" . $this->db->escape($data['city']) . "', locality = '" . $data['locality'] . "', address = '" . $data['address'] . "', date_added = NOW()");

		$doctor_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "doctors SET image = '" . $this->db->escape($data['image']) . "' WHERE id = '" . (int)$doctor_id . "'");
		}

		foreach ($data['doctor_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_description SET doctor_id = '" . (int)$doctor_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}

		if (isset($data['doctor_services'])) {
			$service_array=explode(" ",$data['doctor_services']);
			foreach ($service_array as $service_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_services SET doctor_id = '" . (int)$doctor_id . "', title = '" . $this->db->escape(service_id) . "'");
			}
		}

		if (isset($data['doctor_image'])) {
			foreach ($data['doctor_image'] as $doctor_image) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_image SET doctor_id = '" . (int)$doctor_id . "', image = '" . $this->db->escape($doctor_image['image']) . "', sort_order = '" . (int)$doctor_image['sort_order'] . "'");
			}
		}
		
		if (isset($data['services'])) {
			foreach ($data['services'] as $service) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_services SET doctor_id = '" . (int)$doctor_id . "', title = '" . $this->db->escape($service['service']) . "',language_id = '" . (int)$language_id . "'");
			}
		}

		/*if (isset($data['keyword'])) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}*/

	
		$this->cache->delete('doctor');

		return $doctor_id;
	}

	public function editDoctor($doctor_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "doctors SET name = '" . $this->db->escape($data['name']) . "', speciality = '" . $this->db->escape($data['speciality']) . "', mobile = '" . $this->db->escape($data['mobile']) . "', email = '" . $this->db->escape($data['email']) . "', office_timing = '" . $this->db->escape($data['janoffice_timing']) . "', country = '" . $this->db->escape($data['country']) . "', state = '" . $this->db->escape($data['state']) . "', city = '" . $this->db->escape($data['city']) . "', locality = '" . $data['locality'] . "', address = '" . $data['address'] . "', date_modified = NOW() WHERE doctor_id = '" . (int)$doctor_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "doctors SET image = '" . $this->db->escape($data['image']) . "' WHERE doctors_id = '" . (int)$doctors_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '" . (int)$doctor_id . "'");

		foreach ($data['doctor_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_description SET doctor_id = '" . (int)$doctor_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_services WHERE doctor_id = '" . (int)$doctor_id . "'");
		if (isset($data['doctor_services'])) {
			foreach ($data['doctor_services'] as $service_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_services SET doctor_id = '" . (int)$doctor_id . "', title = '" . $this->db->escape($value['doctor_service_title']) . "'");

		}
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_image WHERE doctor_id = '" . (int)$doctor_id . "'");

		if (isset($data['doctor_image'])) {
			foreach ($data['doctor_image'] as $doctor_image) {$this->db->query("INSERT INTO " . DB_PREFIX . "doctor_image SET doctor_id = '" . (int)$doctor_id . "', image = '" . $this->db->escape($doctor_image['image']) . "', sort_order = '" . (int)$doctor_image['sort_order'] . "'");}
		}

		$this->cache->delete('doctor');
	}

	public function copyDoctor($doctor_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "doctors p WHERE p.id = '" . (int)$doctor_id . "'");

		if ($query->num_rows) {
			$data = $query->row;

			$data['sku'] = '';
			$data['upc'] = '';
			$data['viewed'] = '0';
			$data['keyword'] = '';
			$data['status'] = '0';

			$data['product_attribute'] = $this->getProductAttributes($product_id);
			$data['product_description'] = $this->getProductDescriptions($product_id);
			$data['product_discount'] = $this->getProductDiscounts($product_id);
			$data['product_filter'] = $this->getProductFilters($product_id);
			$data['product_image'] = $this->getProductImages($product_id);
			$data['product_option'] = $this->getProductOptions($product_id);
			$data['product_related'] = $this->getProductRelated($product_id);
			$data['product_reward'] = $this->getProductRewards($product_id);
			$data['product_special'] = $this->getProductSpecials($product_id);
			$data['product_category'] = $this->getProductCategories($product_id);
			$data['product_download'] = $this->getProductDownloads($product_id);
			$data['product_layout'] = $this->getProductLayouts($product_id);
			$data['product_store'] = $this->getProductStores($product_id);
			$data['product_recurrings'] = $this->getRecurrings($product_id);

			$this->addDoctor($data);
		}
	}

	public function deleteProduct($doctor_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctors WHERE id = '" . (int)$doctor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '" . (int)$doctor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_services WHERE doctor_id = '" . (int)$doctor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "doctor_image WHERE doctor_id = '" . (int)$doctor_id . "'");
		
		$this->cache->delete('doctor');
	}

	public function getDoctor($doctor_id) {
		$query = $this->db->query("SELECT DISTINCT *, (SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'id=" . (int)$doctor_id . "') AS keyword FROM " . DB_PREFIX . "doctors p LEFT JOIN " . DB_PREFIX . "doctor_description pd ON (p.id = pd.doctor_id) WHERE p.id = '" . (int)$doctor_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getDoctors($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "Doctors d LEFT JOIN " . DB_PREFIX . "doctor_description dd ON (d.id = dd.doctor_id) WHERE dd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND dd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		if (!empty($data['filter_speciality'])) {
			$sql .= " AND d.speciality LIKE '" . $this->db->escape($data['filter_speciality']) . "%'";
		}

		if (isset($data['filter_state'])) {
			$sql .= " AND d.state LIKE '" . $this->db->escape($data['filter_state']) . "%'";
		}

		if (isset($data['filter_city']) ) {
			$sql .= " AND d.city = '" . $data['filter_city'] . "'";
		}
		if (isset($data['filter_locality']) ) {
			$sql .= " AND d.locality = '" . $data['filter_locality'] . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND d.status = '" . (int)$data['filter_status'] . "'";
		}

		$sql .= " GROUP BY d.id";

		$sort_data = array(
			'dd.name',
			'p.speciality',
			'p.state',
			'p.city',
			'p.status',
			'p.locality'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY dd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getProductsByCategoryId($category_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

		return $query->rows;
	}

	public function getDoctorDescriptions($doctor_id) {
		$doctor_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_description WHERE doctor_id = '" . (int)$doctor_id . "'");

		foreach ($query->rows as $result) {
			$doctor_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'description'      => $result['description'],
				'meta_title'       => $result['meta_title'],
				'meta_description' => $result['meta_description'],
				'meta_keyword'     => $result['meta_keyword'],
				'tag'              => $result['tag']
			);
		}

		return $doctor_description_data;
	}
	public function getDoctorServices($doctor_id) {
			$doctor_service_data = array();
	       $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_services WHERE doctor_id = '" . (int)$doctor_id . "'");
	
			foreach ($query->rows as $result) {
				$doctor_service_data[$result['language_id']] = array(
					'doctor_id'        => $result['doctor_id'],
					'title'            => $result['title'],
					
				);
			}

			return $query->rows;
		}
	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}

	public function getProductFilters($product_id) {
		$product_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_filter_data[] = $result['filter_id'];
		}

		return $product_filter_data;
	}

	public function getProductAttributes($product_id) {
		$product_attribute_data = array();

		$product_attribute_query = $this->db->query("SELECT attribute_id FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' GROUP BY attribute_id");

		foreach ($product_attribute_query->rows as $product_attribute) {
			$product_attribute_description_data = array();

			$product_attribute_description_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

			foreach ($product_attribute_description_query->rows as $product_attribute_description) {
				$product_attribute_description_data[$product_attribute_description['language_id']] = array('text' => $product_attribute_description['text']);
			}

			$product_attribute_data[] = array(
				'attribute_id'                  => $product_attribute['attribute_id'],
				'product_attribute_description' => $product_attribute_description_data
			);
		}

		return $product_attribute_data;
	}

	public function getProductOptions($product_id) {
		$product_option_data = array();

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		foreach ($product_option_query->rows as $product_option) {
			$product_option_value_data = array();

			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");

			foreach ($product_option_value_query->rows as $product_option_value) {
				$product_option_value_data[] = array(
					'product_option_value_id' => $product_option_value['product_option_value_id'],
					'option_value_id'         => $product_option_value['option_value_id'],
					'quantity'                => $product_option_value['quantity'],
					'subtract'                => $product_option_value['subtract'],
					'price'                   => $product_option_value['price'],
					'price_prefix'            => $product_option_value['price_prefix'],
					'points'                  => $product_option_value['points'],
					'points_prefix'           => $product_option_value['points_prefix'],
					'weight'                  => $product_option_value['weight'],
					'weight_prefix'           => $product_option_value['weight_prefix']
				);
			}

			$product_option_data[] = array(
				'product_option_id'    => $product_option['product_option_id'],
				'product_option_value' => $product_option_value_data,
				'option_id'            => $product_option['option_id'],
				'name'                 => $product_option['name'],
				'type'                 => $product_option['type'],
				'value'                => $product_option['value'],
				'required'             => $product_option['required']
			);
		}

		return $product_option_data;
	}

	public function getProductOptionValue($product_id, $product_option_value_id) {
		$query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$product_id . "' AND pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getDoctorImages($doctor_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "doctor_image WHERE doctor_id = '" . (int)$doctor_id . "' ORDER BY sort_order ASC");

		return $query->rows;
	}

	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");

		return $query->rows;
	}

	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");

		return $query->rows;
	}

	public function getProductRewards($product_id) {
		$product_reward_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}

		return $product_reward_data;
	}

	public function getProductDownloads($product_id) {
		$product_download_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}

		return $product_download_data;
	}

	public function getProductStores($product_id) {
		$product_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_store_data[] = $result['store_id'];
		}

		return $product_store_data;
	}

	public function getProductLayouts($product_id) {
		$product_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $product_layout_data;
	}

	public function getProductRelated($product_id) {
		$product_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}

		return $product_related_data;
	}

	public function getRecurrings($product_id) {
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_recurring` WHERE product_id = '" . (int)$product_id . "'");

		return $query->rows;
	}

	public function getTotalDoctors($data = array()) {
		$sql = "SELECT COUNT(DISTINCT d.id) AS total FROM " . DB_PREFIX . "doctors d LEFT JOIN " . DB_PREFIX . "doctor_description dd ON (d.id = dd.doctor_id)";

		$sql .= " WHERE dd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND dd.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}
if (!empty($data['filter_speciality'])) {
			$sql .= " AND d.speciality LIKE '" . $this->db->escape($data['filter_speciality']) . "%'";
		}

		if (isset($data['filter_state'])) {
			$sql .= " AND d.state LIKE '" . $this->db->escape($data['filter_state']) . "%'";
		}

		if (isset($data['filter_city']) ) {
			$sql .= " AND d.city = '" . $data['filter_city'] . "'";
		}
		if (isset($data['filter_locality']) ) {
			$sql .= " AND d.locality = '" . $data['filter_locality'] . "'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND d.status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getTotalProductsByTaxClassId($tax_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE tax_class_id = '" . (int)$tax_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByStockStatusId($stock_status_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE stock_status_id = '" . (int)$stock_status_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByWeightClassId($weight_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE weight_class_id = '" . (int)$weight_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLengthClassId($length_class_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE length_class_id = '" . (int)$length_class_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByDownloadId($download_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_download WHERE download_id = '" . (int)$download_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByManufacturerId($manufacturer_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByAttributeId($attribute_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_attribute WHERE attribute_id = '" . (int)$attribute_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByOptionId($option_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$option_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByProfileId($recurring_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_recurring WHERE recurring_id = '" . (int)$recurring_id . "'");

		return $query->row['total'];
	}

	public function getTotalProductsByLayoutId($layout_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "product_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

		return $query->row['total'];
	}
}
