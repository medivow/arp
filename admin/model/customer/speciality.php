<?php
class ModelCustomerSpeciality extends Model { 
	public function addCustomerGroup($data) { 
		$this->db->query("INSERT INTO " . DB_PREFIX . "speciality SET title = '" . $data['speciality_description']. "', date_added = '" .date(). "', status = '" . (int)$data['status'] . "'");

		$speciality_id = $this->db->getLastId();
		return $speciality_id;
	}

	public function editCustomerGroup($speciality_id, $data) {
  
     $this->db->query("UPDATE " . DB_PREFIX . "speciality SET title = '" . $data['speciality_description']. "' , status = '" . (int)$data['status'] . "' , date_added  = '" .date(). "' WHERE id= '" . (int)$speciality_id . "' ");   
	}


	public function deleteCustomerGroup($speciality_id) { 

		$this->db->query("DELETE FROM " . DB_PREFIX . "speciality WHERE id = '" . (int)$speciality_id . "'");

	}

	public function getCustomerGroup($speciality_id) {
		//$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "speciality cg LEFT JOIN " . DB_PREFIX . "speciality_description cgd ON (cg.speciality_id = cgd.speciality_id) WHERE cg.speciality_id = '" . (int)$speciality_id . "' AND cgd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "speciality  WHERE id = '" . (int)$speciality_id . "'");
		return $query->row;
	}

	public function getCustomerGroups($data = array()) { 
		$sql = "SELECT * FROM " . DB_PREFIX . "speciality as sp";
		$sort_data = array(
			'sp.title'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY sp.id";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCustomerGroupDescriptions($speciality_id) {
		$speciality_data = array();

	  $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "speciality WHERE id = '" . (int)$speciality_id . "'");
        foreach ($query->rows as $result) {
		    $speciality_data[] = array('name' => $result['title'], 'status' => $result['status']);
		}

		return $speciality_data;
	}

	public function getTotalCustomerGroups() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "speciality");

		return $query->row['total'];
	}
}
