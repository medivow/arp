<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button onclick="$('#form').submit();" type="submit" form="form-information" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
          <?php
            if(isset($error)){
                echo('<div style="color:red;text-align:center;font-weight:bold;">'.$error.'</div>');
            }
          ?>
        <table class="form">
          <tr>
            <td><?php echo $entry_status; ?></td>
            <td><select name="rest_api_status">
                <?php if ($rest_api_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
          </tr>
            <tr>
             <td>
          	    <label for="entry_key"><?php echo $entry_key; ?></label>
              </td>
              <td>
                <input type="text" name="rest_api_key" value="<?php echo $rest_api_key; ?>" />
              </td>
            </tr>
            <tr>
                <td>
                    <label for="entry_order_id"><?php echo $entry_order_id; ?></label>
                </td>
                <td>
                    <input type="text" name="rest_api_order_id" value="<?php echo $rest_api_order_id; ?>" required="required"/>
                </td>
            </tr>
        </table>
     
      </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>