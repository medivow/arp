<?php
class ControllerCatalogDoctor extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		$this->getList();
	}

	public function add() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_doctor->addDoctor($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_speciality'])) {
				$url .= '&filter_speciality=' . urlencode(html_entity_decode($this->request->get['filter_speciality'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_state'])) {
				$url .= '&filter_state=' . $this->request->get['filter_state'];
			}

			if (isset($this->request->get['filter_city'])) {
				$url .= '&filter_city=' . $this->request->get['filter_city'];
			}
			if (isset($this->request->get['locality'])) {
				$url .= '&locality=' . $this->request->get['locality'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function edit() {
		$this->load->language('catalog/doctor');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/doctor');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_doctor->editDoctor($this->request->get['id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getForm();
	}

	public function delete() {
		$this->load->language('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->deleteProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	public function copy() {
		$this->load->language('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product');

		if (isset($this->request->post['selected']) && $this->validateCopy()) {
			foreach ($this->request->post['selected'] as $product_id) {
				$this->model_catalog_product->copyProduct($product_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_name'])) {
				$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_model'])) {
				$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->response->redirect($this->url->link('catalog/product', 'token=' . $this->session->data['token'] . $url, true));
		}

		$this->getList();
	}

	protected function getList() {
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_speciality'])) {
			$filter_speciality = $this->request->get['filter_speciality'];
		} else {
			$filter_speciality = null;
		}

		if (isset($this->request->get['filter_state'])) {
			$filter_state = $this->request->get['filter_state'];
		} else {
			$filter_state = null;
		}

		if (isset($this->request->get['filter_city'])) {
			$filter_city = $this->request->get['filter_city'];
		} else {
			$filter_city = null;
		}
		if (isset($this->request->get['filter_locality'])) {
			$filter_locality = $this->request->get['filter_locality'];
		} else {
			$filter_locality = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pd.name';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_speciality'])) {
			$url .= '&filter_speciality=' . urlencode(html_entity_decode($this->request->get['filter_speciality'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_state'])) {
			$url .= '&filter_state=' . $this->request->get['filter_state'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		if (isset($this->request->get['filter_locality'])) {
			$url .= '&filter_locality=' . $this->request->get['filter_locality'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true)
		);

		$data['add'] = $this->url->link('catalog/doctor/add', 'token=' . $this->session->data['token'] . $url, true);
		$data['copy'] = $this->url->link('catalog/doctor/copy', 'token=' . $this->session->data['token'] . $url, true);
		$data['delete'] = $this->url->link('catalog/doctor/delete', 'token=' . $this->session->data['token'] . $url, true);

		$data['doctors'] = array();

		$filter_data = array(
			'filter_name'	      => $filter_name,
			'filter_speciality'	  => $filter_speciality,
			'filter_state'	      => $filter_state,
			'filter_city'         => $filter_city,
			'filter_locality'     => $filter_locality,
			'filter_status'       => $filter_status,
			'sort'                => $sort,
			'order'               => $order,
			'start'               => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'               => $this->config->get('config_limit_admin')
		);

		$this->load->model('tool/image');

		$product_total = $this->model_catalog_doctor->getTotalDoctors($filter_data);

		$results = $this->model_catalog_doctor->getDoctors($filter_data);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			

			$data['doctors'][] = array(
				'id'             => $result['id'],
				'image'          => $image,
				'name'           => $result['name'],
				'speciality'     => $result['speciality'],
				'mobile'         => $result['mobile'],
				'email'          => $result['email'],
				'office_timing'  => $result['office_timing'],
				'country'        => $result['country'],
				'state'          => $result['state'],
				'city'           => $result['city'],
				'locality'       => $result['locality'],
				'address'        => $result['address'],
				'state'          => $result['state'],
				
				'status'         => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'           => $this->url->link('catalog/doctor/edit', 'token=' . $this->session->data['token'] . '&doctor_id=' . $result['id'] . $url, true)
			);
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

		$data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_speciality'] = $this->language->get('column_speciality');
		$data['column_state']      = $this->language->get('column_state');
		$data['column_city']       = $this->language->get('column_city');
		$data['column_locality']       = $this->language->get('column_locality');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_speciality'] = $this->language->get('entry_speciality');
		$data['entry_state'] = $this->language->get('entry_state');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_locality'] = $this->language->get('entry_locality');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');

		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_speciality'])) {
			$url .= '&filter_speciality=' . urlencode(html_entity_decode($this->request->get['filter_speciality'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_state'])) {
			$url .= '&filter_state=' . $this->request->get['filter_state'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}
        if (isset($this->request->get['filter_locality'])) {
			$url .= '&filter_locality=' . $this->request->get['filter_locality'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=dd.name' . $url, true);
		$data['sort_speciality'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.speciality' . $url, true);
		$data['sort_state'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.state' . $url, true);
		$data['sort_city'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.city' . $url, true);
		$data['sort_locality'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.locality' . $url, true);
		$data['sort_status'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.status' . $url, true);
		$data['sort_order'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . '&sort=d.sort_order' . $url, true);

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_speciality'])) {
			$url .= '&filter_speciality=' . urlencode(html_entity_decode($this->request->get['filter_speciality'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_state'])) {
			$url .= '&filter_state=' . $this->request->get['filter_state'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}
        if (isset($this->request->get['filter_locality'])) {
			$url .= '&filter_locality=' . $this->request->get['filter_locality'];
		}
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_name']       = $filter_name;
		$data['filter_speciality'] = $filter_speciality;
		$data['filter_state']      = $filter_state;
		$data['filter_city']       = $filter_city;
		$data['filter_locality']   = $filter_locality;
		$data['filter_status']     = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/doctor_list', $data));
	}

	protected function getForm() {
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_form'] = !isset($this->request->get['id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['text_plus'] = $this->language->get('text_plus');
		$data['text_minus'] = $this->language->get('text_minus');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_option'] = $this->language->get('text_option');
		$data['text_option_value'] = $this->language->get('text_option_value');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_percent'] = $this->language->get('text_percent');
		$data['text_amount'] = $this->language->get('text_amount');
		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_description'] = $this->language->get('entry_description');
		$data['entry_meta_title'] = $this->language->get('entry_meta_title');
		$data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$data['entry_keyword'] = $this->language->get('entry_keyword');
		$data['entry_speciality'] = $this->language->get('entry_speciality');
		$data['entry_state'] = $this->language->get('entry_state');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_locality'] = $this->language->get('entry_locality');
		$data['entry_mobile'] = $this->language->get('entry_mobile');
		$data['entry_email'] = $this->language->get('entry_email');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_tag'] = $this->language->get('entry_tag');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$data['entry_timing'] = $this->language->get('entry_timing');
		$data['entry_state'] = $this->language->get('entry_state');
		$data['entry_city'] = $this->language->get('entry_city');
		$data['entry_locality'] = $this->language->get('entry_locality');
		$data['entry_address'] = $this->language->get('entry_address');
		$data['entry_services'] = $this->language->get('entry_services');
		$data['entry_image'] = $this->language->get('entry_image');
		$data['entry_additional_image'] = $this->language->get('entry_additional_image');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_services'] = $this->language->get('tab_services');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['button_image_add'] = $this->language->get('button_image_add');
		$data['button_service_add'] = $this->language->get('button_service_add');
		$data['button_remove'] = $this->language->get('button_remove');


		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$data['error_name'] = $this->error['name'];
		} else {
			$data['error_name'] = array();
		}

		if (isset($this->error['meta_title'])) {
			$data['error_meta_title'] = $this->error['meta_title'];
		} else {
			$data['error_meta_title'] = array();
		}

		if (isset($this->error['speciality'])) {
			$data['error_speciality'] = $this->error['speciality'];
		} else {
			$data['error_speciality'] = '';
		}
		if (isset($this->error['services'])) {
			$data['error_services'] = $this->error['services'];
		} else {
			$data['error_services'] = '';
		}

		if (isset($this->error['keyword'])) {
			$data['error_keyword'] = $this->error['keyword'];
		} else {
			$data['error_keyword'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_speciality'])) {
			$url .= '&filter_speciality=' . urlencode(html_entity_decode($this->request->get['filter_speciality'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_state'])) {
			$url .= '&filter_state=' . $this->request->get['filter_state'];
		}

		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}
		if (isset($this->request->get['filter_locality'])) {
			$url .= '&filter_locality=' . $this->request->get['filter_locality'];
		}
		if (isset($this->request->get['filter_state'])) {
			$url .= '&filter_state=' . $this->request->get['filter_state'];
		}
		if (isset($this->request->get['filter_city'])) {
			$url .= '&filter_city=' . $this->request->get['filter_city'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true)
		);

		if (!isset($this->request->get['doctor_id'])) {
			$data['action'] = $this->url->link('catalog/doctor/add', 'token=' . $this->session->data['token'] . $url, true);
		} else {
			$data['action'] = $this->url->link('catalog/doctor/edit', 'token=' . $this->session->data['token'] . '&id=' . $this->request->get['doctor_id'] . $url, true);
		}

		$data['cancel'] = $this->url->link('catalog/doctor', 'token=' . $this->session->data['token'] . $url, true);

		if (isset($this->request->get['doctor_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$doctor_info = $this->model_catalog_doctor->getDoctor($this->request->get['doctor_id']);
		}
		$data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['product_description'])) {
			$data['doctor_description'] = $this->request->post['doctor_description'];
		} elseif (isset($this->request->get['doctor_id'])) {
			$data['doctor_description'] = $this->model_catalog_doctor->getDoctorDescriptions($this->request->get['doctor_id']);
		} else {
			$data['doctor_description'] = array();
		}
        if (isset($this->request->post['services'])) {
			$data['services'] = $this->request->post['services'];
		} elseif (isset($this->request->get['doctor_id'])) {
			$data['services'] = $this->model_catalog_doctor->getDoctorServices($this->request->get['doctor_id']);
		} else {
			$data['services'] = array();
		}
		
		if (isset($this->request->post['speciality'])) {
			$data['speciality'] = $this->request->post['speciality'];
		} elseif (!empty($doctor_info)) {
			$data['speciality'] = $doctor_info['speciality'];
		} else {
			$data['speciality'] = '';
		}
		if (isset($this->request->post['speciality'])) {
			$data['speciality'] = $this->request->post['speciality'];
		} elseif (!empty($doctor_info)) {
			$data['speciality'] = $doctor_info['speciality'];
		} else {
			$data['speciality'] = '';
		}

		if (isset($this->request->post['mobile'])) {
			$data['mobile'] = $this->request->post['mobile'];
		} elseif (!empty($doctor_info)) {
			$data['mobile'] = $doctor_info['mobile'];
		} else {
			$data['mobile'] = '';
		}
		if (isset($this->request->post['timing'])) {
			$data['timing'] = $this->request->post['timing'];
		} elseif (!empty($doctor_info)) {
			$data['timing'] = $doctor_info['office_timing'];
		} else {
			$data['timing'] = '';
		}

		if (isset($this->request->post['email'])) {
			$data['email'] = $this->request->post['email'];
		} elseif (!empty($doctor_info)) {
			$data['email'] = $doctor_info['email'];
		} else {
			$data['email'] = '';
		}

		if (isset($this->request->post['state'])) {
			$data['state'] = $this->request->post['state'];
		} elseif (!empty($doctor_info)) {
			$data['state'] = $doctor_info['state'];
		} else {
			$data['state'] = '';
		}

		if (isset($this->request->post['city'])) {
			$data['city'] = $this->request->post['city'];
		} elseif (!empty($doctor_info)) {
			$data['city'] = $doctor_info['city'];
		} else {
			$data['city'] = '';
		}

		if (isset($this->request->post['locality'])) {
			$data['locality'] = $this->request->post['locality'];
		} elseif (!empty($doctor_info)) {
			$data['locality'] = $doctor_info['locality'];
		} else {
			$data['locality'] = '';
		}

		if (isset($this->request->post['address'])) {
			$data['address'] = $this->request->post['address'];
		} elseif (!empty($doctor_info)) {
			$data['address'] = $doctor_info['address'];
		} else {
			$data['address'] = '';
		}

		if (isset($this->request->post['keyword'])) {
			$data['keyword'] = $this->request->post['keyword'];
		} elseif (!empty($doctor_info)) {
			$data['keyword'] = $doctor_info['keyword'];
		} else {
			$data['keyword'] = '';
		}
		if (isset($this->request->post['sort_order'])) {
			$data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($product_info)) {
			$data['sort_order'] = $product_info['sort_order'];
		} else {
			$data['sort_order'] = 1;
		}
		if (isset($this->request->post['status'])) {
			$data['status'] = $this->request->post['status'];
		} elseif (!empty($product_info)) {
			$data['status'] = $product_info['status'];
		} else {
			$data['status'] = true;
		}
		// Image
		if (isset($this->request->post['image'])) {
			$data['image'] = $this->request->post['image'];
		} elseif (!empty($doctor_info)) {
			$data['image'] = $doctor_info['image'];
		} else {
			$data['image'] = '';
		}
		

		$this->load->model('tool/image');

		if (isset($this->request->post['image']) && is_file(DIR_IMAGE . $this->request->post['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], 100, 100);
		} elseif (!empty($doctor_info) && is_file(DIR_IMAGE . $doctor_info['image'])) {
			$data['thumb'] = $this->model_tool_image->resize($doctor_info['image'], 100, 100);
		} else {
			$data['thumb'] = $this->model_tool_image->resize('no_image.png', 100, 100);
		}

		$data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

		// Images
		if (isset($this->request->post['doctor_image'])) {
			$doctor_images = $this->request->post['doctor_image'];
		} elseif (isset($this->request->get['doctor_id'])) {
			$doctor_images = $this->model_catalog_doctor->getDoctorImages($this->request->get['doctor_id']);
		} else {
			$doctor_images = array();
		}

		$data['doctor_images'] = array();

		foreach ($doctor_images as $doctor_images) {
			if (is_file(DIR_IMAGE . $doctor_images['image'])) {
				$image = $doctor_images['image'];
				$thumb = $doctor_images['image'];
			} else {
				$image = '';
				$thumb = 'no_image.png';
			}

			$data['doctor_images'][] = array(
				'image'      => $image,
				'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
				'sort_order' => $doctor_images['sort_order']
			);
		}

       
		$this->load->model('design/layout');
		$data['layouts'] = $this->model_design_layout->getLayouts();
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		$this->response->setOutput($this->load->view('catalog/doctor_form', $data));
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/doctor')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['doctor_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}

			if ((utf8_strlen($value['meta_title']) < 3) || (utf8_strlen($value['meta_title']) > 255)) {
				$this->error['meta_title'][$language_id] = $this->language->get('error_meta_title');
			}
		}

		if ((utf8_strlen($this->request->post['speciality']) < 1) || (utf8_strlen($this->request->post['speciality']) > 64)) {
			$this->error['speciality'] = $this->language->get('error_speciality');
		}

		if (utf8_strlen($this->request->post['keyword']) > 0) {
			$this->load->model('catalog/url_alias');

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['doctor_id']) && $url_alias_info['query'] != 'doctor_id=' . $this->request->get['doctor_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['doctor_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_model'])) {
			$this->load->model('catalog/product');
			$this->load->model('catalog/option');

			if (isset($this->request->get['filter_name'])) {
				$filter_name = $this->request->get['filter_name'];
			} else {
				$filter_name = '';
			}

			if (isset($this->request->get['filter_model'])) {
				$filter_model = $this->request->get['filter_model'];
			} else {
				$filter_model = '';
			}

			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];
			} else {
				$limit = 5;
			}

			$filter_data = array(
				'filter_name'  => $filter_name,
				'filter_model' => $filter_model,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {
				$option_data = array();

				$product_options = $this->model_catalog_product->getProductOptions($result['product_id']);

				foreach ($product_options as $product_option) {
					$option_info = $this->model_catalog_option->getOption($product_option['option_id']);

					if ($option_info) {
						$product_option_value_data = array();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

							if ($option_value_info) {
								$product_option_value_data[] = array(
									'product_option_value_id' => $product_option_value['product_option_value_id'],
									'option_value_id'         => $product_option_value['option_value_id'],
									'name'                    => $option_value_info['name'],
									'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
									'price_prefix'            => $product_option_value['price_prefix']
								);
							}
						}

						$option_data[] = array(
							'product_option_id'    => $product_option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id'            => $product_option['option_id'],
							'name'                 => $option_info['name'],
							'type'                 => $option_info['type'],
							'value'                => $product_option['value'],
							'required'             => $product_option['required']
						);
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price']
				);
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
}
